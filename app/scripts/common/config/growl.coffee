angular.module "veetaTalentApp.common"
.config (growlProvider) ->
  growlProvider.globalTimeToLive
    success: 2000
    error: 10000
    warning: 10000
    info: 2000

  growlProvider.globalPosition "top-center"

# Disable growl messages
# .run ($rootScope, growl) ->
#   $rootScope.$on "flash:error", (event, growl_str) ->
#     growl.error growl_str

#   $rootScope.$on "flash:warning", (event, growl_str) ->
#     growl.warning growl_str

#   $rootScope.$on "flash:info", (event, growl_str) ->
#     growl.info growl_str

#   $rootScope.$on "flash:notice", (event, growl_str) ->
#     growl.success growl_str