"use strict"
angular.module("veetaTalentApp.common")
.config ($httpProvider, $authProvider) ->
  $authProvider.configure
    apiUrl: CONFIG.apiTalentUrl
    signOutUrl: '/auth/sign_out'
    emailSignInPath: '/auth/sign_in'
    emailRegistrationPath: '/auth'
    accountUpdatePath: '/auth'
    accountDeletePath: '/auth'
    passwordResetPath: '/auth/password'
    passwordUpdatePath: '/auth/password'
    tokenValidationPath: '/auth/validate_token'
    # TODO: move to cookies
    storage: 'localStorage'
    handleTokenValidationResponse: (response)->

  $httpProvider.interceptors.push "authHttpResponseInterceptor"

.factory "authHttpResponseInterceptor", ($rootScope, $log, $q, $location) ->
  ###*
  # @ngdoc service
  # @name common.services:authHttpResponseInterceptor
  #
  # @description
  # Intercept all the response from server, throw authentication-error event if request be deny by server
  ###
  return (
    response: (response) ->
      $log.debug "Response 401"  if response.status is 401
      response or $q.when(response)

    responseError: (rejection) ->
      if rejection.status is 401
        $log.debug "Response Error 401", rejection
        $rootScope.$broadcast 'auth:authentication-error'

      $q.reject rejection
  )

.run ($rootScope, $state, $log, FlashService, Talent, TalentSetting) ->

  _loadTalent = ->
    Talent.query().then (response)->
      $rootScope.talent = response
      $log.debug 'get talent', $rootScope.talent

    TalentSetting.query().then (response)->
      $rootScope.talentSetting = response
      $log.debug 'get talent setting', $rootScope.talentSetting

  $rootScope.$on 'auth:authentication-error', ->
    $log.debug 'auth:authentication-error'

  $rootScope.$on 'auth:invalid', ->
    $log.debug 'auth:invalid'
    _loadTalent()
    $state.go 'app.signIn'

  $rootScope.$on 'auth:login-success', ->
    _loadTalent()
    $log.debug 'auth:login-success'

  $rootScope.$on 'auth:login-error', ->
    $log.debug 'auth:login-error'

  $rootScope.$on 'auth:validation-success', ->
    $log.debug 'auth:validation-success'
    _loadTalent()

  $rootScope.$on 'auth:validation-error', ->
    $log.debug 'auth:validation-error'

  $rootScope.$on 'auth:logout-success', ->
    $log.debug 'auth:logout-success'
    _loadTalent()
    $state.go 'app.signIn'

  $rootScope.$on 'auth:logout-error', ->
    $log.debug 'auth:logout-error'

  $rootScope.$on 'auth:registration-email-success', ->
    $log.debug 'auth:registration-email-success'
    _loadTalent()
    FlashService.setMessage
      title: 'talent.flash.thank_you_for_successfully_signup.title'
      body: 'talent.flash.thank_you_for_successfully_signup.body'

  $rootScope.$on 'auth:registration-email-error', ->
    $log.debug 'auth:registration-email-error'

  $rootScope.$on 'auth:email-confirmation-success', ->
    $log.debug 'auth:email-confirmation-success'
    _loadTalent()

  $rootScope.$on 'auth:email-confirmation-error', ->
    $log.debug 'auth:email-confirmation-error'

  $rootScope.$on 'auth:password-reset-request-success', ->
    $log.debug 'auth:password-reset-request-success'

  $rootScope.$on 'auth:password-reset-request-error', ->
    $log.debug 'auth:password-reset-request-error'

  $rootScope.$on 'auth:password-reset-confirm-success', ->
    $log.debug 'auth:password-reset-confirm-success'

  $rootScope.$on 'auth:password-reset-confirm-error', ->
    $log.debug 'auth:password-reset-confirm-error'

  $rootScope.$on 'auth:password-change-success', ->
    $log.debug 'auth:password-change-success'

  $rootScope.$on 'auth:password-change-error', ->
    $log.debug 'auth:password-change-error'

  $rootScope.$on 'auth:account-update-success', ->
    $log.debug 'auth:account-update-success'
    _loadTalent()

  $rootScope.$on 'auth:account-update-error', ->
    $log.debug 'auth:account-update-error'

  $rootScope.$on 'auth:account-destroy-success', ->
    $log.debug 'auth:account-destroy-success'
    _loadTalent()

  $rootScope.$on 'auth:account-destroy-error', ->
    $log.debug 'auth:account-destroy-error'
