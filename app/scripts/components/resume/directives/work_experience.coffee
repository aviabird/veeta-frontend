"use strict"

###*
  # @ngdoc directive
  # @name resume.directives:resume-work-experiences
  #
  # @requires ResumeWorkExperience
  # @requires RESUME
  #
  # @description
  # education block of resume
  # be used on resume detail page
###

angular.module("veetaTalentApp.resume.directives")
.directive "resumeWorkExperiences", (ResumeWorkExperience, RESUME) ->
  templateUrl = (element, attrs)->
    if attrs.mobile == 'true' then RESUME.MOBILE.PARTIALS.WORK_EXPERIENCES else RESUME.PARTIALS.WORK_EXPERIENCES

  return(
    restrict: "AE"
    templateUrl: templateUrl
    required: "ngModel"
    replace: true
    scope: {}
    controller: ($scope, $timeout) ->

      # ----------------------------------------
      # Initialize attributes
      # ----------------------------------------

      # init a empty workExperiences array
      $scope.workExperiences = []

      # get new WorkExperience Instance
      $scope.newWorkExperience = $scope.$parent.resume.newWorkExperience()

      $scope.workExperiencesTimelineOptions =
        heading: "position"
        subHeading: "company"
        eventName: "position"


      # ----------------------------------------
      # Private methods
      # ----------------------------------------

      _loadWorkExperiences = ->
        $scope.$parent.resume.getWorkExperiences().then (data) ->
          $scope.workExperiences = data

      _initWorkExperiences = ->
        $scope.workExperiences.push new ResumeWorkExperience(workExperience) for workExperience in $scope.$parent.resume.workExperiences


      # ----------------------------------------
      # Events
      # ----------------------------------------

      # set a listener for changing of parent resume
      $scope.$on 'resume:handle-imported-data', (event, importedResume) ->
        for workExperience in importedResume.workExperiences
          w = new ResumeWorkExperience(workExperience)
          w.isJustImported = true
          $scope.workExperiences.push w

      $scope.$on "timeline:events:toReloadList", ->
        _loadWorkExperiences()

      # ----------------------------------------
      # Initialize methods
      # ----------------------------------------

      # init WorkExperiences from resume
      _loadWorkExperiences()
#      _initWorkExperiences()
  )
.directive "resumeWorkExperienceForm", (RESUME, OptionsService) ->
  templateUrl = (element, attrs)->
    if attrs.mobile == 'true' then RESUME.MOBILE.PARTIALS.WORK_EXPERIENCE else RESUME.PARTIALS.WORK_EXPERIENCE

  return(
    restrict: "AE"
    templateUrl: RESUME.PARTIALS.FORM.WORK_EXPERIENCE
    replace: true
    scope:
      originWorkExperience: "=ngModel"

    controller: ($scope, $timeout, $translate) ->

      # ----------------------------------------
      # Initialize attributes
      # ----------------------------------------
      $scope.workExperience = angular.copy($scope.originWorkExperience)
      #      $scope.workExperience = $scope.originWorkExperience

      $scope.levelOptions = OptionsService.jobLevelOptions
      $scope.countryOptions = OptionsService.countryOptions
      $scope.termOfEmploymentOptions = OptionsService.termsOfEmploymentOptions
      $scope.industryOptions = OptionsService.industryOptions
      $scope.currentJobOptions =
        checkbox:
          # text: 'Current job'
          text: $translate.instant('talent.data.work_experience.current_job')

      $scope.toOptions =
        monthSelect:
          allowNow: true

      # ----------------------------------------
      # Public methods
      # ----------------------------------------

      $scope.save = ->
        $scope.workExperience.save().then ->
          angular.extend $scope.originWorkExperience, $scope.workExperience
          $scope.originWorkExperience.isJustImported = false
          $scope.$emit "eventExitEditMode"
          $scope.$emit 'timeline:events:toReloadList'


      $scope.cancel = ->
        $scope.$emit "eventExitEditMode"


      $scope.toValid =
        name: 'to',
        expression: (value) ->
          return true unless value?
          value > $scope.workExperience.from
        messages:
          invalid: $translate.instant('talent.errors.date.to_is_greater')

      # ----------------------------------------
      # Watch
      # ----------------------------------------

      $scope.$watch 'workExperience.from', (newVal, oldVal) ->
        if $scope.workExperienceForm.to.$dirty
          $scope.$broadcast 'trigger-to-validate'

  )
