describe "veetaTalentApp.jobapp", ->
  describe "module", ->
    module = undefined

    beforeEach ->
      module = angular.module('veetaTalentApp.jobapp')

    it "should be registered", ->
      expect(module).toBeDefined()

    describe "Dependencies:", ->
      deps = undefined

      hasModule = (m)->
        deps.indexOf(m) >= 0

      beforeEach ->
        deps = module.value('appName').requires

      it "should have veetaTalentApp.jobapp.controllers as a dependency", ->
        expect(hasModule('veetaTalentApp.jobapp.controllers')).toBeTruthy()

      it "should have veetaTalentApp.jobapp.directives as a dependency", ->
        expect(hasModule('veetaTalentApp.jobapp.directives')).toBeTruthy()

      it "should have  veetaTalentApp.jobapp.resources as a dependency", ->
        expect(hasModule('veetaTalentApp.jobapp.resources')).toBeTruthy()