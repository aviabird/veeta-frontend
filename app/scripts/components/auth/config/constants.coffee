angular.module("veetaTalentApp.auth")
.constant "AUTH",
  VIEWS:
    SIGN_IN: "views/components/auth/sign-in.html"
    SIGN_UP: "views/components/auth/sign-up.html"
    REQUEST_PASSWORD: "views/components/auth/request-password.html"
    RESET_PASSWORD: "views/components/auth/reset-password.html"