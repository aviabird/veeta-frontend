describe('veetaTalentApp.desktop States -', function() {

    var $state,
        $rootScope,
        $injector,
        $timeout,
        state = 'myState';

    beforeEach(module('veetaTalentApp.desktop'));
    beforeEach(inject(function ($injector) {
        $rootScope = $injector.get('$rootScope');
        $state = $injector.get('$state');
        $timeout = $injector.get('$timeout');
        $scope = $injector.get('$state');
    }));

//    beforeEach(inject(function ($injector) {
//        inject(function(_$rootScope_, _$state_, _$injector_, $templateCache) {
//            $rootScope = _$rootScope_;
//            $state = _$state_;
//            $injector = _$injector_;
//
//            // We need add the template entry into the templateCache if we ever
//            // specify a templateUrl
//            $templateCache.put('template.html', '');
//        })
//    });

    xdescribe('app:', function() {
        describe('resumes:', function() {
            describe('new:', function() {
                it('should respond from state app.resumes.overview', function() {
                    expect($state.href(state, { id: 1 })).toEqual('#/state/1');
                });

                it('should redirect to app.resumes.overview from other states', function() {
                    expect($state.href(state, { id: 1 })).toEqual('#/state/1');
                });
            })
        })
    });
});