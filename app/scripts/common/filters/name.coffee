angular.module 'veetaTalentApp.common.filters'
.filter 'name', ->
  (person) ->
    fullNameArr = []
    fullNameArr.push person.firstName  if person.firstName
    fullNameArr.push person.lastName  if person.lastName
    fullNameArr.join " "
.filter 'fullName', ->
  (person) ->
    fullNameArr = []
    fullNameArr.push person.title  if person.title
    fullNameArr.push person.firstName  if person.firstName
    fullNameArr.push person.lastName  if person.lastName
    fullNameArr.join " "
