describe "veetaTalentApp", ->
  describe 'account', ->
    describe 'directives', ->
      describe 'accountSettings', ->
        beforeEach module(
          'veetaTalentApp.account'
          'veetaTalentApp.common'
          "views/components/account/partials/settings.html"
        )

        $document = null
        $scope = null
        $rootScope = null
        $modal = null
        elementScope = null
        $httpBackend = null
        TalentAuth = null
        TalentSetting = null

        beforeEach inject (_$rootScope_
                           _$document_
                           _$modal_
                           _$httpBackend_
                           _TalentAuth_
                           _TalentSetting_) ->
          $rootScope = _$rootScope_
          $scope = _$rootScope_.$new()
          $document = _$document_
          $modal = _$modal_
          $httpBackend = _$httpBackend_
          TalentAuth = _TalentAuth_
          TalentSetting = _TalentSetting_

          $httpBackend.whenGET(/(api\/locales)/).respond()
          $httpBackend.whenPUT(/(api\/talent\/talent_setting)/).respond()


        beforeEach inject (_$compile_) ->
          element = angular.element '<div account-settings=""></div>'
          $rootScope.talentSetting = new TalentSetting
            resumeType: 1
            newsletterUpdates: "disabled"
          _$compile_(element)($scope)
          $scope.$apply()
          elementScope = element.isolateScope()


        describe 'initialize', ->
          it 'should initialize a new Talent Setting', ->
            expect(elementScope.talentSetting).toEqual $rootScope.talentSetting

          it 'should initialize newsletterUpdatesOptions', ->
            expect(elementScope.newsletterUpdatesOptions).not.toBe null


        describe 'save()', ->
          it 'should save setting to server', ->
            elementScope.save()
            $httpBackend.flush()

        describe 'reset()', ->
          it 'should save setting to server', ->
            elementScope.talentSetting.workingAreas = []
            elementScope.reset()
            expect(elementScope.talentSetting).toEqual $rootScope.talentSetting

        # We don't want to format the text right now
        # describe 'resumeTypeFormatter(value)', ->
        #   it 'should add a new WorkingArea', ->
        #     expect(elementScope.resumeTypeFormatter(1)).toBe 'Beginner'
        #     expect(elementScope.resumeTypeFormatter(3)).toBe 'Experienced'

