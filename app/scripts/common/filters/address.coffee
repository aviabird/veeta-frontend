angular.module 'veetaTalentApp.common.filters'
.filter 'placeInfo', ->
  (address) ->
    if address?
      out = []
      out.push address.zip  if address.zip
      out.push address.city  if address.city
      out.push address.country.name  if address.country
      out.join " "