describe "veetaTalentApp.jobapp", ->
  describe "controllers", ->
    describe 'JobappWithdrawConfirmationCtrl', ->
      beforeEach module(
        "veetaTalentApp.jobapp"
        'mocked/jobapps/detail.json'
      )

      $scope = undefined
      $controller = undefined
      $httpBackend = undefined
      jobapp = undefined
      Jobapp = undefined

      beforeEach inject (_$httpBackend_
                         _$rootScope_
                         _$modal_
                         _$controller_
                         _Jobapp_
                         _JOBAPP_
                         _mockedJobappsDetail_) ->
        $httpBackend = _$httpBackend_
        $scope = _$rootScope_.$new()

        # mock a resume
        Jobapp = _Jobapp_
        jobapp = new Jobapp(_mockedJobappsDetail_)

        # Create a mock modalInstance using spies
        $modalInstance =
          close: jasmine.createSpy("modalInstance.close")
          dismiss: jasmine.createSpy("modalInstance.dismiss")
          result:
            then: jasmine.createSpy("modalInstance.result.then")

        # create controller object
        $controller = _$controller_ 'JobappWithdrawConfirmationCtrl',
          $scope: $scope
          $modalInstance: $modalInstance
          jobapp: jobapp

      describe 'initialize', ->
        it 'should get a jobapp', ->
          expect($scope.jobapp.id).toBe(1)

      describe 'withdrawn()', ->
        it 'should close modal after jobapp withdrawn', ->

      describe 'cancel()', ->
        it 'should close modal', ->


