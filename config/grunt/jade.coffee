module.exports = (grunt, appConfig) ->
  tasks:
    watch:
      jade:
        files: ["#{appConfig.paths.app}/views/{,*/}{,*/}{,*/}*.jade"]
        tasks: [
          "newer:jade:dist"
          'html2js:main'
        ]
        options:
          spawn: false
          livereload: true
      jadeIndex:
        files: ["#{appConfig.paths.app}/*.jade"]
        tasks: [
          "newer:jade:index"
          "replace:server"
          'html2js:main'
        ]
        options:
          spawn: false
          livereload: true

    # Compiles Jade to Html
    jade:
      options:
        sourceMap: true
        sourceRoot: ""

      dist:
        files: [
          expand: true
          cwd: "#{appConfig.paths.app}/views"
          src: "**/*.jade"
          dest: "#{appConfig.paths.tmp}/views"
          ext: ".html"
        ]

      index:
        options:
          pretty: true
        files: [
          expand: true
          pretty: true
          cwd: "#{appConfig.paths.app}"
          src: "*.jade"
          dest: "#{appConfig.paths.tmp}"
          ext: ".html"
        ]

