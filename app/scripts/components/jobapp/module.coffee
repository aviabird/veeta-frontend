###*
  # @ngdoc overview
  # @name jobapp
  #
###

angular.module 'veetaTalentApp.jobapp', [

  # components
  'veetaTalentApp.resume'

  # modules
  'veetaTalentApp.jobapp.controllers'
  'veetaTalentApp.jobapp.directives'
  'veetaTalentApp.jobapp.resources'
]

angular.module 'veetaTalentApp.jobapp.controllers', [
  'ui.bootstrap'
]
angular.module 'veetaTalentApp.jobapp.directives', []
angular.module 'veetaTalentApp.jobapp.resources', ['rails']
