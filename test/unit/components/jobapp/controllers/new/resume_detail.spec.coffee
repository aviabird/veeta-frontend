describe "veetaTalentApp.jobapp", ->
  describe "controllers", ->
    describe 'new', ->
      describe 'JobappNewResumeDetailCtrl', ->
        beforeEach module(
          "veetaTalentApp.jobapp"
          'mocked/jobapps/new-with-job.json'
          'mocked/jobapps/new-without-job.json'
          'mocked/resumes/detail.json'
        )

        $rootScope = null
        $scope = null
        $controller = null
        $httpBackend = null
        jobapp = null
        Jobapp = null
        Resume = null
        resume = null

        beforeEach inject (_$httpBackend_
                           _$rootScope_
                           _$controller_
                           _Resume_
                           _$state_
                           _Jobapp_
                           _JOBAPP_
                           _mockedJobappsNewWithJob_
                           _mockedResumesDetail_) ->
          $httpBackend = _$httpBackend_
          $rootScope = _$rootScope_
          $scope = $rootScope.$new()

          # mock a jobapp with resume
          Jobapp = _Jobapp_
          Resume = _Resume_
          jobapp = new Jobapp(_mockedJobappsNewWithJob_)
          jobapp.resume = new Resume()
          resume = new Resume(_mockedResumesDetail_)
          $rootScope.talent =
            firstName: 'John'
            lastName: 'Smith'
            salutation: 'male'

          $scope.$parent.jobapp = jobapp

          spyOn(_$state_, 'go')

          # create controller object
          $controller = _$controller_ 'JobappNewResumeDetailCtrl',
            $scope: $scope
            $rootScope: $rootScope
            $state: _$state_
            resume: resume

        describe 'EmailApplication', ->
          describe 'initialize', ->
            it 'should get a jobapp', ->

        describe 'RefApplication', ->
          describe 'initialize', ->
            it 'should get a jobapp', ->

        describe 'isShowSharedCompanies()', ->
          it 'should true with companies', ->
            $scope.resume.sharedWith = [1, 2, 3]
            expect($scope.isShowSharedCompanies()).toBeTruthy()

          it 'should false without companies', ->
            $scope.resume.sharedWith = null
            expect($scope.isShowSharedCompanies()).toBeFalsy()
            $scope.resume.sharedWith = []
            expect($scope.isShowSharedCompanies()).toBeFalsy()

        xdescribe 'isNewResume()', ->
          it 'should check if resume is new', ->
            $scope.resume = new Resume()
            expect($scope.isNewResume()).toBeTruthy()
            $scope.resume.id = 1
            expect($scope.isNewResume()).toBeFalsy()

        describe 'goNextStep()', ->
          it 'should save resume and go to next step', inject (_$q_) ->
#            deferred = _$q_.defer()
#            spyOn($scope.resume, 'save').and.returnValue(deferred.promise)
#            $scope.goNextStep()
#            deferred.resolve()
#            $rootScope.$digest();
#            expect($scope.resume).toHaveBeenCalled();
