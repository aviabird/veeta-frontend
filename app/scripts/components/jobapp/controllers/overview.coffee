###*
  # @ngdoc controller
  # @name jobapp.controllers:JobappOverviewCtrl
  #
  # @requires $scope
  # @requires Jobapp
  #
  # @description
  # Controller for Jobapp Overview
###

angular.module 'veetaTalentApp.jobapp.controllers'
.controller 'JobappOverviewCtrl', ($scope, Jobapp) ->

  # Get a list of jobapps from API
  Jobapp.query().then (data) ->
    $scope.jobapps = data
