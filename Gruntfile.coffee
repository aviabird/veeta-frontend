# Generated on 2014-10-28 using generator-angular 0.9.8
"use strict"

# # Globbing
# for performance reasons we're only matching one level down:
# 'test/spec/{,*/}*.js'
# use this if you want to recursively match all subfolders:
# 'test/spec/**/*.js'
module.exports = (grunt) ->

  # Load grunt tasks automatically
  require("load-grunt-tasks") grunt

  # Time how long tasks take. Can help when optimizing build times
  require("time-grunt") grunt

  # Configurable paths for the application
  appConfig =
    config:
      src: "config/grunt/*"

    paths:
      app: require("./bower.json").appPath or "app"
      dist: "dist"
      test: "test"
      tmp: ".tmp"

      scripts:
        common: [
          'app/scripts/lib/angular-rails-resource-extend/module.coffee'
          'app/scripts/lib/angular-rails-resource-extend/{,*/}{,*/}*.coffee'

          'app/scripts/lib/angular-translate/angular-translate.js'
          'app/scripts/lib/angular-translate/angular-translate-loader-url.js'

          'app/scripts/common/module.coffee'
          'app/scripts/common/{,*/}{,*/}*.coffee'
        ]
        desktop: [
          'app/scripts/domains/desktop/module.coffee'
          'app/scripts/domains/desktop/{,*/}{,*/}*.coffee'

          'app/scripts/components/auth/module.coffee'
          'app/scripts/components/auth/{,*/}{,*/}*.coffee'

          'app/scripts/components/resume/module.coffee'
          'app/scripts/components/resume/{,*/}{,*/}*.coffee'

          'app/scripts/components/profile/module.coffee'
          'app/scripts/components/profile/{,*/}{,*/}*.coffee'

          'app/scripts/components/jobapp/module.coffee'
          'app/scripts/components/jobapp/{,*/}{,*/}*.coffee'

          'app/scripts/components/support/module.coffee'
          'app/scripts/components/support/{,*/}{,*/}*.coffee'

          'app/scripts/components/account/module.coffee'
          'app/scripts/components/account/{,*/}{,*/}*.coffee'

          'app/scripts/components/sharing/module.coffee'
          'app/scripts/components/sharing/{,*/}{,*/}*.coffee'
        ]
        mobile: [
          'app/scripts/domains/mobile/module.coffee'
          'app/scripts/domains/mobile/{,*/}{,*/}*.coffee'

          'app/scripts/components/auth/module.coffee'
          'app/scripts/components/auth/{,*/}{,*/}*.coffee'

          'app/scripts/components/jobapp/module.coffee'
          'app/scripts/components/jobapp/{,*/}{,*/}*.coffee'

          'app/scripts/components/account/module.coffee'
          'app/scripts/components/account/{,*/}{,*/}*.coffee'

          'app/scripts/components/resume/module.coffee'
          'app/scripts/components/resume/{,*/}{,*/}*.coffee'
        ]

  # Define the configuration for all the tasks
  configs = require("load-grunt-configs") grunt, appConfig

  grunt.initConfig configs


  grunt.registerTask "serve", "Compile then start a connect web server", (target) ->
    if target is "dist"
      return grunt.task.run([
        "build"
        "connect:dist:keepalive"
      ])

    grunt.task.run [
      "clean:server"
      "install"
      "concurrent:server"
      "replace:server"
      "autoprefixer"
      'html2js:main'
      "connect:livereload"
      "watch"
    ]

  grunt.registerTask "server", "DEPRECATED TASK. Use the \"serve\" task instead", (target) ->
    grunt.log.warn "The `server` task has been deprecated. Use `grunt serve` to start a server."
    grunt.task.run ["serve:" + target]

  grunt.registerTask "test", [
    "clean:server"
    "install"
    "concurrent:test"
    "autoprefixer"
    'html2js:main'
    "connect:test"
    "karma"
  ]
  grunt.registerTask "build", [
    "clean:dist"
    "install"
    "jade"
    "useminPrepare"
    "concurrent:dist"
    "autoprefixer"
    'html2js:main'
    "concat"
    "ngAnnotate"
    "copy:dist"
    "cdnify"
    "cssmin"
    "uglify"
    "filerev"
    "usemin"
    "replace:dist"
  ]
  grunt.registerTask "default", [
    "newer:jshint"
    "test"
    "build"
  ]

  grunt.registerTask "install", [
    "wiredep"
    "injector"
  ]

  grunt.registerTask 'docs', [
    "clean:docs"
    'coffee'
    'ngdocs'
  ]
