"use strict"

###*
  # @ngdoc directive
  # @name desktop.directives:app-footer
  #
  # @requires DESKTOP
  #
  # @description
  # application footer
  # be used on desktop application layout
###

angular.module("veetaTalentApp.desktop.directives")
.directive "appFooter", (DESKTOP) ->
  restrict: "AE"
  templateUrl: DESKTOP.PARTIALS.FOOTER
  replace: true
  controller: ($scope, $rootScope) ->
