describe "veetaTalentApp.sharing", ->
  describe "module", ->
    module = undefined

    beforeEach ->
      module = angular.module('veetaTalentApp.sharing')

    it "should be registered", ->
      expect(module).toBeDefined()

    describe "Dependencies:", ->
      deps = undefined

      hasModule = (m)->
        deps.indexOf(m) >= 0

      beforeEach ->
        deps = module.value('appName').requires

      it "should have veetaTalentApp.sharing.controllers as a dependency", ->
        expect(hasModule('veetaTalentApp.sharing.controllers')).toBeTruthy()

      it "should have veetaTalentApp.sharing.directives as a dependency", ->
        expect(hasModule('veetaTalentApp.sharing.directives')).toBeTruthy()

      it "should have  veetaTalentApp.sharing.resources as a dependency", ->
        expect(hasModule('veetaTalentApp.sharing.resources')).toBeTruthy()
