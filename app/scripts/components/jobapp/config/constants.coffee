angular.module 'veetaTalentApp.jobapp'
.constant 'JOBAPP',
  VIEWS:
    OVERVIEW: 'views/components/jobapp/overview.html'
    DETAIL: 'views/components/jobapp/detail.html'

    JOB: 'views/components/jobapp/job.html'
    JOB_UNAVAILABLE: 'views/components/jobapp/job-unavailable.html'

    NEW:
      WRAPPER: 'views/components/jobapp/new.html'
      RESUME_SELECT: 'views/components/jobapp/new/resume-select.html'
      RESUME_DETAIL: 'views/components/jobapp/new/resume-detail.html'
      ADD_COVER_LETTER: 'views/components/jobapp/new/add-cover-letter.html'
      CHECK_AND_SEND: 'views/components/jobapp/new/check-and-send.html'

  PARTIALS:
    WIZARD_BAR: 'views/components/jobapp/partials/wizard-bar.html'
    JOB_INTRO: 'views/components/jobapp/partials/job-intro.html'
    WITHDRAWN_CONFIRM: 'views/components/jobapp/partials/withdrawn-confirm.html'
    PASSWORD: 'views/components/jobapp/partials/password.html'


  MOBILE:
    VIEWS:
      OVERVIEW: 'views/components/jobapp/overview.html'
      DETAIL: 'views/components/jobapp/detail.html'

      JOB: 'views/components/jobapp/mobile/job.html'
      NEW: 'views/components/jobapp/mobile/new.html'
      NEW_SELECT_RESUME: 'views/components/jobapp/mobile/new/select_resume.html'
      NEW_RESUME_DETAIL: 'views/components/jobapp/mobile/new/resume_detail.html'
      NEW_ADD_COVER_LETTER: 'views/components/jobapp/mobile/new/add_cover_letter.html'
      NEW_CHECK_AND_SEND: 'views/components/jobapp/mobile/new/check_and_send.html'
      NEW_FINISHED: 'views/components/jobapp/mobile/new/finished.html'

    PARTIALS:
      WIZARD_BAR: 'views/components/jobapp/partials/wizard_bar.html'
      JOB_INTRO: 'views/components/jobapp/partials/job_intro.html'
      WITHDRAWN_CONFIRM: 'views/components/jobapp/partials/withdrawn_confirm.html'
