"use strict"

###*
  # @ngdoc directive
  # @name resume.directives:resume-profile
  #
  # @requires $rootScope
  # @requires $log
  # @requires $auth
  # @requires RESUME
  # @requires FileUploader
  # @requires OptionsService
  #
  # @description
  # a wrapper of talent profile
  # within its html will use talent-profile directive
  # this directive prepare, if there is something different
  # be used on resume detail page
###

angular.module("veetaTalentApp.resume.directives")
.directive "resumeProfile", ($rootScope
                             $log
                             $auth
                             RESUME
                             FileUploader
                             OptionsService) ->
  templateUrl = (element, attrs)->
    if attrs.mobile == 'true' then RESUME.MOBILE.PARTIALS.PROFILE else RESUME.PARTIALS.PROFILE
  
  restrict: "AE"
  templateUrl: templateUrl
  replace: true
  scope: {}
  controller: ($scope) ->

    # ----------------------------------------
    # Initialize attributes
    # ----------------------------------------

    # get resume from parent
    $scope.resume = $scope.$parent.resume
    $scope.talent = $rootScope.talent
    $scope.resume.talent = $scope.talent
