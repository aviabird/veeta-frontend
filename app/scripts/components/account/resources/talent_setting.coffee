"use strict"
angular.module("veetaTalentApp.account.resources")
.factory "TalentSetting", (railsResourceFactory) ->
  resource = railsResourceFactory
    url: CONFIG.apiTalentUrl + "/talent_setting"
    name: "talent_setting"
    serializer: "TalentSettingSerializer"

  resource::updateResumeUpdates = ->
    resource.$put(@$url(), {resumeUpdates: @resumeUpdates}).then (data) ->
      @resumeUpdates = data.resumeUpdates

  resource

.factory "TalentSettingSerializer", (railsSerializer) ->
  railsSerializer ->
    @only(
      'resumeType'
      'newsletterLocale'
      'newsletterUpdates'
      'resumeUpdates'
      'workingAreas'
      'workingIndustries'
      'availableIn'
      'interested'
      'salaryExpMin'
      'salaryExpMax'
    )
    @add 'salaryExpMax', (setting) ->
      return unless setting.salaryExpMin?
      switch setting.salaryExpMin
        when 0
          setting.salaryExpMax = 4000
        when 4000
          setting.salaryExpMax = 10000
        when 10000
          setting.salaryExpMax = 20000
        when 20000
          setting.salaryExpMax = 30000
        when 30000
          setting.salaryExpMax = 40000
        when 40000
          setting.salaryExpMax = 50000
        when 50000
          setting.salaryExpMax = 70000
        when 70000
          setting.salaryExpMax = 90000
        when 90000
          setting.salaryExpMax = 110000
        when 110000
          setting.salaryExpMax = 130000
        when 130000
          setting.salaryExpMax = 150000
        when 150000
          setting.salaryExpMax = 0
