describe "veetaTalentApp.resume", ->
  describe "module", ->
    module = undefined

    beforeEach ->
      module = angular.module('veetaTalentApp.resume')

    it "should be registered", ->
      expect(module).toBeDefined()

    describe "Dependencies:", ->
      deps = undefined

      hasModule = (m)->
        deps.indexOf(m) >= 0

      beforeEach ->
        deps = module.value('appName').requires

      it "should have veetaTalentApp.resume.controllers as a dependency", ->
        expect(hasModule('veetaTalentApp.resume.controllers')).toBeTruthy()

      it "should have veetaTalentApp.resume.directives as a dependency", ->
        expect(hasModule('veetaTalentApp.resume.directives')).toBeTruthy()

      it "should have  veetaTalentApp.resume.resources as a dependency", ->
        expect(hasModule('veetaTalentApp.resume.resources')).toBeTruthy()