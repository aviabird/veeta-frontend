"use strict"

###*
  # @ngdoc directive
  # @name account.directives:account-availability
  #
  # @requires $rootScope
  # @requires ACCOUNT
  # @requires TalentSetting
  # @requires OptionsService
  #
  # @restrict AE
  #
  # @description
  # form for talent availability
  # be used on talent setting page and resume edit page(new resume)
###

angular.module("veetaTalentApp.account.directives")
.directive "accountAvailability", ($rootScope, ACCOUNT, TalentSetting, OptionsService) ->
  ###*
  Clone a origin talentSetting
  ###
  originTalentSetting = null
  keepOrigin = ($scope)->
    originTalentSetting = angular.copy($scope.talentSetting)

  return (
    restrict: "AE"
    templateUrl: ACCOUNT.PARTIALS.AVAILABILITY
    replace: true
    scope: {}
    controller: ($scope) ->
      ###*
      Initialize a new TalentSetting with the data from rootScope
      ###
      $scope.talentSetting = new TalentSetting
        workingAreas: $rootScope.talentSetting.workingAreas
        availableIn: $rootScope.talentSetting.availableIn
        interested: $rootScope.talentSetting.interested
        workingIndustries: $rootScope.talentSetting.workingIndustries
        salaryExpMin: $rootScope.talentSetting.salaryExpMin
      keepOrigin($scope)

      ###*
      Initialize newWorkingArea & newWorkingIndustry model
      ###
      $scope.newWorkingArea = ''
      $scope.newWorkingIndustry = ''

      ###*
      Set options
      ###
      $scope.salaryExpectationOptions = OptionsService.salaryOptions
      $scope.jobInterestOptions = OptionsService.jobInterestOptions
      $scope.availabilityInOptions = OptionsService.availableOptions

      ###*
      Save
      ###
      $scope.save = ->
        delete $scope.talentSetting.id
        $scope.talentSetting.update().then (data) ->
          keepOrigin($scope)

      ###*
      Reset
      ###
      $scope.reset = ->
        $scope.talentSetting = angular.copy(originTalentSetting)

      ###*
      addWorkingArea
      ###
      $scope.addWorkingArea = (workingArea)->
        if $scope.talentSetting.workingAreas.indexOf(workingArea) == -1
          $scope.talentSetting.workingAreas.push angular.copy(workingArea)
          $scope.newWorkingArea = ''
        else
          console.log('already exist')

      ###*
      removeWorkingArea
      ###
      $scope.removeWorkingArea = (workingArea) ->
        $scope.talentSetting.workingAreas = $scope.talentSetting.workingAreas.filter (area) ->
          area isnt workingArea

      ###*
      addWorkingIndustry
      ###
      $scope.addWorkingIndustry = (workingIndustry)->
        if $scope.talentSetting.workingIndustries.indexOf(workingIndustry) == -1
          $scope.talentSetting.workingIndustries.push angular.copy(workingIndustry)
          $scope.newWorkingIndustry = ''
        else
          console.log('already exist')

      ###*
      removeWorkingIndustry
      ###
      $scope.removeWorkingIndustry = (workingIndustry) ->
        $scope.talentSetting.workingIndustries = $scope.talentSetting.workingIndustries.filter (industry) ->
          industry isnt workingIndustry
  )
