###*
  # @ngdoc overview
  # @name support
  #
###

angular.module 'veetaTalentApp.support', [
    'veetaTalentApp.support.directives',
    'veetaTalentApp.support.resources'
]

angular.module 'veetaTalentApp.support.directives', []
angular.module 'veetaTalentApp.support.resources', [ 'rails']
