"use strict"

###*
  # @ngdoc directive
  # @name desktop.directives:app-header
  #
  # @requires DESKTOP
  #
  # @description
  # application header
  # be used on desktop application layout
###

angular.module("veetaTalentApp.common.directives")
.directive "appHeader", ($rootScope, $auth, $translate, $state, localStorageService, DESKTOP) ->
  restrict: "AE"
  templateUrl: DESKTOP.PARTIALS.HEADER
  replace: true
  scope: {}
  controller: ($scope) ->

    $scope.talent = $rootScope.talent

    # optimize
    $rootScope.$watch 'talent', ->
      $scope.talent = $rootScope.talent

    $scope.switchStatus =
      languages: false

    $scope.languages = $rootScope.translationOptions

    $scope.currentTranslate = $rootScope.currentTranslate

    # TODO: Use translationService
    $scope.changeTranslate = (lang) ->
      $scope.currentTranslate = $rootScope.currentTranslate = lang
      localStorageService.set('currentTranslate', lang)
      $translate.use lang.key
      $scope.switchStatus.languages = false
      $state.reload() unless $state.current.abstract is true


    $scope.isShowUserNav = ->
      true unless $state.includes("**.jobappAssistant.**") or
        $state.includes("app.signIn") or
        $state.includes('app.welcome') or
        $state.includes('app.job') or
        !$rootScope.talent

    $scope.isShowUserAvatar = ->
      true unless $state.includes("**.jobappAssistant.**") or
        $state.includes("app.signIn") or
        $state.includes('app.welcome') or
        $state.includes('app.job') or
        !($rootScope.talent and $rootScope.talent.avatarUrl and $rootScope.talent.avatarUrl.small)

    $scope.isShowLoginForm = ->
      true if $state.includes('app.welcome')

    $scope.onSignOut = ->
      $auth.signOut()

    $scope.signIn = ->
      $auth.submitLogin($scope.talentAuth)
      .then ->
        $state.go 'app.inside.jobapps.overview'
      .catch (rejection) ->
        if  rejection.errors? and rejection.errors.length > 0
          error = rejection.errors[0]
          $rootScope.$broadcast 'flash:warning', error
