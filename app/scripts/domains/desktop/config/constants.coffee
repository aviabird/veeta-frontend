angular.module("veetaTalentApp.desktop")
.constant "DESKTOP",
  VIEWS:
    WELCOME: 'views/domains/desktop/welcome.html'

  PARTIALS:
    HEADER: "views/domains/desktop/partials/header.html"
    FOOTER: "views/domains/desktop/partials/footer.html"
    MAIN_NAV: "views/domains/desktop/partials/main-nav.html"
