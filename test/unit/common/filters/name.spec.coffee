describe "veetaTalentApp.common", ->
  describe "filter", ->
    describe "Name", ->
      beforeEach module('veetaTalentApp.common.filters')

      $filter = undefined

      beforeEach inject (_$filter_) ->
        $filter = _$filter_

      it 'has a filters', ->
        expect($filter('fullName')).not.toBeNull()
        expect($filter('name')).not.toBeNull()

      describe 'name', ->
        $nameFilter = undefined

        beforeEach inject (_nameFilter_) ->
          $nameFilter = _nameFilter_

        it 'with complete name', ->
          person =
            title: 'Dr.'
            firstName: 'First'
            lastName: 'Last'

          expect($nameFilter(person)).toBe('First Last')

      describe 'fullName', ->
        $fullNameFilter = undefined

        beforeEach inject (_fullNameFilter_) ->
          $fullNameFilter = _fullNameFilter_

        it 'with complete name', ->
          person =
            title: 'Dr.'
            firstName: 'First'
            lastName: 'Last'

          expect($fullNameFilter(person)).toBe('Dr. First Last')

        it 'only with last name', ->
          person =
            lastName: 'Last'
          expect($fullNameFilter(person)).toBe('Last')

        it 'only with first name', ->
          person =
            firstName: 'First'
          expect($fullNameFilter(person)).toBe('First')

        it 'without title', ->
          person =
            firstName: 'First'
            lastName: 'Last'
          expect($fullNameFilter(person)).toBe('First Last')