module.exports = (grunt, appConfig) -> tasks:
  # Make sure code styles are up to par and there are no obvious mistakes
  jshint:
    options:
      jshintrc: ".jshintrc"
      reporter: require("jshint-stylish")

    all:
      src: ["Gruntfile.js"]


  # Add vendor prefixed styles
  autoprefixer:
    options:
      browsers: ["last 1 version"]

    dist:
      files: [
        expand: true
        cwd: "#{appConfig.paths.tmp}/styles/"
        src: "{,*/}*.css"
        dest: "#{appConfig.paths.tmp}/styles/"
      ]


  # Renames files for browser caching purposes
  # todo filerev fonts files
  filerev:
    dist:
      src: [
        "#{appConfig.paths.dist}/scripts/{,*/}*.js"
        "#{appConfig.paths.dist}/styles/{,*/}*.css"
        "#{appConfig.paths.dist}/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}"
        "#{appConfig.paths.dist}/fonts/*"
      ]


  # Reads HTML for usemin blocks to enable smart builds that automatically
  # concat, minify and revision files. Creates configurations in memory so
  # additional tasks can operate on them
  useminPrepare:
    html: ["#{appConfig.paths.tmp}/index.html", "#{appConfig.paths.tmp}/mobile.html"]
    options:
      dest: "#{appConfig.paths.dist}"
      flow:
        html:
          steps:
            js: [
              "concat"
              "uglifyjs"
            ]
            css: ["cssmin"]
          post: {}



# Performs rewrites based on filerev and the useminPrepare configuration
  usemin:
    html: ["#{appConfig.paths.dist}/**/*.html"]
    css: ["#{appConfig.paths.dist}/styles/**/*.css"]
    options:
      assetsDirs: [
        "#{appConfig.paths.dist}"
        "#{appConfig.paths.dist}/images"
      ]


  # The following *-min tasks will produce minified files in the dist folder
  # By default, your `index.html`'s <!-- Usemin block --> will take care of
  # minification. These next options are pre-configured if you do not wish
  # to use the Usemin blocks.
  # cssmin: {
  #   dist: {
  #     files: {
  #       '#{appConfig.paths.dist}/styles/main.css': [
  #         '.tmp/styles/{,*/}*.css'
  #       ]
  #     }
  #   }
  # },
  # uglify: {
  #   dist: {
  #     files: {
  #       '#{appConfig.paths.dist}/scripts/scripts.js': [
  #         '#{appConfig.paths.dist}/scripts/scripts.js'
  #       ]
  #     }
  #   }
  # },
#  concat: {
#     dist: {}
#  },
  imagemin:
    dist:
      files: [
        expand: true
        cwd: "#{appConfig.paths.app}/images"
        src: "{,*/}*.{png,jpg,jpeg,gif}"
        dest: "#{appConfig.paths.dist}/images"
      ]

  svgmin:
    dist:
      files: [
        expand: true
        cwd: "#{appConfig.paths.app}/images"
        src: "{,*/}*.svg"
        dest: "#{appConfig.paths.dist}/images"
      ]

  htmlmin:
    dist:
      options:
        collapseWhitespace: true
        conservativeCollapse: true
        collapseBooleanAttributes: true
        removeCommentsFromCDATA: true
        removeOptionalTags: true

      files: [
        expand: true
        cwd: "#{appConfig.paths.dist}"
        src: [
          "*.html"
          "views/**/*.html"
        ]
        dest: "#{appConfig.paths.dist}"
      ]

  # compile all html template to js
  html2js:
    options:
      base: '.tmp'
      module: 'veetaTalentApp.templates'
      singleModule: true
      useStrict: true
      htmlmin:
        collapseBooleanAttributes: true
        collapseWhitespace: true
        removeAttributeQuotes: true
        removeComments: true
        removeEmptyAttributes: true
        removeRedundantAttributes: true
        removeScriptTypeAttributes: true
        removeStyleLinkTypeAttributes: true
    main:
      src: ["#{appConfig.paths.tmp}/views/**/*.html"]
      dest: "#{appConfig.paths.tmp}/scripts/template-cache.js"


  # ng-annotate tries to make the code safe for minification automatically
  # by using the Angular long form for dependency injection.
  ngAnnotate:
    dist:
      files: [
        expand: true
        cwd: "#{appConfig.paths.tmp}/concat/scripts"
        src: [
          "*.js"
          "!oldieshim.js"
        ]
        dest: "#{appConfig.paths.tmp}/concat/scripts"
      ]


  # Replace Google CDN references
  cdnify:
    dist:
      html: ["#{appConfig.paths.dist}/*.html"]


  # Copies remaining files to places other tasks can use
  copy:
    dist:
      files: [
        {
          expand: true
          dot: true
          cwd: "#{appConfig.paths.app}"
          dest: "#{appConfig.paths.dist}"
          src: [
            "*.{ico,png,txt}"
            ".htaccess"
            "*.html"
            "views/{,*/}*.html"
            "images/{,*/}*.{webp}"
            "fonts/**/*.{eot,svg,ttf,woff}"
          ]
        }
        {
          expand: true
          cwd: "#{appConfig.paths.tmp}"
          src: "views/**/*.html"
          dest: "#{appConfig.paths.dist}"
        }
        {
          expand: true
          cwd: "#{appConfig.paths.tmp}"
          src: "*.html"
          dest: "#{appConfig.paths.dist}"
        }
        {
          expand: true
          cwd: "/images"
          dest: "#{appConfig.paths.dist}/images"
          src: ["generated/*"]
        }
        {
          expand: true
          cwd: "."
          src: "bower_components/bootstrap-sass-official/assets/fonts/bootstrap/*"
          dest: "#{appConfig.paths.dist}"
        }
        {
          expand: true
          cwd: "."
          src: "bower_components/font-awesome/fonts/*"
          dest: "#{appConfig.paths.dist}"
        }
      ]

    styles:
      expand: true
      cwd: "#{appConfig.paths.app}/styles"
      dest: "#{appConfig.paths.tmp}/styles/"
      src: "{,*/}*.css"

