angular.module("veetaTalentApp.desktop.controllers")
.controller "WelcomeCtrl", ($scope, $state,$rootScope, $auth, FlashService, OptionsService) ->


  $scope.talentAuth =
    resumeType: 1

  $scope.termOptions =
    checkbox:
      text: 'By clicking Sign Up you agree to our Terms & Conditions'

  $scope.countryOptions = OptionsService.countryOptions

  $scope.signUp = ->
    talentAuth = {}

    talentAuth.first_name = $scope.talentAuth.firstName if $scope.talentAuth.firstName
    talentAuth.last_name = $scope.talentAuth.lastName if $scope.talentAuth.lastName
    talentAuth.email = $scope.talentAuth.email if $scope.talentAuth.email
    talentAuth.password = $scope.talentAuth.password if $scope.talentAuth.password
    talentAuth.country_id = $scope.talentAuth.country.id if $scope.talentAuth.country and $scope.talentAuth.country.id
    talentAuth.resume_type = $scope.talentAuth.resumeType if $scope.talentAuth.resumeType

    $auth.submitRegistration(talentAuth)
    .then ->
      $state.go 'app.inside.jobapps.overview'
    .catch (rejection) ->
      if rejection.errors? and rejection.errors.length > 0
        error = rejection.data.errors[0]
        $rootScope.$broadcast 'flash:warning', error

      if rejection.data? and rejection.data.errors? and rejection.data.errors.full_messages and rejection.data.errors.full_messages.length > 0
        error = rejection.data.errors.full_messages[0]
        $rootScope.$broadcast 'flash:warning', error
