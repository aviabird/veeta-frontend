"use strict"

###*
  # @ngdoc directive
  # @name account.directives:talent-profile
  #
  # @requires ACCOUNT
  # @requires $auth
  # @requires $state
  # @requires Talent
  # @requires FileUploader
  # @requires $modal
  # @requires $log
  # @requires $rootScope
  # @requires OptionsService
  #
  # @restrict AE
  #
  # @description
  # form for talent setting
  # be used on talent setting page and resume edit page
###

angular.module "veetaTalentApp.account.directives"
.directive "talentProfile", (ACCOUNT
                             $auth
                             $state
                             Talent
                             FileUploader
                             $modal
                             $log
                             $rootScope
                             $filter
                             OptionsService) ->
  restrict: "AE"
  templateUrl: ACCOUNT.PARTIALS.TALENT_PROFILE
  replace: true
  scope: {
    #TODO: That can be string
    editOnly: '='
  }
  controller: ($scope) ->
    # ----------------------------------------
    # Initialize attributes
    # ----------------------------------------

    # get talent object from rootScope
    $scope.talent = angular.copy($rootScope.talent)
    $scope.talentOnEdit = angular.copy($scope.talent)

    # set default editMode
    $scope.editMode = (($scope.editOnly is true) || ($scope.talent.guest is true))

    # set options
    $scope.genderOptions = OptionsService.genderOptions
    $scope.salutationOptions = OptionsService.salutationOptions
    $scope.countryOptions = OptionsService.countryOptions
    $scope.stateOptions = OptionsService.stateOptions
    $scope.birthdateOptions =
      showButtonBar: "false"
      datepickerMode: "'year'"
      showWeeks: "false"
      yearRange: "30"

    $scope.birthdateOpened = false

    # config avatar uploader
    avatarUploader = $scope.avatarUploader = new FileUploader
      url: CONFIG.apiTalentUrl + "/talent/avatar_upload"
      autoUpload: true
      headers: $auth.retrieveData('auth_headers')
      alias: "talent[avatar]"

    # ----------------------------------------
    # Public methods
    # ----------------------------------------

    $scope.isOnEditMode = ->
      $scope.editMode is true or $scope.talent.phoneNumber is null

    # Unused method
    $scope.onSave = ->
      modalInstance = $modal.open
        templateUrl: ACCOUNT.PARTIALS.MODAL.UPDATE_CONFIRMATION
        controller: 'AccountUpdateConfirmationModalCtrl'
        size: 'sm'
        resolve:
          talent: ->
            $scope.talentOnEdit

      modalInstance.result.then (talent)->
        $rootScope.talent = $scope.talent = $scope.talentOnEdit
        $scope.exitEditMode() unless $scope.editOnly is true

    $scope.save = ->
      if $scope.isOnEditMode()
        $rootScope.talent = $scope.talent = $scope.talentOnEdit
        delete $scope.talent.id
        $scope.talent.update()

    $scope.onEditMode = ->
      $scope.talentOnEdit = angular.copy($scope.talent)
      $scope.editMode = true

    $scope.exitEditMode = ->
      $scope.editMode = false

    $scope.removeAvatar = ->
      $scope.talent.removeAvatar().then (response)->
        $rootScope.talent.avatarUrl = $scope.talent.avatarUrl = response.talentAvatar.avatarUrl


    $scope.birthdateOpen = ($event) ->
      $event.preventDefault()
      $event.stopPropagation()
      $scope.birthdateOpened = true

    # Hide cancel button, when the user doesn't have address (he didn't fill out all informations yet)
    $scope.hiddenCancelButton = ->
      $scope.talent.addressId is null

    # ----------------------------------------
    # Events
    # ----------------------------------------

    $scope.$on "countryOptions:loaded", (event, data) ->
      result = $filter('first')($scope.countryOptions.collection, 'id === ' + $scope.talentOnEdit.nationalityCountryId)
      $scope.talentOnEdit.nationalityCountry = result

    $scope.$on 'resume:save', ->
      $scope.save()

    # Successful callback of avatar uploader
    avatarUploader.onSuccessItem = (item, response, status, headers) ->
      if response.talent_avatar
        $rootScope.talent.avatarUrl = $scope.talent.avatarUrl = response.talent_avatar.avatar_url

.controller 'AccountUpdateConfirmationModalCtrl', ($scope, $modalInstance, talent) ->
  $scope.update = ->
    delete talent.id
    talent.update().then ->
      $modalInstance.close(talent)

  $scope.cancel = ->
    $modalInstance.dismiss 'cancel'
