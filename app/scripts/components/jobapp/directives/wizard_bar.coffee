"use strict"

###*
  # @ngdoc directive
  # @name jobapp.directives:jobapp-wizard-bar
  #
  # @requires JOBAPP
  #
  # @description
  # jobapp wizard bar, show the jobapp assistant process
  # be used on jobapp assistant page
###

angular.module 'veetaTalentApp.jobapp.directives'
.directive "jobappWizardBar", (JOBAPP) ->
  restrict: "AE"
  templateUrl: JOBAPP.PARTIALS.WIZARD_BAR
  replace: true
  scope:
    step: '='
    resumesLength: '='