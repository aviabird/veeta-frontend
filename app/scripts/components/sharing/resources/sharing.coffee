angular.module("veetaTalentApp.sharing.resources")
.factory "Sharing", (railsResourceFactory) ->
  resource = railsResourceFactory
    url: CONFIG.apiTalentUrl + "/sharing"
    name: "sharing"

  resource.$revoke = (company_id) ->
    resource.$put resource.$url("revoke/" + company_id)

  resource
