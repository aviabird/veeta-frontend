angular.module "veetaTalentApp.resume.resources"
.factory "ResumeLinks", (railsResourceFactory) ->
  resource = railsResourceFactory
    url: CONFIG.apiTalentUrl + '/resume/link'
    name: 'link'
    serializer: 'ResumeLinksSerializer'
    interceptors: ['belongsToResume']

  resource::update = ->
    @$put(resource.$url())

  resource

.factory "ResumeLinksSerializer", (railsSerializer) ->
  railsSerializer ->
    @only "linkedin", "xing", "website"