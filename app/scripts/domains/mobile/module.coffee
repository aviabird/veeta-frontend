###*
  # @ngdoc overview
  # @name mobile
  #
###


angular.module 'veetaTalentApp.mobile', [
  # common module
  'veetaTalentApp.common'

  # components
  'veetaTalentApp.account'
  'veetaTalentApp.jobapp'
  'veetaTalentApp.resume'

  # modules
  'veetaTalentApp.mobile.controllers'
  'veetaTalentApp.mobile.directives'
]

angular.module 'veetaTalentApp.mobile.controllers', []
angular.module 'veetaTalentApp.mobile.directives', []