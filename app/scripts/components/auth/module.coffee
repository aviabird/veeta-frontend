###*
  # @ngdoc overview
  # @name auth
  #
###

angular.module 'veetaTalentApp.auth', [
  'veetaTalentApp.auth.controllers',
  'veetaTalentApp.auth.directives',
  'veetaTalentApp.auth.resources'
]

angular.module 'veetaTalentApp.auth.controllers', []
angular.module 'veetaTalentApp.auth.directives', [
  'ui.bootstrap'
]
angular.module 'veetaTalentApp.auth.resources', [
  'rails'
]
