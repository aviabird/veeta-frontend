###*
  # @ngdoc controller
  # @name jobapp.controllers:JobCtrl
  #
  # @requires $scope
  # @requires $modal
  # @requires jobapp
  # @requires JOBAPP
  #
  # @description
  # Controller for Job Detail
###

angular.module 'veetaTalentApp.jobapp.controllers'
.controller 'JobCtrl', ($scope, $state, $auth, job, OptionsService)->
  $scope.job = job
  $scope.talentAuth =
    careerLevel: 6
  $scope.authMode = 'signUp'

  $scope.countryOptions = OptionsService.countryOptions

  $scope.setAuthMode = (mode) ->
    $scope.authMode = mode

  $scope.signInApply = ->
    talentAuth =
      email: $scope.talentAuth.email
      password: $scope.talentAuth.password

    $auth.submitLogin(talentAuth)

  $scope.signUpApply = ->
    talentAuth =
      email: $scope.talentAuth.email
      resume_type: $scope.talentAuth.careerLevel
      is_guest: true

    talentAuth.country_id = $scope.talentAuth.country.id if $scope.talentAuth and $scope.talentAuth.country
    $auth.submitRegistration(talentAuth)

  $scope.$on 'auth:login-success', ->
    $state.go 'app.inside.jobappAssistant', {jobId: job.id}

  $scope.$on 'auth:registration-email-success', ->
    $state.go 'app.inside.jobappAssistant', {jobId: job.id}

  $scope.$on 'auth:registration-email-error', (ev, reason) ->
    $scope.loginError = reason.errors.email[0]
