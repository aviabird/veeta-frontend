'use strict';

angular.module 'veetaTalentApp.common'
.config ($easyInputProvider) ->
  $easyInputProvider.registerInput 'month-select',
    templateUrl: 'easy-form/templates/inputs/month-select.html'


angular.module 'veetaTalentApp.common'
.run ($templateCache) ->
  $templateCache.put "easy-form/templates/inputs/month-select.html",
      "<month-select name=\"inputIn\" ng-model=\"model\" allow-now=\"options\.monthSelect\.allowNow\" ng-disabled=\"ngDisabled\"/>"


