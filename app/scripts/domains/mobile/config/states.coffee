angular.module("veetaTalentApp.mobile")
.config ($stateProvider
         $urlRouterProvider
         COMMON
         LAYOUTS
         JOBAPP) ->
  $urlRouterProvider.otherwise "/404"

  $stateProvider
  .state "app",
    abstract: true
    templateUrl: LAYOUTS.VIEWS.MOBILE
    controller: "AppCtrl"
    resolve:
      options: (OptionsService)->
        OptionsService.init()

  .state "app.notFound",
    url: "/404"
    templateUrl: COMMON.VIEWS.NOT_FOUND

  .state 'app.job',
    url: '/jobs/:jobToken'
    templateUrl: JOBAPP.MOBILE.VIEWS.JOB
    controller: 'JobCtrl'
    resolve:
      job: (Job, $stateParams) ->
        Job.get($stateParams.jobToken).then (response)->
          response

  .state "app.inside",
    abstract: true
    template: '<ui-view />'
    controller: 'InsideCtrl'
    resolve:
      auth: ($rootScope, $timeout, $q, $auth, Talent, TalentSetting)->

        # REFACTOR dirty code, but works
        deferred = $q.defer()

        dTasksDone = 0

        auth = {}

        _doAuth = ->
          $auth.validateUser().then (response)->
            auth = response

            Talent.query().then (response) ->
              auth.talent = response
              dTasksDone = dTasksDone + 1
              deferred.resolve(auth) if dTasksDone is 2
            TalentSetting.query().then (response) ->
              auth.talentSetting = response
              dTasksDone = dTasksDone + 1
              deferred.resolve(auth) if dTasksDone is 2
          , (reject) ->
            deferred.reject(reject)

        $timeout _doAuth, 300

        deferred.promise

  .state "app.inside.jobappAssistant",
    url: '/jobapp-assistant/:jobId'
    templateUrl: JOBAPP.MOBILE.VIEWS.NEW
    controller: "JobappNewCtrl"
    resolve:
      jobapp: ($q, $timeout, Jobapp, $rootScope, $stateParams, $state) ->
        deferred = $q.defer()
        Jobapp.getNew($stateParams.jobId).then (response) ->
          if response.job and response.job.id and response.jobId
            if response.job.available
              deferred.resolve(response)
            else
              $timeout $rootScope.$broadcast('$window.redirectTo', URL.BASE + '/jobs/' + response.job.id), 10000
          else
            $state.go('app.notFound')
        , ->
          $state.go('app.notFound')

        return deferred.promise

  .state "app.inside.jobappAssistant.resume",
    url: '/resume'
    template: '<ui-view />'
    controller: 'JobappNewResumeCtrl'
    resolve:
      pageRefreshed: (jobapp, $stateParams) ->
        $state.go('app.jobappAssistant', { jobId: $stateParams.jobId } ) unless jobapp.jobId

  .state "app.inside.jobappAssistant.resume.select",
    url: "/select"
    templateUrl: JOBAPP.MOBILE.VIEWS.NEW_SELECT_RESUME
    controller: "JobappNewResumeSelectCtrl"
    resolve:
      pageRefreshed: (jobapp, $stateParams) ->
        $state.go('app.jobappAssistant', { jobId: $stateParams.jobId } ) unless jobapp.jobId

  .state "app.inside.jobappAssistant.resume.new",
    url: "/new"
    templateUrl: JOBAPP.MOBILE.VIEWS.NEW_RESUME_DETAIL
    controller: "JobappNewResumeDetailCtrl"
    resolve:
      resume: (Resume) ->
        Resume.get("new").then (response) ->
          response
      pageRefreshed: (jobapp, $stateParams) ->
        $state.go('app.jobappAssistant', { jobId: $stateParams.jobId } ) unless jobapp.jobId

  .state "app.inside.jobappAssistant.resume.detail",
    url: "/:resumeId"
    templateUrl: JOBAPP.MOBILE.VIEWS.NEW_RESUME_DETAIL
    controller: "JobappNewResumeDetailCtrl"
    resolve:
      resume: (Resume, $stateParams) ->
        Resume.get($stateParams.resumeId).then (response) ->
          response
      pageRefreshed: (jobapp, $stateParams) ->
        $state.go('app.jobappAssistant', { jobId: $stateParams.jobId } ) unless jobapp.jobId

  .state "app.inside.jobappAssistant.addCoverLetter",
    url: "/add_cover_letter"
    templateUrl: JOBAPP.MOBILE.VIEWS.NEW_ADD_COVER_LETTER
    controller: "JobappNewAddCoverLetterCtrl"
    resolve:
      pageRefreshed: (jobapp, $stateParams) ->
        $state.go('app.jobappAssistant', { jobId: $stateParams.jobId } ) unless jobapp.jobId

  .state "app.inside.jobappAssistant.checkAndSend",
    url: "/check_and_send"
    templateUrl: JOBAPP.MOBILE.VIEWS.NEW_CHECK_AND_SEND
    controller: "JobappNewCheckAndSendCtrl"
    resolve:
      pageRefreshed: (jobapp, $stateParams) ->
        $state.go('app.jobappAssistant', { jobId: $stateParams.jobId } ) unless jobapp.jobId

  .state 'app.inside.jobappAssistant.finished',
    url: '/finished'
    templateUrl: JOBAPP.MOBILE.VIEWS.NEW_FINISHED
    controller: 'JobappNewFinishedCtrl'
    resolve:
      pageRefreshed: (jobapp, $stateParams) ->
        $state.go('app.jobappAssistant', { jobId: $stateParams.jobId } ) unless jobapp.jobId