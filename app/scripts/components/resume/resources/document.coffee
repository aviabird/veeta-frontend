angular.module "veetaTalentApp.resume.resources"
.factory "ResumeDocument", (railsResourceFactory) ->
  railsResourceFactory
    url: CONFIG.apiTalentUrl + '/resume/documents'
    name: 'document'
    interceptors: ['belongsToResume']