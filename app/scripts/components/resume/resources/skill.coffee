"use strict"
angular.module "veetaTalentApp.resume.resources"
.factory "ResumeSkill", (railsResourceFactory) ->
  railsResourceFactory
    url: CONFIG.apiTalentUrl + "/resume/skills"
    name: "skill"
    serializer: "ResumeSkillSerializer"
    interceptors: ["belongsToResume"]

.
factory "ResumeSkillSerializer", (railsSerializer) ->
  railsSerializer ->
    @exclude "resumeId"
