"use strict"

###*
  # @ngdoc controller
  # @name common.controllers:InsideCtrl
  #
  # @requires $scope
  # @requires $rootScope
  # @requires $timeout
  # @requires auth
  # @requires FlashService
  #
  # @description
  # a parent controller of inside application
###

angular.module("veetaTalentApp.common.controllers")
.controller "InsideCtrl", ($scope, $rootScope, $timeout, auth, FlashService) ->
  $rootScope.talent = auth.talent

  $rootScope.talentSetting = auth.talentSetting
