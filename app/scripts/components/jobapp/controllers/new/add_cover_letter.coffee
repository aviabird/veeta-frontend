###*
  # @ngdoc controller
  # @name jobapp.controllers:JobappNewAddCoverLetterCtrl
  #
  # @requires $scope
  # @requires $state
  #
  # @description
  # Controller for add cover letter for jobapp creating
###

angular.module 'veetaTalentApp.jobapp.controllers'
.controller 'JobappNewAddCoverLetterCtrl', ($anchorScroll,
                                            $location,
                                            $scope,
                                            $state,
                                            OptionsService) ->
  $parentScope = $scope.$parent
  $parentScope.step = 2
  $scope.jobapp = $parentScope.jobapp
  $scope.newBlockedCompany = ''
  $scope.jobSourceOptions = OptionsService.referrerOptions

  _gotoTop = ->
    $location.hash('jobapp-new-add-cover-letter')
    $anchorScroll()

  $scope.goPreviousStep = ->
    $state.go '^.resume.detail', {resumeId: $scope.jobapp.resumeId}

  $scope.goNextStep = ->
    $state.go '^.checkAndSend'

  # check if user can go to next step
  $scope.canGoNextStep = () ->
    $scope.jobappCoverLetterForm.$valid

  ###*
  add blocked company
  ###
  $scope.addBlockedCompany = (blockedCompany)->
    $scope.jobapp.blocked.push angular.copy(blockedCompany)
    $scope.newBlockedCompany = ''

  ###*
  remove blocked company
  ###
  $scope.removeBlockedCompany = (blockedCompany) ->
    $scope.jobapp.blocked = $scope.jobapp.blocked.filter (company) ->
      company isnt blockedCompany

  _gotoTop()
