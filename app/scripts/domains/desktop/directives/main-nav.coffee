"use strict"

###*
  # @ngdoc directive
  # @name desktop.directives:main-nav
  #
  # @requires DESKTOP
  #
  # @description
  # application main nav
  # be used on desktop application layout
###

angular.module("veetaTalentApp.desktop.directives")
.directive "mainNav", (DESKTOP) ->
  restrict: "AE"
  templateUrl: DESKTOP.PARTIALS.MAIN_NAV
  replace: true
  controller: ($scope, $state) ->
    $scope.isBelongsToState = (state) ->
      $state.includes(state)

    $scope.isShow = ->
      true unless $state.includes("**.jobappAssistant.**") or
        $state.includes('app.welcome') or
        $state.includes('app.signIn') or
        $state.includes('app.signUp') or
        $state.includes('app.requestPassword') or
        $state.includes('app.resetPassword') or
        $state.includes('app.job')
