'use strict';

# TODO: Move to namespaced API
angular.module 'veetaTalentApp.common.resources'
.factory 'Language', (railsResourceFactory) ->
  resource = railsResourceFactory
    url: CONFIG.apiUrl + '/languages'
    name: 'language'
  resource