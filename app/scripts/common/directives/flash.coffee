"use strict"

###*
  # @ngdoc directive
  # @name common.directives:app-flash
  #
  # @requires COMMON
  #
  # @restrict AE
  #
  # @description
  # show flash message if there is a message in FlashService
  # be used on application layout
###

angular.module("veetaTalentApp.common.directives")
.directive "appFlash", (COMMON) ->
  restrict: "AE"
  templateUrl: COMMON.PARTIALS.FLASH
  replace: true
  controller: ($scope, $rootScope, FlashService) ->

    $scope.flashMessage =
      title: ''
      body: ''

    $scope.isMessageExisted = ->
      $scope.flashMessage and ($scope.flashMessage.title.length > 0 or
        $scope.flashMessage.body.length > 0)


    $rootScope.$on '$stateChangeSuccess', ->
      $scope.flashMessage = FlashService.getMessage()
