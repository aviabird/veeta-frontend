angular.module 'veetaTalentApp.resume.resources'
.factory 'Resume', (railsResourceFactory
                    ResumeEducation
                    ResumeWorkExperience
                    ResumeLanguage
                    ResumeSkill
                    ResumeSharing
                    ResumeLinks
                    ResumeDocument) ->

  resource = railsResourceFactory
    url: CONFIG.apiTalentUrl + '/resumes'
    name: 'resume'
    serializer: 'ResumeSerializer'

  resource::updateName = ->
    resource.$put(@$url(), {name: @name})


  resource::getWorkExperiences = ->
    ResumeWorkExperience.query({resumeId: this.id}).then (data) ->
      @workExperiences = data

  resource::newWorkExperience = ->
    self = this
    new ResumeWorkExperience({resumeId: self.id})

  resource.prototype.getEducations = ->
    self = this
    ResumeEducation.query({resumeId: this.id}).then (data) ->
      self.educations = data
      self.educations

  resource.prototype.newEducation = ->
    self = this
    new ResumeEducation({resumeId: self.id})


  resource::getDocuments = ->
    ResumeDocument.query({resumeId: this.id}).then (data) ->
      @languages = data


  resource::getLanguages = ->
    self = this
    ResumeLanguage.query({resumeId: this.id}).then (data) ->
      self.languages = data
      self.languages


  resource::newLanguage = ->
    self = this
    new ResumeLanguage({resumeId: self.id})

  resource::addLanguage = (language) ->
    self= this;
    language.resumeId = @id
    language.save().then (data) ->
      self.languages.push(data)
      data


  resource::removeLanguage = (language) ->
    self = this;
    language.remove().then (data) ->
      index = self.languages.indexOf(language)
      if (index > -1)
        self.languages.splice(index, 1)
      data


  resource::getSkills = ->
    self = this
    ResumeSkill.query({resumeId: this.id}).then (data) ->
      self.skills = data
      self.skills

  resource::newSkill = ->
    self = this
    new ResumeSkill({resumeId: self.id})

  resource::addSkill = (skill) ->
    self = this
    skill.resumeId = self.id
    skill.save().then (data) ->
      self.skills.push(data)
      data

  resource::removeSkill = (skill) ->
    self = this
    skill.remove().then (data) ->
      index = self.skills.indexOf(skill)
      if (index > -1)
        self.skills.splice(index, 1)
      data

  resource::getLinks = ->
    ResumeLinks.query({resumeId: @id})

  resource::newSharing = ->
    new ResumeSharing({resumeId: @id})

  resource

.factory 'ResumeSerializer', (railsSerializer) ->
  railsSerializer ->
    @only('name', 'personalStatement', 'languageId', 'talent')
    @nestedAttribute('talent')
    @serializeWith('talent', 'TalentSerializer')
    @add "languageId", (jobapp) ->
      jobapp.language.id  if jobapp.language

.factory 'belongsToResume', ->
  'request': (httpConfig, resourceConstructor, context) ->
    if context && context.resumeId
      if (httpConfig.method == 'post' || httpConfig.method == 'put' || httpConfig.method == 'patch')
        if !httpConfig.data
          httpConfig.data = {}
        httpConfig.data['resume_id'] = context.resumeId

      if (httpConfig.method == 'get' || httpConfig.method == 'delete')
        if !httpConfig.params
          httpConfig.params = {}
        httpConfig.params['resume_id'] = context.resumeId
    httpConfig
