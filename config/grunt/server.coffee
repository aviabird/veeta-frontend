module.exports = (grunt, appConfig) -> tasks:
  watch:
    livereload:
      options:
        livereload: "<%= connect.options.livereload %>"
        spawn: false

      files: [
        "#{appConfig.paths.app}/{,*/}*.html"
        "#{appConfig.paths.tmp}/styles/{,*/}{,*/}{,*/}*.css"
        "#{appConfig.paths.tmp}/scripts/**/*.js"
        "#{appConfig.paths.tmp}/views/**/*.html"
        "#{appConfig.paths.app}/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}"
      ]

  # The actual grunt server settings
  connect:
    options:
      port: 9000
    # Change this to '0.0.0.0' to access the server from outside.
      hostname: "localhost"
      livereload: 35729

    livereload:
      options:
        open: true
        middleware: (connect) ->
          [
            connect.static(".tmp")
            connect().use("/bower_components", connect.static("./bower_components"))
            connect.static(appConfig.paths.app)
          ]

    test:
      options:
        port: 9001
        middleware: (connect) ->
          [
            connect.static(".tmp")
            connect.static("test")
            connect().use("/bower_components", connect.static("./bower_components"))
            connect.static(appConfig.paths.app)
          ]

    dist:
      options:
        open: true
#        base: "#{appConfig.paths.dist}"
        middleware: (connect) ->
          [
            connect.static(appConfig.paths.dist)
          ]
  # Run some tasks in parallel to speed up the build process
  concurrent:
    server: [
      "coffee:dist"
      "jade"
      "compass:server"
    ]
    test: [
      "coffee"
      "jade"
      "compass:server"
    ]
    dist: [
      "coffee"
      "compass:dist"
      "imagemin"
      "svgmin"
    ]

  # Record, mock, and proxy HTTP requests
  prism:
    options:
      mode: 'proxy'
      mocksPath: './mocks'
      https: false

    server:
      options:
        mode: 'record'
        context: '/api'
        host: 'localhost'
        port: 3000
