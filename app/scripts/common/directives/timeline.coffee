"use strict"

###*
  # @ngdoc directive
  # @name common.directives:timeline
  #
  # @requires COMMON
  # @requires $modal
  # @requires $compile
  # @requires $filter
  #
  # @restrict AE
  #
  # @description
  # create a editable timeline from events array
  # be used on resume edit page
  # TODO: Refactor this code someday
###

angular.module("veetaTalentApp.common.directives")
.directive "timeline", (COMMON, $modal, $compile, $filter) ->
  # Move private methods here (they don't have access to scope) 
  # In this case we need to return whole directive object:
  # return(..)
  restrict: "AE"
  templateUrl: COMMON.PARTIALS.TIMELINE
  required: "ngModel"
  scope:
    events: "=ngModel"
    # That could be object one-way bidning
    # Example:
    # ... origin-new-event='::newEducation' ...
    originNewEvent: "=newNgModel"
    # Options and editable can be cust one-way binded
    options: "="
    editable: "="
    newCallback: "&"
    queryCallback: "&"
    getCallback: "&"
    saveCallback: "&"
    removeCallback: "&"
    formDirective: "@"
  replace: true
  # TODO:
  # Move functions cotaining css styles from controller to link
  controller: ($scope) ->
    # ----------------------------------------
    # Initialize attributes
    # ----------------------------------------
    # Constants -> Uppercase
    min_radius = 30
    max_radius = 100
    radius_gradient = 0.2

    $scope.thisYear = moment().format("YYYY")
    $scope.eventName = $scope.options.eventName
    $scope.eventInEdit = {}

    # ----------------------------------------
    # Private methods
    # ----------------------------------------

    duration = (to, from) ->
      toDate = if to? then new Date(to) else new Date()
      fromDate = if from? then new Date(from) else new Date()
      durationDate = new Date(toDate - fromDate)
      Math.abs(durationDate.getUTCFullYear() - 1970) + durationDate.getMonth() / 12

    yearsToRadius = (years) ->
      max_radius - (max_radius - min_radius) * Math.exp(-years * radius_gradient)

    prettyDate = (date) ->
      if date? then moment(date).format("MMM YYYY") else 'now'

    initNewEvent = ->
      $scope.newEvent = angular.copy($scope.originNewEvent)
    # ----------------------------------------
    # public methods
    # ----------------------------------------

    $scope.isInEditMode = (event) ->
      $scope.eventInEdit is event

    $scope.printDuration = (event) ->
      prettyDate(event.from) + ' - ' + prettyDate(event.to)

    $scope.printHeading = (event) ->
      event[$scope.options.heading]

    $scope.printSubHeading = (event) ->
      subHeading = $scope.options.subHeading.split(/[ ,]+/)
      out1 = event[subHeading[0]] if subHeading.length > 0
      out1 = out1[subHeading[1]] if subHeading.length > 1

      if $scope.options.subHeading2?
        subHeading2 = $scope.options.subHeading2.split(/[ ,]+/)
        out2 = event[subHeading2[0]] if event[subHeading2[0]]? and subHeading2.length > 0
        out2 = out2[subHeading2[1]] if out2? and subHeading2.length > 1 and out2[subHeading2[1]]
        out1 = out1 + ', ' + out2 if out2?
      out1

    $scope.onEdit = (event) ->
      initNewEvent() if event isnt $scope.newEvent
      $scope.eventInEdit = event

    $scope.onRemove = (event) ->
      modalInstance = $modal.open
        templateUrl: COMMON.PARTIALS.ITEM_REMOVE_MODAL
        controller: 'ItemRemoveModalCtrl'
        size: 'sm'
        resolve:
          item: ->
            event

      modalInstance.result.then (event)->
        idx = $scope.events.indexOf(event)
        $scope.events.splice(idx, 1)

    $scope.timelineIconStyle = (event, $index) ->
      # open edit form, if there is no events available

      # f(x) = 90−60∙exp(−x/5)
      return  unless event?
      yearsDiff = duration(event.to, event.from)
      left = undefined
      right = undefined
      radius = yearsToRadius(yearsDiff) / 2
      shift = -56 - radius
      borderWidth = 2
      if $index % 2 is 0
        left = "auto"
        right = (shift + borderWidth) + "px"
      else
        left = (shift - borderWidth) + "px"
        right = "auto"
      left: left
      right: right
      "margin-top": -radius + borderWidth + "px"

    $scope.timeIconStyle = (event) ->
      return  unless event?
      yearsDiff = duration(event.to, event.from)
      width: yearsToRadius(yearsDiff) + "px"
      height: yearsToRadius(yearsDiff) + "px"
      "background-color": $scope.icon_backgroud_color

    $scope.maxYear = ->
      if !$scope.events? or $scope.events.length < 1
        $scope.thisYear
      else
        events = $filter('orderBy')($scope.events, 'to', true)
        if events[0].to
          newestDate = new Date(events[0].to)
          newestDate.getUTCFullYear()
        else
          $scope.thisYear


    $scope.minYear = ->
      if $scope.events? and $scope.events.length > 0
        events = $filter('orderBy')($scope.events, 'from')
        if events[0].from
          oldestDate = new Date(events[0].from)
          oldestDate.getUTCFullYear()
        else
          null
      else
        null

    $scope.eventOrder = (event) ->
      if event.to? then new Date(event.to) else new Date()


    # ----------------------------------------
    # Events
    # ----------------------------------------

    $scope.$watchCollection "events", (newVal, oldVal) ->
      $scope.eventInEdit = if $scope.events.length < 1 then $scope.newEvent else {}

    $scope.$on "eventExitEditMode", ->
      initNewEvent()
      $scope.eventInEdit = {}


    # ----------------------------------------
    # Initialize methods
    # ----------------------------------------
    initNewEvent()

.directive "timelineForm", ($compile) ->
  restrict: "E"
  template: "<div class=\"timeline-form-wrapper\" ng-model=\"model\"></div>"
  required: "ngModel"
  scope:
    model: "=ngModel"
    formDirective: "="

  link: (scope, element, attrs) ->
    element.find(".timeline-form-wrapper").attr scope.formDirective, ""
    $compile(element.find(".timeline-form-wrapper")) scope

