"use strict"

###*
  # @ngdoc directive
  # @name account.directives:account-delete
  #
  # @requires $rootScope
  # @requires $translate
  # @requires $modal
  # @requires ACCOUNT
  # @requires TalentAuth
  #
  # @restrict AE
  #
  # @description
  # Block for account deleting
  # be used on bottom of talent setting page
###


angular.module("veetaTalentApp.account.directives")
.directive "accountDelete", ($rootScope, $translate, $modal, ACCOUNT, TalentAuth) ->

  # TODO: Move translations to OptionsService
  getTranslations = ($scope)->
    $translate([
      "talent.form.account_delete.reason.job_found"
      "talent.form.account_delete.reason.not_happy"
    ]).then (translations) ->
      $scope.deleteReasonOptions = [
        {
          value: 'job_found',
          text: translations["talent.form.account_delete.reason.job_found"]
        },
        {
          value: 'not_happy',
          text: translations["talent.form.account_delete.reason.not_happy"]
        }
      ]

  return(
    restrict: "AE"
    templateUrl: ACCOUNT.PARTIALS.DELETE
    replace: true
    scope: {}
    controller: ($scope) ->
      # initial talentAuth instance, ensures the attributes initialization
      $scope.talentAuth = new TalentAuth
        deleteReason: ''
        withOtherDeleteReason: false
        otherDeleteReason: ''
        deleteAgreement: false

      $scope.deleteReasonOptions = []

      $scope.deleteAccountMode = false

      getTranslations($scope)
      $rootScope.$on "$translateChangeSuccess", ->
        getTranslations($scope)

      $scope.onDeleteAccountMode = ->
        $scope.deleteAccountMode = true

      $scope.onCancelDeleteAccountMode = ->
        $scope.deleteAccountMode = false

      $scope.deleteButtonEnabled = ->
        $scope.talentAuth.deleteAgreement is true && ($scope.talentAuth.deleteReason isnt '' || ($scope.talentAuth.withOtherDeleteReason && $scope.talentAuth.otherDeleteReason != ''))

      $scope.$watch 'talentAuth.deleteReason', (newVal, oldVal) ->
        unless newVal == oldVal
          $scope.talentAuth.withOtherDeleteReason = (newVal is null)

      $scope.selectOtherDeleteReason = ->
        $scope.talentAuth.deleteReason = ''

      $scope.openDeleteAccountConfirmationModal = ->
        unless $scope.deleteButtonEnabled() then return
        modalInstance = $modal.open
          templateUrl: ACCOUNT.PARTIALS.MODAL.DELETE_CONFIRMATION
          controller: "accountDeleteConfirmationModalCtrl"
          resolve:
            talentAuth: ->
              $scope.talentAuth

        modalInstance.result.then ->
          $scope.talent.get()


  )
.controller 'accountDeleteConfirmationModalCtrl', ($scope, $modalInstance, talentAuth) ->
  $scope.talentAuth = talentAuth

  #TODO: Use $auth.destroyAccount (not currently possible)
  $scope.delete = ->
    $scope.talentAuth.deleteAccount().then ->
      $modalInstance.close()

  $scope.cancel = ->
    $modalInstance.dismiss "cancel"

