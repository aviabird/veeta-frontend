'use strict';

# TODO: Move to namespaced API
angular.module 'veetaTalentApp.common.resources'
.factory 'State', (railsResourceFactory) ->
  resource = railsResourceFactory
    url: CONFIG.apiUrl + '/states'
    name: 'state'
  resource