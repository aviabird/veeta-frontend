###*
  # @ngdoc controller
  # @name resume.controllers:ResumeDetailCtrl
  #
  # @requires $scope
  # @requires $stateParams
  # @requires FileUploader
  # @requires OptionsService
  # @requires resume
  #
  # @description
  # Controller for the Resume Detail
###


angular.module 'veetaTalentApp.resume.controllers'
.controller 'ResumeDetailCtrl', ($scope, $rootScope, $state, $stateParams, $log, FileUploader, InflectorService, OptionsService, Resume, resume) ->

  # ----------------------------------------
  # Initialize attributes
  # ----------------------------------------
  $scope.resume = resume

  $scope.modules =
    language: true
#    import: true
    import: $state.includes('**.resumes.new')
    profile: true
    generalInformation: not $rootScope.talent.salutation? # check if user fill out personal data once
    workExperience: true
    education: true
    languages: true
    skills: true
    links: true
    documents: true
    addModule: true

  # ----------------------------------------
  # Private methods
  # ----------------------------------------

  # ----------------------------------------
  # Public methods
  # ----------------------------------------

  $scope.onSave = ->
    $scope.resume.update().then ->
      $scope.resume.get()

  $scope.save = ->
    $scope.$broadcast 'resume:save'

  $scope.isNew = ->
    $state.includes('**.resumes.new')

  # ----------------------------------------
  # Events
  # ----------------------------------------

  ## forward resume imported message
  $scope.$on 'resume:import-successful', (event, importedResume)->
    $scope.$broadcast 'resume:handle-imported-data', importedResume
