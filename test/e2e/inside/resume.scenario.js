'use strict';

var ResumeOverviewPage = require('./pages/resume/overview.page');
var HttpBackend = require('http-backend-proxy');
var resumesFullListResults = require('../../api_mocks/resumes/list.json');

xdescribe('Resume', function () {
    var page, proxy, returnValue1, returnValue2;

    var firstRun = true;

    beforeEach(function () {
        page = new ResumeOverviewPage();

        if(firstRun){
            firstRun = false;

            page = new ResumeOverviewPage();

            proxy = new HttpBackend(browser, {buffer: true});

            returnValue1 = proxy.whenGET('/url1').respond(200);
            returnValue2 = proxy.whenGET('/url2').respond(200);
        }
        proxy = new HttpBackend(browser, {buffer: true});
        $httpBackend.whenGET('/search&q=beatles').respond(200, resumesFullListResults);
    });

    describe('resumes list', function () {
        it('should list resumes', function () {
            expect(page.resumesList.count()).toEqual(3);
        });
    });
});