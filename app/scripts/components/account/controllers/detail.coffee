###*
  # @ngdoc controller
  # @name account.controllers:AccountDetailCtrl
  #
  # @requires $scope
  #
  # @description
  # Controller for Account Detail
###

angular.module("veetaTalentApp.account.controllers")
.controller "AccountDetailCtrl", ($scope, $filter) ->

  $scope.talentName = ->
    $filter('name')($scope.talent)