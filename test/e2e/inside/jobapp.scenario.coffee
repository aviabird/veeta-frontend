JobappOverviewPage = require './pages/jobapp/overview.page'

describe 'Jobapp', ->
  jobappOverviewPage = undefined

  beforeEach ->
    jobappOverviewPage = new JobappOverviewPage()
    jobappOverviewPage.get()

  describe 'has a overview page', ->
    it 'should has a list of jobapps', ->
      expect(jobappOverviewPage.jobappsList.count()).toEqual(3)