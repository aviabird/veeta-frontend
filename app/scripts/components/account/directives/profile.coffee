"use strict"

###*
  # @ngdoc directive
  # @name account.directives:account-profile
  #
  # @requires ACCOUNT
  #
  # @restrict AE
  #
  # @description
  # a wrapper of talent profile
  # within its html will use talent-profile directive
  # this directive prepare, if there is something different
  # be used on talent setting page
###

angular.module "veetaTalentApp.account.directives"
.directive "accountProfile", (ACCOUNT) ->
  restrict: "AE"
  templateUrl: ACCOUNT.PARTIALS.PROFILE
  replace: true
