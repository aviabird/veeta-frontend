angular.module "veetaTalentApp.common"
.factory 'FlashService', ($rootScope, $log) ->

  _message =
    title: ''
    body: ''
    iconName: ''

  return (
    message: _message

    getMessage: ->
      m = angular.copy(_message)
      _message =
        title: ''
        body: ''
        iconName: ''
      m

    setMessage: (message)->
      _message.title = message.title if message.title?
      _message.body = message.body if message.body?
      _message.iconName = message.iconName if message.iconName?
      $rootScope.$broadcast 'flashMessage:newMessage'
  )


