'use strict';

angular.module 'veetaTalentApp.common.resources'
.factory 'AddressSerializer', (railsSerializer) ->
  return railsSerializer ->
    # Use whitelist instead of black list (exclude -> include)
    @exclude('country')
    @add 'country_id', (address)->
      address.country.id if address.country?