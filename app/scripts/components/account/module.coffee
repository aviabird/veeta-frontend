###*
  # @ngdoc overview
  # @name account
  #
###

angular.module 'veetaTalentApp.account', [
  'veetaTalentApp.account.controllers',
  'veetaTalentApp.account.directives',
  'veetaTalentApp.account.resources'
]

angular.module 'veetaTalentApp.account.controllers', []
angular.module 'veetaTalentApp.account.directives', [
  'ui.bootstrap'
  'angular.filter'
]
angular.module 'veetaTalentApp.account.resources', [
  'rails'
]
