# Depreciated
angular.module 'veetaTalentApp.common.filters'
.filter 'utc', ->
  (val) ->
    date = new Date(val)
    new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(), date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds())

.filter 'datetimeDefault', ($filter) ->
  angularDateFilter = $filter('date')
  urcFilter = $filter('utc')
  (theDate) ->
    angularDateFilter(urcFilter(theDate), 'dd.MM.yyyy HH:mm')

