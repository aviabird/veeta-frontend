"use strict"

###*
  # @ngdoc directive
  # @name resume.directives:resume-education
  #
  # @requires ResumeEducation
  # @requires RESUME
  #
  # @description
  # education block of resume
  # be used on resume detail page
###


angular.module 'veetaTalentApp.resume.directives'
.directive 'resumeEducation', (ResumeEducation, RESUME) ->
  restrict: "AE"
  templateUrl: RESUME.PARTIALS.EDUCATIONS
  replace: true
  required: 'ngModel'
  scope: {}
  controller: ($scope) ->

    # ----------------------------------------
    # Initialize attributes
    # ----------------------------------------

    # init a empty workExperiences array
    $scope.educations = []

    # get new education Instance
    $scope.newEducation = $scope.$parent.resume.newEducation()

    $scope.educationsTimelineOptions =
      heading: 'subject',
      subHeading: 'schoolName',
      subHeading2: 'country, isoCode'
      eventName: 'education'

    # ----------------------------------------
    # Private methods
    # ----------------------------------------
    _loadEducations = ->
      $scope.$parent.resume.getEducations().then (data) ->
        $scope.educations = data

    _initEducations = ->
      $scope.educations.push new ResumeEducation(education) for education in $scope.$parent.resume.educations


    # ----------------------------------------
    # Events
    # ----------------------------------------
    $scope.$on 'timeline:events:toReloadList', ->
      _loadEducations()

    # set a listener for changing of parent resume
    $scope.$on 'resume:handle-imported-data', (event, importedResume)->
      for education in importedResume.educations
        w = new ResumeEducation(education)
        w.isJustImported = true
        $scope.educations.push w
      _initEducations()

    # load Educations from api
    _loadEducations()
#    _initEducations()

.directive 'resumeEducationForm', (RESUME, OptionsService) ->
  restrict: "AE"
  templateUrl: RESUME.PARTIALS.FORM.EDUCATION
  replace: true
  scope:
    originEducation: '=ngModel'
  controller: ($scope, $translate) ->

    # ----------------------------------------
    # Initialize attributes
    # ----------------------------------------
    $scope.education = angular.copy($scope.originEducation)

    $scope.countryOptions = OptionsService.countryOptions
    $scope.typeOfEducationOptions = OptionsService.educationOptions
    $scope.completedOptions =
      checkbox:
        # text: 'Completed'
        text: $translate.instant('talent.data.educations.completed')

    $scope.toOptions =
      monthSelect:
        allowNow: true


    # ----------------------------------------
    # Public methods
    # ----------------------------------------
    $scope.save = ->
      $scope.education.save().then ->
        angular.extend $scope.originEducation, $scope.education
        $scope.originEducation.isJustImported = false
        $scope.$emit 'eventExitEditMode'
        $scope.$emit 'timeline:events:toReloadList'

    $scope.cancel = ->
      $scope.$emit 'eventExitEditMode'

    $scope.toValid =
      name: 'to',
      expression: (value) ->
        return true unless value?
        value > $scope.education.from
      messages:
        invalid: $translate.instant('talent.errors.date.to_is_greater')

    # ----------------------------------------
    # Watch
    # ----------------------------------------

    $scope.$watch 'education.from', (newVal, oldVal) ->
      if $scope.educationForm.to.$dirty
        $scope.$broadcast 'trigger-to-validate'




