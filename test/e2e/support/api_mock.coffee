HttpBackend = require('http-backend-proxy')

class ApiMock
  constructor: ->
    @baseUrl = 'http://localhost:3010/api/'
    @proxy = new HttpBackend(browser, {buffer: true})

  init: ->
    @proxy.whenGET(@baseUrl + 'talent/talent').respond({})
    @proxy.whenGET(@baseUrl + 'locales?lang=en').respond({})

module.exports = ApiMock