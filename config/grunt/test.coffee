module.exports.tasks =
  watch:
    coffeeTest:
      files: ["test/spec/{,*/}*.{coffee,litcoffee,coffee.md}"]
      tasks: [
        "newer:coffee:test"
        "karma"
      ]
      options:
        spawn: false

  # Test settings
  karma:
    dev:
      configFile: "test/karma-dev.conf.coffee"
      singleRun: true

#    dist:
#      configFile: "test/karma-dist.conf.coffee"
#      singleRun: true

