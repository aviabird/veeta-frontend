"use strict"

###*
  # @ngdoc directive
  # @name account.directives:account-settings
  #
  # @requires $rootScope
  # @requires TalentSetting
  # @requires ACCOUNT
  #
  # @restrict AE
  #
  # @description
  # form for talent setting
  # be used on talent setting page
###

angular.module("veetaTalentApp.account.directives")
.directive "accountSettings", ($rootScope, TalentSetting, ACCOUNT, OptionsService) ->
  ###*
  Clone a origin talentSetting
  ###
  originTalentSetting = null
  keepOrigin = ($scope)->
    originTalentSetting = angular.copy($scope.talentSetting)

  return (
    restrict: "AE"
    templateUrl: ACCOUNT.PARTIALS.SETTINGS
    replace: true
    scope: {}
    controller: ($scope) ->
      ###*
      Initialize a new TalentSetting with the data from rootScope
      ###
      $scope.talentSetting = new TalentSetting
        resumeType: $rootScope.talentSetting.resumeType
        newsletterUpdates: $rootScope.talentSetting.newsletterUpdates
      keepOrigin($scope)

      ###*
      Set options
      ###
      $scope.newsletterUpdatesOptions = OptionsService.newsletterOptions

      ###*
      careerLevelFormatter
      ###
      $scope.resumeTypeFormatter = (value)->
        ''

      ###*
      Save
      ###
      $scope.save = ->
        delete $scope.talentSetting.id
        $scope.talentSetting.update().then (data) ->
          $rootScope.talentSetting = data
          keepOrigin($scope)

      ###*
      Reset
      ###
      $scope.reset = ->
        $scope.talentSetting = angular.copy(originTalentSetting)

  )


