"use strict"

###*
  # @ngdoc directive
  # @name resume.directives:resume-links
  #
  # @requires ResumeLinks
  # @requires RESUME
  #
  # @description
  # link block of resume
  # be used on resume detail page
###

angular.module 'veetaTalentApp.resume.directives'
.directive 'resumeLinks', (ResumeLinks, RESUME) ->

  _loadLink = (scope)->
    scope.resume.getLinks().then (response) ->
      scope.links = response

  return(
    restrict: "AE"
    templateUrl: RESUME.PARTIALS.LINKS
    replace: true
    scope: {}
    controller: ($scope) ->
      # ----------------------------------------
      # Initialize attributes
      # ----------------------------------------

      # init a empty link
      $scope.links = {}



      # ----------------------------------------
      # Private methods
      # ----------------------------------------

      _initLink = ->
        $scope.links = new ResumeLinks($scope.$parent.resume.link)



      # ----------------------------------------
      # Public methods
      # ----------------------------------------
      $scope.save = ->
        $scope.links.resumeId = $scope.$parent.resume.id
        $scope.links.save().then ->
          $scope.$parent.resume.links = $scope.links



      # ----------------------------------------
      # Events
      # ----------------------------------------

      # set a listener for handle the imported data
      $scope.$on 'resume:handle-imported-data', ->
        _initLink()

      $scope.$on 'resume:save', ->
        $scope.save()

      # ----------------------------------------
      # Initialize methods
      # ----------------------------------------

      # init link from resume
      _initLink($scope)
  )
