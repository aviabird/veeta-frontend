"use strict"

###*
  # @ngdoc directive
  # @name jobapp.directives:jobapp-password
  #
  # @requires JOBAPP
  #
  # @description
  # a job intro component, show information summary about a job
  # be used on jobapp assistant page
###

angular.module 'veetaTalentApp.jobapp.directives'
.directive "jobappPassword", (JOBAPP) ->
  restrict: "AE"
  templateUrl: JOBAPP.PARTIALS.PASSWORD
  replace: true
