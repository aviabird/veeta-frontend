describe "veetaTalentApp.jobapp", ->
  describe "controllers", ->
    describe 'JobappOverviewCtrl', ->
      beforeEach module(
        "veetaTalentApp.jobapp"
        'mocked/jobapps/list.json'
      )

      $scope = undefined
      $controller = undefined
      $httpBackend = undefined
      mockedJobappsList = undefined
      Jobapp = undefined

      beforeEach inject (_$httpBackend_
                         _$rootScope_
                         _$controller_
                         _Jobapp_
                         _mockedJobappsList_) ->
        mockedJobappsList = _mockedJobappsList_

        Jobapp = _Jobapp_

        $httpBackend = _$httpBackend_

        $scope = _$rootScope_.$new()
        $controller = _$controller_ 'JobappOverviewCtrl',
          $scope: $scope
          Jobapp: Jobapp

      describe 'initialize', ->
        it 'should get a list of jobapps after into the controller', ->
          $httpBackend.whenGET(Jobapp.$url()).respond(mockedJobappsList)
          $httpBackend.flush()
          expect($scope.jobapps.length).toBeGreaterThan(1)