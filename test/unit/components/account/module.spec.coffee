describe "veetaTalentApp.account", ->
  describe "module", ->
    module = undefined

    beforeEach ->
      module = angular.module('veetaTalentApp.account')

    it "should be registered", ->
      expect(module).toBeDefined()

    describe "Dependencies:", ->
      deps = undefined

      hasModule = (m)->
        deps.indexOf(m) >= 0

      beforeEach ->
        deps = module.value('appName').requires

      it "should have veetaTalentApp.account.controllers as a dependency", ->
        expect(hasModule('veetaTalentApp.account.controllers')).toBeTruthy()

      it "should have veetaTalentApp.account.directives as a dependency", ->
        expect(hasModule('veetaTalentApp.account.directives')).toBeTruthy()

      it "should have  veetaTalentApp.account.resources as a dependency", ->
        expect(hasModule('veetaTalentApp.account.resources')).toBeTruthy()
