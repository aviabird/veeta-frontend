# config valid only for Capistrano 3.1
lock '3.2.1'

set :application, 'veeta_talent_client'
set :repo_url, 'git@bitbucket.org:talent-solutions/veeta_talent_client.git'

# Default branch is :master
# ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }.call

# Default deploy_to directory is /var/www/my_app
# set :deploy_to, '/var/www/my_app'

# Default value for :scm is :git
# set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
set :linked_files, %w{app/scripts/config.coffee}

# Default value for linked_dirs is []
# set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}
set :linked_dirs, %w{node_modules bower_components}

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5

set :rbenv_ruby, '2.1.2'
set :rbenv_custom_path, '/opt/rbenv'

namespace :deploy do

   # call bower install and npm install
   task :bower_and_npm_install do 
    on roles(:app), in: :sequence, wait: 5 do
      within release_path do 
        execute :npm, "install"
        execute :bower, "install --allow-root --force-latest"
      end
    end
  end
  after :published, :bower_and_npm_install


  # build the app
  task :build do 
    on roles(:app), in: :sequence, wait: 5 do 
      within release_path do 
        execute :grunt, "build"
      end
    end
  end
  after :bower_and_npm_install, :build

end
