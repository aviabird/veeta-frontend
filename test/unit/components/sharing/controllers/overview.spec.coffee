describe "veetaTalentApp.sharing", ->
  describe "controllers", ->
    describe "SharingOverviewCtrl", ->
      beforeEach module(
        "veetaTalentApp.sharing"
        "mocked/sharings/list.json"
        "ui.bootstrap")

      $scope = undefined
      $controller = undefined
      $httpBackend = undefined
      mockedSharingsList = undefined

      beforeEach inject (_$httpBackend_
                         _$rootScope_
                         _$controller_
                         _$modal_
                         _Sharing_
                         _mockedSharingsList_) ->
        mockedSharingsList = _mockedSharingsList_

        $httpBackend = _$httpBackend_
        $httpBackend.whenGET(_Sharing_.$url()).respond mockedSharingsList
        $scope = _$rootScope_.$new()


        $controller = _$controller_ "SharingOverviewCtrl",
          $scope: $scope
          $modal: _$modal_
          Sharing: _Sharing_

      it "should receive a list of sharings", ->
        $httpBackend.flush()
        expect($scope.sharings.length).toBeGreaterThan 1
