"use strict"

###*
  # @ngdoc controller
  # @name jobapp.controllers:JobappNewCheckAndSendCtrl
  #
  # @requires $scope
  # @requires $state
  # @requires TalentAuth
  # @requires Talent
  # @requires $window
  #
  # @description
  # Controller for check and send for jobapp creating
  # be used on Jabapp assistant
###

angular.module 'veetaTalentApp.jobapp.controllers'
.controller 'JobappNewCheckAndSendCtrl', ($log, $scope, $state, TalentAuth, Talent, FlashService)->

  $parentScope = $scope.$parent
  $parentScope.step = 3
  $scope.jobapp = $parentScope.jobapp
  $scope.job = $parentScope.job

  $scope.talent = $scope.$parent.talent

  $scope.emailChangeMode = false

  $scope.goPreviousStep = ->
    $state.go '^.addCoverLetter'

  $scope.onSend = ->
    $scope.jobapp.save().then (data) ->
      FlashService.setMessage
        title: 'talent.flash.application_has_been_successfully_sent.title'
        body: 'talent.flash.application_has_been_successfully_sent.body'
        iconName: 'fa-check'
      $state.go 'app.inside.jobapps.overview'

  # Go to final step, when jobapp was succesfully saved
  $scope.sendApplication = ->
    $scope.jobapp.save().then ->
      $state.go '^.finished'

  $scope.resendConfirmationEmail = ->
    TalentAuth.$put(TalentAuth.$url('email'), {email: $scope.talent.email}).then ->

  $scope.checkEmailConfirmation = ->
    Talent.query().then (data) ->
      $scope.talent = data

  $scope.updateEmail = ->
    $scope.talent.save().then ->
      $scope.emailChangeMode = false

  $scope.canSend = ->
    $scope.generalTerm is true and $scope.policyTerm is true
