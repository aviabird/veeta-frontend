# Karma configuration
# http://karma-runner.github.io/0.12/config/configuration-file.html
# Generated on 2014-10-28 using
# generator-karma 0.8.3

module.exports = (config) ->
  config.set
  # base path, that will be used to resolve files and exclude
    basePath: '../'

  # testing framework to use (jasmine/mocha/qunit/...)
    frameworks: ['jasmine']

  # list of files / patterns to load in the browser
    files: [
      # bower:js
      'bower_components/jquery/dist/jquery.js'
      'bower_components/es5-shim/es5-shim.js'
      'bower_components/json3/lib/json3.js'
      'bower_components/jquery-ui/jquery-ui.js'
      'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/affix.js'
      'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/alert.js'
      'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/button.js'
      'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/carousel.js'
      'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/collapse.js'
      'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/dropdown.js'
      'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/tab.js'
      'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/transition.js'
      'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/scrollspy.js'
      'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/modal.js'
      'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/tooltip.js'
      'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/popover.js'
      'bower_components/angular/angular.js'
      'bower_components/angular-sanitize/angular-sanitize.js'
      'bower_components/angular-cookies/angular-cookies.js'
      'bower_components/angular-rails-resource/angularjs-rails-resource.js'
      'bower_components/angular-bootstrap/ui-bootstrap-tpls.js'
      'bower_components/angular-ui-router/release/angular-ui-router.js'
      'bower_components/angular-xeditable/dist/js/xeditable.js'
      'bower_components/angular-file-upload/angular-file-upload.js'
      'bower_components/select2/select2.js'
      'bower_components/angular-ui-select2/src/select2.js'
      'bower_components/angular-dragdrop/src/angular-dragdrop.js'
      'bower_components/spin.js/spin.js'
      'bower_components/angular-spinner/angular-spinner.js'
      'bower_components/angular-growl-v2/build/angular-growl.js'
      'bower_components/momentjs/moment.js'
      'bower_components/jsnlog/jsnlog.js'
      'bower_components/bootstrap-switch/dist/js/bootstrap-switch.js'
      'bower_components/angular-bootstrap-switch/dist/angular-bootstrap-switch.js'
      'bower_components/angular-touch/angular-touch.js'
      'bower_components/venturocket-angular-slider/build/angular-slider.js'
      'bower_components/checklist-model/checklist-model.js'
      'bower_components/angular-elastic/elastic.js'
      'bower_components/angular-ui-select/dist/select.js'
      'bower_components/textAngular/dist/textAngular.min.js'
      'bower_components/ng-tags-input/ng-tags-input.min.js'
      'bower_components/angular-easy-form/dist/angular-easy-form.min.js'
      'bower_components/angular-ui-utils/ui-utils.js'
      'bower_components/bootstrap-filestyle/src/bootstrap-filestyle.js'
      'bower_components/angular-cookie/angular-cookie.js'
      'bower_components/ng-token-auth/dist/ng-token-auth.js'
      'bower_components/angular-local-storage/dist/angular-local-storage.js'
      'bower_components/angular-filter/dist/angular-filter.js'
      'bower_components/angular-mocks/angular-mocks.js'
      # endbower

      # injector
      'app/scripts/lib/angular-rails-resource-extend/module.coffee'
      'app/scripts/lib/angular-rails-resource-extend/extensions/array.coffee'
      'app/scripts/lib/angular-rails-resource-extend/extensions/attributes_update.coffee'
      'app/scripts/lib/angular-rails-resource-extend/extensions/flash.coffee'
      'app/scripts/lib/angular-rails-resource-extend/extensions/http_exception.coffee'
      'app/scripts/lib/angular-translate/angular-translate.js'
      'app/scripts/lib/angular-translate/angular-translate-loader-url.js'
      'app/scripts/common/module.coffee'
      'app/scripts/common/config/authentication.coffee'
      'app/scripts/common/config/constants.coffee'
      'app/scripts/common/config/easy-form.coffee'
      'app/scripts/common/config/growl.coffee'
      'app/scripts/common/config/localstorage.coffee'
      'app/scripts/common/config/resources.coffee'
      'app/scripts/common/config/translate.coffee'
      'app/scripts/common/config/ui-jq.coffee'
      'app/scripts/common/config/ui-router.coffee'
      'app/scripts/common/config/xeditable.coffee'
      'app/scripts/common/controllers/inside.coffee'
      'app/scripts/common/controllers/item-remove-modal.coffee'
      'app/scripts/common/directives/flash.coffee'
      'app/scripts/common/directives/month-select.coffee'
      'app/scripts/common/directives/timeline.coffee'
      'app/scripts/common/filters/address.coffee'
      'app/scripts/common/filters/datetime.coffee'
      'app/scripts/common/filters/name.coffee'
      'app/scripts/common/resources/address.coffee'
      'app/scripts/common/resources/country.coffee'
      'app/scripts/common/resources/language.coffee'
      'app/scripts/common/resources/state.coffee'
      'app/scripts/common/services/flash.coffee'
      'app/scripts/common/services/inflector.coffee'
      'app/scripts/common/services/options.coffee'
      'app/scripts/domains/desktop/module.coffee'
      'app/scripts/domains/desktop/config/constants.coffee'
      'app/scripts/domains/desktop/config/states.coffee'
      'app/scripts/domains/desktop/controllers/app.coffee'
      'app/scripts/domains/desktop/controllers/welcome.coffee'
      'app/scripts/domains/desktop/directives/footer.coffee'
      'app/scripts/domains/desktop/directives/header.coffee'
      'app/scripts/domains/desktop/directives/main-nav.coffee'
      'app/scripts/components/auth/module.coffee'
      'app/scripts/components/auth/config/constants.coffee'
      'app/scripts/components/auth/controllers/sign-in.coffee'
      'app/scripts/components/resume/module.coffee'
      'app/scripts/components/resume/config/constants.coffee'
      'app/scripts/components/resume/controllers/detail.coffee'
      'app/scripts/components/resume/controllers/overview.coffee'
      'app/scripts/components/resume/controllers/updates_deactivate_modal.coffee'
      'app/scripts/components/resume/directives/add_module.coffee'
      'app/scripts/components/resume/directives/additional_documents.coffee'
      'app/scripts/components/resume/directives/detail_nav.coffee'
      'app/scripts/components/resume/directives/education.coffee'
      'app/scripts/components/resume/directives/header-extend.coffee'
      'app/scripts/components/resume/directives/header.coffee'
      'app/scripts/components/resume/directives/import.coffee'
      'app/scripts/components/resume/directives/language.coffee'
      'app/scripts/components/resume/directives/languages.coffee'
      'app/scripts/components/resume/directives/links.coffee'
      'app/scripts/components/resume/directives/profile.coffee'
      'app/scripts/components/resume/directives/skills.coffee'
      'app/scripts/components/resume/directives/updates.coffee'
      'app/scripts/components/resume/directives/work_experience.coffee'
      'app/scripts/components/resume/resources/document.coffee'
      'app/scripts/components/resume/resources/education.coffee'
      'app/scripts/components/resume/resources/language.coffee'
      'app/scripts/components/resume/resources/links.coffee'
      'app/scripts/components/resume/resources/resume.coffee'
      'app/scripts/components/resume/resources/sharing.coffee'
      'app/scripts/components/resume/resources/skill.coffee'
      'app/scripts/components/resume/resources/work_experience.coffee'
      'app/scripts/components/jobapp/module.coffee'
      'app/scripts/components/jobapp/config/constants.coffee'
      'app/scripts/components/jobapp/controllers/detail.coffee'
      'app/scripts/components/jobapp/controllers/job.coffee'
      'app/scripts/components/jobapp/controllers/new.coffee'
      'app/scripts/components/jobapp/controllers/new/add_cover_letter.coffee'
      'app/scripts/components/jobapp/controllers/new/check_and_send.coffee'
      'app/scripts/components/jobapp/controllers/new/finished.coffee'
      'app/scripts/components/jobapp/controllers/new/resume.coffee'
      'app/scripts/components/jobapp/controllers/new/resume_detail.coffee'
      'app/scripts/components/jobapp/controllers/new/resume_select.coffee'
      'app/scripts/components/jobapp/controllers/overview.coffee'
      'app/scripts/components/jobapp/controllers/withdraw_confirmation.coffee'
      'app/scripts/components/jobapp/directives/job_intro.coffee'
      'app/scripts/components/jobapp/directives/password.coffee'
      'app/scripts/components/jobapp/directives/wizard_bar.coffee'
      'app/scripts/components/jobapp/resources/job.coffee'
      'app/scripts/components/jobapp/resources/jobapp.coffee'
      'app/scripts/components/support/module.coffee'
      'app/scripts/components/support/config/constants.coffee'
      'app/scripts/components/account/module.coffee'
      'app/scripts/components/account/config/constants.coffee'
      'app/scripts/components/account/controllers/detail.coffee'
      'app/scripts/components/account/directives/availability.coffee'
      'app/scripts/components/account/directives/delete.coffee'
      'app/scripts/components/account/directives/profile.coffee'
      'app/scripts/components/account/directives/settings.coffee'
      'app/scripts/components/account/directives/talent-profile.coffee'
      'app/scripts/components/account/resources/talent.coffee'
      'app/scripts/components/account/resources/talent_auth.coffee'
      'app/scripts/components/account/resources/talent_setting.coffee'
      'app/scripts/components/sharing/module.coffee'
      'app/scripts/components/sharing/config/constants.coffee'
      'app/scripts/components/sharing/controllers/overview.coffee'
      'app/scripts/components/sharing/resources/sharing.coffee'
      'app/scripts/domains/mobile/module.coffee'
      'app/scripts/domains/mobile/config/constants.coffee'
      'app/scripts/domains/mobile/config/states.coffee'
      'app/scripts/domains/mobile/controllers/app.coffee'
      'app/scripts/domains/mobile/directives/mobile_footer.coffee'
      'app/scripts/domains/mobile/directives/mobile_header.coffee'
      # endinjector

      '.tmp/scripts/template-cache.js'
      'test/support/**/*.coffee'
      'api_mocks/**/*.json'
      'app/views/**/*.jade'
      'test/unit/**/*.coffee'
    ],

    # list of files / patterns to exclude
    exclude: []

    # web server port
    port: 8080

    # level of logging
    # possible values: LOG_DISABLE || LOG_ERROR || LOG_WARN || LOG_INFO || LOG_DEBUG
    logLevel: config.LOG_INFO

    # Start these browsers, currently available:
    # - Chrome
    # - ChromeCanary
    # - Firefox
    # - Opera
    # - Safari (only Mac)
    # - PhantomJS
    # - IE (only Windows)
    browsers: [
      'PhantomJS'
    ]

    # Which plugins to enable
    plugins: [
      'karma-phantomjs-launcher'
      'karma-jasmine'
      'karma-coverage'
      'karma-ng-json2js-preprocessor'
      'karma-ng-html2js-preprocessor'
      'karma-ng-jade2js-preprocessor'
      'karma-coffee-preprocessor'
    ]

    # enable / disable watching file and executing tests whenever any file changes
    autoWatch: true

    # Continuous Integration mode
    # if true, it capture browsers, run tests and exit
    singleRun: false

    colors: true

    # coverage reporter generates the coverage
    reporters: ['progress', 'coverage'],

    preprocessors:
      '**/*.coffee': ['coffee']
      'app/views/**/*.jade': ['ng-jade2js']
      'api_mocks/**/*.json': ['ng-json2js']

    # optionally, configure the reporter
    coverageReporter:
      type: 'html',
      dir: 'test/coverage/'


    ngHtml2JsPreprocessor:
      stripPrefix: 'app/'

    ngJson2JsPreprocessor:
      # strip this from the file path
      stripPrefix: 'api_mocks/'
      # prepend this to the
      prependPrefix: 'mocked/'


    coffeePreprocessor:
      # options passed to the coffee compiler
      options:
        bare: true
        sourceMap: false
      # transforming the filenames
      transformPath: (path) ->
        path.replace(/\.coffee$/, '.js')


    ngJade2JsPreprocessor:
      # strip this from the file path
      stripPrefix: 'app/'

      # or define a custom transform function
#      cacheIdPath: (filepath) ->
#        console.log filepath,'sdfffffffffffffffffffffffffffffffff'
#        cacheId
#                  cacheIdFromPath: function(filepath) {
#                     return cacheId;
#                 },

      # By default, Jade files are added to template cache with '.html' extension.
      # Set this option to change it.
      templateExtension: 'html'

      # setting this option will create only a single module that contains templates
      # from all the files, so you can load them all with module('foo')
      # moduleName: 'foo'

    # Uncomment the following lines if you are using grunt's server to run the tests
    # proxies: '/': 'http://localhost:9000/'
    # URL root prevent conflicts with the site root
    # urlRoot: '_karma_'
