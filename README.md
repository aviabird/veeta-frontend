# How to start the application
1. `npm install grunt-cli -g`
2. `npm install bower -g`
3. `npm install yo -g`
4. `npm install & bower install`
5. `grunt serve`   
6. `grunt compass:dev` 
  
# List of Grunt commands
 1. `grunt build` compile the app
 2. `grunt serve` compile the app, start dev server
 3. `grunt docs` generate docs file
 4. `grunt compass:dev` start compass watch. we don't use grunt-contrib-watch to watching sass file, because of performance reason
 5. `grunt install` automatic inject bower package and app code into index.jade, main.scss and karma.conf
 6. `grunt test` run test suit

# What needs to be done

## ASAP
* Reduce number of `watchers`
* Mobile website

## long-term
* Move translations from filter to directives
* Write more tests
* Enhance the performance (by reducing number of `watchers`, caching list of languages and countries)
* Missing translations
* Authentication for uploading files and loosing connection (token vs header update)
* Use only one API namespace per Angular App
* Missing 404 error page
* Add `use strict` to all files
* Remove `growl`

# Feature list
* Add loading bar while uploading document 
* Log errors also front front-end
* Consider using HTML5mode (on nginx server) - url without #

# Tips
1. use "grunt-contrib-compass" with version 0.7.2! the latest version looks like has some problem.
2. Use `$log.debug()` instead of `console.log()`
3. Use `$scope` instead of `$rootScope` if it’s possible
4. Use `ng-if` instead of `ng-show`
5. Use new Angular naming convention 

# Good to know
1. `$rootScope.user` is set up by default and is pointing to `auth` (not talent)

# List of files with todos
* __localStorage -> cookies__ 
app/scripts/common/config/authentication.coffee
* __Add text to modal window__
app/scripts/common/controllers/item-remove-modal.coffee
* __Switch from object to string (example)__
app/scripts/common/directives/month-select.coffee
* __Refactor timeline directive (also some examples)__
app/scripts/common/directives/timeline.coffe
* __Move translations to options service__
app/scripts/components/account/directive/delete.coffee
* __Add hiding button function__
app/scripts/components/acount/talent-profile.coffee
* __Create shared controllers for sign-up/in__
app/scripts/components/auth/controllers/sign-in.coffee
* __Enhance page reload__
app/scripts/components/jobapp/controllers/new.coffee
* __Use real URL, not hardcoded__
app/scripts/components/jobapp/controllers/new/finished.coffee
* __Optimize, decrease number of $watch__
app/scripts/components/jobapp/controllers/new/resume.coffee
* __Validate uploaded documenty type__
app/scripts/components/resume/directives/additional_documents.coffee
* __PDF validation__
app/scripts/components/resume/directives/import.coffee
* __Use $q.all__
app/scripts/domains/desktop/config/states.coffee
* __Use translationService__
app/scripts/domains/directives/header.coffee