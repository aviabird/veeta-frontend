path = require('path');

module.exports = (grunt, appConfig) -> tasks:

  # create docs from scripts
  ngdocs:
    options:
      dest: 'docs'
      html5mode: false
      startPage: '/api'
      title: "Veeta Talent Client Docs"

    api:
      src: [
        "#{appConfig.paths.tmp}/scripts/components/**/*.js"
        "#{appConfig.paths.tmp}/scripts/domains/**/*.js"
        "#{appConfig.paths.tmp}/scripts/common/**/*.js"
      ]
      title: 'API'