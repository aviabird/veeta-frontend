angular.module("rails.extend").factory "RailsResourceHttpExceptionMixin", (RailsResourceInjector, $q, $log, $rootScope) ->
  RailsResourceHttpExceptionMixin = ->
  RailsResourceHttpExceptionMixin.extended = (Resource) ->
    Resource.intercept "responseError", (rejection) ->
      switch rejection.status
        when 0 then $rootScope.$broadcast "error:no_connection"
        when 422 then $rootScope.$broadcast "error:unprocessable_entity"
        else $rootScope.$broadcast "error:unknown"
      $q.reject rejection

  RailsResourceHttpExceptionMixin
