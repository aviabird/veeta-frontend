"use strict"

#JobPage = require('../outside/pages/job.page');
JobappAssistantSelectResumePage = require('./pages/select_resume.page');

describe "E2E: Jobapp Assistant", ->
  ptor = undefined
  jobPage = undefined
  jobappAssistantNewSelectResumePage = undefined

  beforeEach ->
    ptor = protractor.getInstance()
    jobPage = new JobPage.get()
#    jobappAssistantNewSelectResumePage = new JobappAssistantNewSelectResumePage()

  it 'redirect to job assistant page first', ->
#    expect(browser.getCurrentUrl()).toMatch(jobappAssistantNewSelectResumePage.url)
    expect(ptor.getCurrentUrl()).toMatch('')

  describe "jobapps list", ->
    it "should list jobapps", ->
      ptor.sleep(500);
      expect(jobappAssistantNewSelectResumePage.resumesList.count()).toEqual 3
