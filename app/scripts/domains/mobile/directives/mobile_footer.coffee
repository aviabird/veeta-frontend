"use strict"

###*
  # @ngdoc directive
  # @name mobile.directives:mobile-footer
  #
  # @requires MOBILE
  #
  # @description
  # application footer
  # be used on mobile application layout
###

angular.module 'veetaTalentApp.mobile.directives'
.directive "mobileFooter", (MOBILE)->
  restrict: "AE",
  templateUrl: MOBILE.PARTIALS.FOOTER,
  replace: true