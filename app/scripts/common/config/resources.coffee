angular.module 'veetaTalentApp.common'
.config (RailsResourceProvider) ->
  RailsResourceProvider.extensions ['flash'
                                    'httpException'
                                    'attributes'
                                    'array']
