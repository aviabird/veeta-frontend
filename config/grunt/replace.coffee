module.exports = (grunt, appConfig) -> tasks:
  replace:
    server:
      options:
        patterns: [
          match: 'BASE_URL'
          replacement: '/'
        ]
      files: [
        {
          expand: true
          flatten: true
          src: ["#{appConfig.paths.tmp}/*.html"]
          dest: "#{appConfig.paths.tmp}/"
        }
      ]
    dist:
      options:
        patterns: [
          match: 'BASE_URL'
          replacement: '/clients/talent/'
        ]
      files: [
        {
          expand: true
          flatten: true
          src: ["#{appConfig.paths.dist}/*.html"]
          dest: "#{appConfig.paths.dist}/"
        }
      ]
