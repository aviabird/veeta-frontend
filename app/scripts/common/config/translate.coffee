angular.module 'veetaTalentApp.common'
.config ($translateProvider, $httpProvider)->

  # register a dummy translate table to pending URL loader
  $translateProvider.translations 'xx',
    GREETING: 'Hello world!'

  $translateProvider.preferredLanguage('xx')
  $translateProvider.useUrlLoader(CONFIG.apiUrl + '/locales')

  $httpProvider.interceptors.push "localeHttpResponseInterceptor"

.run ($rootScope, TranslateService) ->
  $rootScope.translationOptions = [
    {
      key: "en"
      text: "English"
    }
    {
      key: "de"
      text: "Deutsch"
    }
  ]
.factory "localeHttpResponseInterceptor", ($translate) ->
  return (
    request: (config)->
      config.headers['x-locale'] = $translate.use()
      return config
  )

.factory 'TranslateService', ($rootScope, $log, localStorageService, $translate, $state, OptionsService) ->
  return (
    recognizeDefaultLanguage: ->
      if localStorageService.isSupported
        $rootScope.currentTranslate = localStorageService.get('currentTranslate') || $rootScope.translationOptions[0]
      else
        $rootScope.currentTranslate = $rootScope.translationOptions[0]
      moment.locale($translate.use())
  )