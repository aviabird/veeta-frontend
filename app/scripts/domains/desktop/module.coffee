###*
  # @ngdoc overview
  # @name desktop
  #
###

angular.module 'veetaTalentApp.desktop', [
  # common module
  'veetaTalentApp.common'

  # components
  'veetaTalentApp.auth'
  'veetaTalentApp.resume'
  'veetaTalentApp.jobapp'
  'veetaTalentApp.support'
  'veetaTalentApp.account'
  'veetaTalentApp.sharing'

  # modules
  'veetaTalentApp.desktop.controllers'
  'veetaTalentApp.desktop.directives'
]

angular.module 'veetaTalentApp.desktop.controllers', []
angular.module 'veetaTalentApp.desktop.directives', []
