angular.module "veetaTalentApp.common"
.constant "COMMON",
  VIEWS:
    NOT_FOUND: "views/common/404.html"

  PARTIALS:
    TIMELINE: "views/common/partials/timeline.html"
    ITEM_REMOVE_MODAL: "views/common/partials/item-remove-modal.html"
    MONTH_SELECT: "views/common/partials/month-select.html"
    FLASH: "views/common/partials/flash.html"

.constant "LAYOUTS",
  VIEWS:
    DESKTOP: "views/layouts/desktop.html"
    MOBILE: "views/layouts/mobile.html"
