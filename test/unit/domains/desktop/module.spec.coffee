describe "veetaTalentApp.desktop", ->
  describe "module", ->
    module = undefined

    beforeEach ->
      module = angular.module('veetaTalentApp.desktop')

    it "should be registered", ->
      expect(module).toBeDefined()

    describe "Dependencies:", ->
      deps = undefined

      hasModule = (m)->
        deps.indexOf(m) >= 0

      beforeEach ->
        deps = module.value('appName').requires

      # components
      it "should have veetaTalentApp.resume as a dependency", ->
        expect(hasModule('veetaTalentApp.resume')).toBeTruthy()

      it "should have veetaTalentApp.jobapp as a dependency", ->
        expect(hasModule('veetaTalentApp.jobapp')).toBeTruthy()

      it "should have veetaTalentApp.support as a dependency", ->
        expect(hasModule('veetaTalentApp.support')).toBeTruthy()

      it "should have veetaTalentApp.account as a dependency", ->
        expect(hasModule('veetaTalentApp.account')).toBeTruthy()

      it "should have veetaTalentApp.sharing as a dependency", ->
        expect(hasModule('veetaTalentApp.sharing')).toBeTruthy()

      # modules
      it "should have veetaTalentApp.desktop.controllers as a dependency", ->
        expect(hasModule('veetaTalentApp.desktop.controllers')).toBeTruthy()

      it "should have veetaTalentApp.desktop.directives as a dependency", ->
        expect(hasModule('veetaTalentApp.desktop.directives')).toBeTruthy()
