###*
  # @ngdoc controller
  # @name jobapp.controllers:JobappNewFinishedCtrl
  #
  # @requires $scope
  # @requires $state
  #
  # @description
  # Controller showed up when jobapp was succesfully created
###

angular.module 'veetaTalentApp.jobapp.controllers'
.controller 'JobappNewFinishedCtrl', ($scope, $log, $state, $rootScope, $window) ->
  # Delete step to hide navbar
  delete $scope.$parent.step
  $scope.jobapp = $scope.$parent.jobapp
  $scope.talent = $scope.$parent.talent

  $log.debug 'jobapp assistant: finished ', $scope.jobapp

  # Redirect to desktop page on click
  # TODO: Use real URL, not hardcoded
  $scope.goToDesktop = ->
    $rootScope.$broadcast '$window.redirectTo', URL.BASE + '/app/talent/#/'
    