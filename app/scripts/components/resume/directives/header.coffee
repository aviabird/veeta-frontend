"use strict"

###*
  # @ngdoc directive
  # @name resume.directives:resume-header
  #
  # @requires ResumeSharing
  # @requires RESUME
  #
  # @description
  # a basic resume header of a resume, just show shared companies
  # be used on resume detail page of jobapp assistant
###

angular.module 'veetaTalentApp.resume.directives'
.directive 'resumeHeader', (RESUME, ResumeSharing) ->
  restrict: "AE"
  templateUrl: RESUME.PARTIALS.HEADER
  replace: true
  scope: {}
  controller: ($scope, $state) ->
    $scope.resume = $scope.$parent.resume

    $scope.isShowSharedCompanies = ->
      $scope.resume.companies and $scope.resume.companies.length > 0