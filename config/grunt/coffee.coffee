module.exports = (grunt, appConfig) -> tasks:

  watch:
    coffee:
      files: ["#{appConfig.paths.app}/scripts/**/**/*.{coffee,litcoffee,coffee.md}"]
      tasks: ["newer:coffee:dist"]
      options:
        spawn: false
        livereload: true

  # Compiles CoffeeScript to JavaScript
  coffee:
    options:
      sourceMap: true
      sourceRoot: ""

    dist:
      files: [
        expand: true
        cwd: "#{appConfig.paths.app}/scripts"
        src: "**/*.coffee"
        dest: "#{appConfig.paths.tmp}/scripts"
        ext: ".js"
      ]

    test:
      files: [
        expand: true
        cwd: "test/spec"
        src: "{,*/}*.coffee"
        dest: "#{appConfig.paths.tmp}/spec"
        ext: ".js"
      ]