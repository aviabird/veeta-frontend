###*
  # @ngdoc controller
  # @name jobapp.controllers:JobappNewSelectResumeCtrl
  #
  # @requires $scope
  # @requires Resume
  # @requires $state
  #
  # @description
  # Controller for select resume for jobapp creating
###

angular.module 'veetaTalentApp.jobapp.controllers'
.controller 'JobappNewResumeCtrl', ($scope, $location, $log, Resume, $state) ->

  $parentScope = $scope.$parent

  $scope.jobapp = $parentScope.jobapp
  $scope.job = $parentScope.job
  $scope.resume = $parentScope.resume
  $scope.resumes = $parentScope.resumes

  $parentScope.step = 1

  $log.debug 'jobapp assistant: resume ', $scope.jobapp

  # TODO: Optimize, decrease number of $watch
  $scope.$watch 'jobapp', (newVal, oldVal) ->
    $parentScope.jobapp = $scope.jobapp unless newVal is oldVal

  $scope.$watch 'job', (newVal, oldVal) ->
    $parentScope.job = $scope.job unless newVal is oldVal

  $scope.$watch 'resume', (newVal, oldVal) ->
    $parentScope.resume = $scope.resume unless newVal is oldVal

  $scope.$watch 'resumes', (newVal, oldVal) ->
    $parentScope.resumes = $scope.resumes unless newVal is oldVal