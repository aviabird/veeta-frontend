describe "veetaTalentApp.account", ->
  describe "controllers", ->
    describe 'AccountDetailCtrl', ->
      beforeEach module(
        "veetaTalentApp.account"
        'veetaTalentApp.common.filters'
      )

      $scope = undefined
      $controller = undefined
      $httpBackend = undefined

      beforeEach inject (_$rootScope_
                         _$httpBackend_
                         _$controller_) ->
        $httpBackend = _$httpBackend_
        $scope = _$rootScope_.$new()

        # create controller object
        $controller = _$controller_ 'AccountDetailCtrl',
          $scope: $scope

      describe 'talentName()', ->
        it 'should convert talent name with name filter', ->
          $scope.talent =
            firstName: 'John'
            lastName: 'Antony'
          expect($scope.talentName()).toBe 'John Antony'
