describe "veetaTalentApp.resume", ->
  describe "controllers", ->
    describe 'ResumeUpdatesDeactivateModalCtrl', ->
      beforeEach module("veetaTalentApp.resume")

      $scope = undefined
      $controller = undefined

      beforeEach inject (_$httpBackend_
                         _$rootScope_
                         _$controller_) ->
        $scope = _$rootScope_.$new()

        # Create a mock modalInstance using spies
        $modalInstance =
          close: jasmine.createSpy("modalInstance.close")
          dismiss: jasmine.createSpy("modalInstance.dismiss")
          result:
            then: jasmine.createSpy("modalInstance.result.then")

        # create controller object
        $controller = _$controller_ 'ResumeUpdatesDeactivateModalCtrl',
          $scope: $scope
          $modalInstance: $modalInstance

      describe 'confirm()', ->
        it 'should close modal after jobapp withdrawn', ->

      describe 'cancel()', ->
        it 'should close modal', ->


