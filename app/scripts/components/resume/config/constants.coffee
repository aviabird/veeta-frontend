angular.module 'veetaTalentApp.resume'
.constant 'RESUME',
  VIEWS:
    OVERVIEW: 'views/components/resume/overview.html'
    DETAIL: 'views/components/resume/detail.html'
  PARTIALS:
    HEADER: 'views/components/resume/partials/header.html'
    HEADER_EXTEND: 'views/components/resume/partials/header-extend.html'
    NAME: 'views/components/resume/partials/name.html'
    UPDATES: 'views/components/resume/partials/updates.html'
    ACTIONS: 'views/components/resume/partials/actions.html'
    SHARING: 'views/components/resume/partials/sharing.html'
    PROFILE: 'views/components/resume/partials/profile.html'
    ADDITIONAL_DOCUMENTS: 'views/components/resume/partials/additional_documents.html'
    EDUCATIONS: 'views/components/resume/partials/educations.html'
    LANGUAGES: 'views/components/resume/partials/languages.html'
    SKILLS: 'views/components/resume/partials/skills.html'
    WORK_EXPERIENCES: 'views/components/resume/partials/work_experiences.html'
    IMPORT: 'views/components/resume/partials/import.html'
    DETAIL_NAV: 'views/components/resume/partials/detail_nav.html'
    LANGUAGE:  'views/components/resume/partials/language.html'
    LINKS: 'views/components/resume/partials/links.html'

    ADD_MODULE: 'views/components/resume/partials/add_module.html'

    FORM:
        PROFILE: 'views/components/resume/partials/profile.html'
        EDUCATION: 'views/components/resume/partials/form/education.html'
        WORK_EXPERIENCE: 'views/components/resume/partials/form/work_experience.html'

    MODAL:
        RESUME_UPDATES_DEACTIVATE: 'views/components/resume/partials/modal/updates_deactivate.html'
  MOBILE:
    PARTIALS:
      PROFILE: 'views/components/resume/mobile/partials/profile.html'
      LANGUAGES: 'views/components/resume/mobile/partials/languages.html'
      WORK_EXPERIENCES: 'views/components/resume/mobile/partials/work_experiences.html'
      WORK_EXPERIENCE: 'views/components/resume/mobile/partials/work_experience.html'