###*
  # @ngdoc controller
  # @name jobapp.controllers:JobappNewSelectResumeCtrl
  #
  # @requires $scope
  # @requires Resume
  # @requires $state
  #
  # @description
  # Controller for select resume for jobapp creating
###

angular.module 'veetaTalentApp.jobapp.controllers'
.controller 'JobappNewResumeSelectCtrl', ($scope, $rootScope, $log, Resume, $state, OptionsService) ->
  $parentScope = $scope.$parent

  $scope.jobapp = $parentScope.jobapp
  $scope.job = $parentScope.job
  $scope.resume = {}
  $scope.jobSourceOptions = OptionsService.referrerOptions
  $rootScope.pageLoading = true

  $log.debug 'jobapp assistant: select resume', $scope.jobapp

  Resume.query().then (response) ->
    $scope.resumes = response
    $parentScope.resumes = response
    if $scope.resumes.length is 0 and $scope.jobapp.jobType is 'Job'
      $state.go('^.new')
    else
      $rootScope.pageLoading = false

  $scope.newResume = new Resume()

  $scope.canAddNewResume = ->
    ($scope.resumes.length < 3) if $scope.resumes

  $scope.isShowJobDetailForm = ->
    $scope.jobapp.jobType is 'EmailApplication'

  $scope.goNextStep = ->
    $parentScope.job = $scope.job
    $parentScope.jobapp.source = $scope.jobapp.source
    $parentScope.jobapp.job = $scope.job

    if $scope.resume is $scope.newResume
      $state.go('^.new')
    else
      $state.go('^.detail', {resumeId: $scope.resume.id})

  # check if user can go to next step
  $scope.canGoNextStep = () ->
    ($scope.resumes && $scope.resume in $scope.resumes) or $scope.resume is $scope.newResume

  # Set resume in param as selected resume
  $scope.selectResume = (resume) ->
    $scope.resume = resume

  # Check if given resume is selected
  $scope.isSelected = (resume) ->
    $scope.resume is resume

