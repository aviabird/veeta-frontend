
"use strict"

# How to use translations

# 1. Make a request to translate key
#   $translate(['namespace.translation.key']).then (translations) ->
#     console.log(translations)

# 2. In `then` block
#   a) Without translation variable, use text from translations response array

#   Example:
#     translated = translations['namespace.translation.key']

#   b) With variable use method for translating
  
#   Example:
#     translated = $translate.instant('namespace.translation.key', { count: 2 })

angular.module("veetaTalentApp.common.services")
.factory "OptionsService", ($rootScope, $filter, $log, $translate, $q, Country, State, Language) ->
  _loadCountryOptions = ->
    Country.query().then (countries)->
      service.countryOptions.collection = countries
      service.countryOptions.uiSelect.collection = $filter('orderBy')(service.countryOptions.collection, 'name')
      $rootScope.$broadcast 'countryOptions:loaded'

  _loadStateOptions = ->
    State.query().then (states)->
      service.stateOptions.uiSelect.collection = states
      $rootScope.$broadcast 'stateOptions:loaded'

  _loadLanguageOptions = ->
    Language.query().then (languages)->
      service.languageOptions.uiSelect.collection = $filter('orderBy')(languages, 'name')
      $rootScope.$broadcast 'languageOptions:loaded'

  _translateGenderOptions = ->
    $translate([
      'talent.data.gender.male'
      'talent.data.gender.female'
    ])
    .then (translations) ->
      service.genderOptions.collection = [
        {
          value: 'male'
          text: translations['talent.data.gender.male']
        },
        {
          value: 'female'
          text: translations['talent.data.gender.female']
        }
      ]

      # Keep collection under uiSelect for backwards compatibility
      service.genderOptions.uiSelect.collection = service.genderOptions.collection

  _translateSalutationOptions = ->
    $translate([
      'talent.data.salutation.mr'
      'talent.data.salutation.mrs'
    ])
    .then (translations) ->
      service.salutationOptions.collection = [
        {
          value: 'mr'
          text: translations['talent.data.salutation.mr']
        },
        {
          value: 'mrs'
          text: translations['talent.data.salutation.mrs']
        }
      ]

      # Keep collection under uiSelect for backwards compatibility
      service.salutationOptions.uiSelect.collection = service.salutationOptions.collection

  _translateIndustryOptions = ->
    $translate([
      'talent.data.industry.example'
    ])
    .then (translations) ->
      service.industryOptions.collection = [
        {
          value: 'example'
          text: translations['talent.data.industry.example']
        }
      ]

      # Keep collection under uiSelect for backwards compatibility
      service.industryOptions.uiSelect.collection = service.industryOptions.collection

  _translateJobLevelOptions = ->
    $translate([
      'talent.data.job_level.intern'
      'talent.data.job_level.starter'
      'talent.data.job_level.experienced'
      'talent.data.job_level.executive'
    ])
    .then (translations) ->
      service.jobLevelOptions.collection = [
        {
          value: 'intern'
          text: translations['talent.data.job_level.intern']
        },
        {
          value: 'starter'
          text: translations['talent.data.job_level.starter']
        },
        {
          value: 'experienced'
          text: translations['talent.data.job_level.experienced']
        },
        {
          value: 'executive'
          text: translations['talent.data.job_level.executive']
        }
      ]

      # Keep collection under uiSelect for backwards compatibility
      service.jobLevelOptions.uiSelect.collection = service.jobLevelOptions.collection

  _translateTermsOfEmploymentOptions = ->
    $translate([
      'talent.data.toe.full_time'
      'talent.data.toe.part_time'
      'talent.data.toe.self_employed'
    ])
    .then (translations) ->
      service.termsOfEmploymentOptions.collection = [
        {
          value: 'full_time'
          text: translations['talent.data.toe.full_time']
        }
        {
          value: 'part_time'
          text: translations['talent.data.toe.part_time']
        }
        {
          value: 'self_employed'
          text: translations['talent.data.toe.self_employed']
        }
      ]

      service.termsOfEmploymentOptions.uiSelect.collection = service.termsOfEmploymentOptions.collection

  _translateEducationOptions = ->
    $translate([
      'talent.data.education.intermediate'
      'talent.data.education.vocational'
      'talent.data.education.highschool'
      'talent.data.education.college'
      'talent.data.education.university'
      'talent.data.education.other'
    ])
    .then (translations) ->
      service.educationOptions.collection = [
        {
          value: 'intermediate'
          text: translations['talent.data.education.intermediate']
        },
        {
          value: 'vocational'
          text: translations['talent.data.education.vocational']
        },
        {
          value: 'highschool'
          text: translations['talent.data.education.highschool']
        },
        {
          value: 'college'
          text: translations['talent.data.education.college']
        },
        {
          value: 'university'
          text: translations['talent.data.education.university']
        },
        {
          value: 'other'
          text: translations['talent.data.education.other']
        }
      ]

      # Keep collection under uiSelect for backwards compatibility
      service.educationOptions.uiSelect.collection = service.educationOptions.collection

  _translateAcademicTitleOptions = ->
    $translate([
      'talent.data.academic_title.bc'
      'talent.data.academic_title.master'
      'talent.data.academic_title.mba'
      'talent.data.academic_title.phd'
      'talent.data.academic_title.other'
    ])
    .then (translations) ->
      service.industryOptions.collection = [
        {
          value: 'bc'
          text: translations['talent.data.academic_title.bc']
        },
        {
          value: 'master'
          text: translations['talent.data.academic_title.master']
        },
        {
          value: 'mba'
          text: translations['talent.data.academic_title.mba']
        },
        {
          value: 'phd'
          text: translations['talent.data.academic_title.phd']
        },
        {
          value: 'other'
          text: translations['talent.data.academic_title.other']
        }
      ]

      # Keep collection under uiSelect for backwards compatibility
      service.academicTitleOptions.uiSelect.collection = service.academicTitleOptions.collection

  _translateLanguageLevelOptions = ->
    $translate([
      'talent.data.language_level.beginner'
      'talent.data.language_level.intermediate'
      'talent.data.language_level.fluent'
      'talent.data.language_level.business_fluent'
      'talent.data.language_level.native'
    ])
    .then (translations) ->
      service.languageLevelOptions.collection = [
        {
          value: '1'
          text: translations['talent.data.language_level.beginner']
        },
        {
          value: '2'
          text: translations['talent.data.language_level.intermediate']
        },
        {
          value: '3'
          text: translations['talent.data.language_level.fluent']
        },
        {
          value: '4'
          text: translations['talent.data.language_level.business_fluent']
        },
        {
          value: '5'
          text: translations['talent.data.language_level.native']
        }
      ]

      # Keep collection under uiSelect for backwards compatibility
      service.languageLevelOptions.uiSelect.collection = service.languageLevelOptions.collection

  _translateSkillLevelOptions = ->
    $translate([
      'talent.data.skill_level.beginner'
      'talent.data.skill_level.intermediate'
      'talent.data.skill_level.advanced'
      'talent.data.skill_level.expert'
    ])
    .then (translations) ->
      service.skillLevelOptions.collection = [
        {
          value: '1'
          text: translations['talent.data.skill_level.beginner']
        }
        {
          value: '2'
          text: translations['talent.data.skill_level.intermediate']
        }
        {
          value: '3'
          text: translations['talent.data.skill_level.advanced']
        }
        {
          value: '4'
          text: translations['talent.data.skill_level.expert']
        }
      ]

      # Keep collection under uiSelect for backwards compatibility
      service.skillLevelOptions.uiSelect.collection = service.skillLevelOptions.collection

  _translateDocumentOptions = ->
    $translate([
      'talent.data.document.reference'
      'talent.data.document.recommendation'
      'talent.data.document.credentials'
      'talent.data.document.work_permit'
      'talent.data.document.certificate'
      'talent.data.document.other'
    ])
    .then (translations) ->
      service.documentOptions.collection = [
        {
          value: 'reference'
          text: translations['talent.data.document.reference']
        },
        {
          value: 'recommendation'
          text: translations['talent.data.document.recommendation']
        },
        {
          value: 'credentials'
          text: translations['talent.data.document.credentials']
        },
        {
          value: 'work_permit'
          text: translations['talent.data.document.work_permit']
        },
        {
          value: 'certificate'
          text: translations['talent.data.document.certificate']
        },
        {
          value: 'other'
          text: translations['talent.data.document.other']
        }
      ]

      # Keep collection under uiSelect for backwards compatibility
      service.documentOptions.uiSelect.collection = service.documentOptions.collection

  _translateJobInterestOptions = ->
    $translate([
      'talent.data.job_interest.very_much'
      'talent.data.job_interest.moderately'
      'talent.data.job_interest.not_searching'
      'talent.data.job_interest.do_not_contact'
    ])
    .then (translations) ->
      service.jobInterestOptions.collection = [
        {
          value: 'very_much'
          text: translations['talent.data.job_interest.very_much']
        }
        {
          value: 'moderately'
          text: translations['talent.data.job_interest.moderately']
        }
        {
          value: 'not_searching'
          text: translations['talent.data.job_interest.not_searching']
        }
        {
          value: 'do_not_contact'
          text: translations['talent.data.job_interest.do_not_contact']
        }
      ]

      # Keep collection under uiSelect for backwards compatibility
      service.jobInterestOptions.uiSelect.collection = service.jobInterestOptions.collection

  _translateAvailableOptions = ->
    $translate([
      'talent.data.available.immediately'
      'talent.data.available.within_month'
      'talent.data.available.within_2_months'
      'talent.data.available.within_3_months'
      'talent.data.available.within_4-6_months'
      'talent.data.available.within_6+_months'
    ])
    .then (translations) ->
      service.availableOptions.collection = [
        {
          value: '0'
          text: translations['talent.data.available.immediately']
        }
        {
          value: '1'
          text: translations['talent.data.available.within_month']
        }
        {
          value: '2'
          text: translations['talent.data.available.within_2_months']
        }
        {
          value: '3'
          text: translations['talent.data.available.within_3_months']
        }
        {
          value: '4-6'
          text: translations['talent.data.available.within_4-6_months']
        }
        {
          value: '6+'
          text: translations['talent.data.available.within_6+_months']
        }
      ]

      # Keep collection under uiSelect for backwards compatibility
      service.availableOptions.uiSelect.collection = service.availableOptions.collection

  _translateReferrerOptions = ->
    $translate([
      'talent.data.referrer.company_website'
      'talent.data.referrer.job_agency'
      'talent.data.referrer.online_jobplatform'
      'talent.data.referrer.personal_recommendation'
      'talent.data.referrer.printed_media'
      'talent.data.referrer.social_media'
      'talent.data.referrer.other_source'
    ])
    .then (translations) ->
      service.referrerOptions.collection = [
        {
          value: 'company_website'
          text: translations['talent.data.referrer.company_website']
        }
        {
          value: 'job_agency'
          text: translations['talent.data.referrer.job_agency']
        }
        {
          value: 'online_jobplatform'
          text: translations['talent.data.referrer.online_jobplatform']
        }
        {
          value: 'personal_recommendation'
          text: translations['talent.data.referrer.personal_recommendation']
        }
        {
          value: 'printed_media'
          text: translations['talent.data.referrer.printed_media']
        }
        {
          value: 'social_media'
          text: translations['talent.data.referrer.social_media']
        }
        {
          value: 'other_source'
          text: translations['talent.data.referrer.other_source']
        }
      ]

      # Keep collection under uiSelect for backwards compatibility
      service.referrerOptions.uiSelect.collection = service.referrerOptions.collection

  _translateNewsletterUpdateOptions = ->
    $translate([
      'talent.data.newsletter.enabled'
      'talent.data.newsletter.minimum'
      'talent.data.newsletter.disabled'
    ])
    .then (translations) ->
      service.newsletterUpdateOptions.collection = [
        {
          value: 'enabled'
          text: translations['talent.data.newsletter.enabled']
        },
        {
          value: 'minimum'
          text: translations['talent.data.newsletter.minimum']
        },
        {
          value: 'disabled'
          text: translations['talent.data.newsletter.disabled']
        }
      ]

  _translateSalaryOptions = ->
    $translate([
      'talent.data.salary.smaller'
      'talent.data.salary.between'
      'talent.data.salary.bigger'
    ])
    .then (translations) ->
      service.salaryOptions.collection = [
        {
          value: 0
          text: $translate.instant('talent.data.salary.smaller', { to: '4.000' })
        },
        {
          value: 4000
          text: $translate.instant('talent.data.salary.between', { from: '4.000', to: '10.000' })
        },
        {
          value: 10000
          text: $translate.instant('talent.data.salary.between', { from: '10.000', to: '20.000' })
        },
        {
          value: 20000
          text: $translate.instant('talent.data.salary.between', { from: '20.000', to: '30.000' })
        },
        {
          value: 30000
          text: $translate.instant('talent.data.salary.between', { from: '30.000', to: '40.000' })
        },
        {
          value: 40000
          text: $translate.instant('talent.data.salary.between', { from: '40.000', to: '50.000' })
        },
        {
          value: 50000
          text: $translate.instant('talent.data.salary.between', { from: '50.000', to: '70.000' })
        },
        {
          value: 70000
          text: $translate.instant('talent.data.salary.between', { from: '70.000', to: '90.000' })
        },
        {
          value: 90000
          text: $translate.instant('talent.data.salary.between', { from: '90.000', to: '110.000' })
        },
        {
          value: 110000
          text: $translate.instant('talent.data.salary.between', { from: '110.000', to: '130.000' })
        },
        {
          value: 130000
          text: $translate.instant('talent.data.salary.between', { from: '130.000', to: '150.000' })
        },
        {
          value: 150000
          text: $translate.instant('talent.data.salary.bigger', { from: '150.000' })
        }
      ]

      # Keep collection under uiSelect for backwards compatibility
      service.salaryOptions.uiSelect.collection = service.salaryOptions.collection


  service =

    load: ->
      promises = [
        _loadCountryOptions()
        _loadLanguageOptions()
      ]

      $q.all(promises)

    translate: ->
      promises = [
        _translateGenderOptions()
        _translateSalutationOptions()
        _translateIndustryOptions()
        _translateJobLevelOptions()
        _translateTermsOfEmploymentOptions()
        _translateEducationOptions()
        _translateAcademicTitleOptions()
        _translateLanguageLevelOptions()
        _translateSkillLevelOptions()
        _translateDocumentOptions()
        _translateJobInterestOptions()
        _translateAvailableOptions()
        _translateReferrerOptions()
        _translateNewsletterUpdateOptions()
        _translateSalaryOptions()
      ]

      $q.all(promises)

    genderOptions:
      uiSelect:
        searchEnabled: false
        bindProperty: 'value'
        formatResult: (item) ->
          item.text if item?
        formatSelection: (item) ->
          item.text if item?
        collection: []
      collection: []

    salutationOptions:
      uiSelect:
        searchEnabled: false
        bindProperty: 'value'
        formatResult: (item) ->
          item.text if item?
        formatSelection: (item) ->
          item.text if item?
        collection: []
      collection: []

    levelOptions:
      uiSelect:
        searchEnabled: false
        bindProperty: 'value'
        formatResult: (item) ->
          item.text if item?
        formatSelection: (item) ->
          item.text if item?
        collection: []
      collection: []

    countryOptions:
      uiSelect:
        formatResult: (item) ->
          item.name if item?
        formatSelection: (item) ->
          item.name if item?
        collection: []
      collection: []

    academicTitleOptions:
      uiSelect:
        formatResult: (item) ->
          item.name if item?
        formatSelection: (item) ->
          item.name if item?
        collection: []
      collection: []

    stateOptions:
      uiSelect:
        searchEnabled: false
        formatResult: (item) ->
          item.name if item?
        formatSelection: (item) ->
          item.name if item?
        collection: []
      collection: []

    languageOptions:
      uiSelect:
        formatSelection: (item) ->
          item.name if item?
        formatResult: (item) ->
          item.name if item?
        collection: []
      collection: []

    careerLevelOptions:
      uiSelect:
        searchEnabled: false
        bindProperty: 'value'
        formatResult: (item) ->
          item.text if item?
        formatSelection: (item) ->
          item.text if item?
        collection: []
      collection: []

    jobLevelOptions:
      uiSelect:
        searchEnabled: false
        bindProperty: 'value'
        formatResult: (item) ->
          item.text if item?
        formatSelection: (item) ->
          item.text if item?
        collection: []
      collection: []

    languageLevelOptions:
      uiSelect:
        searchEnabled: false
        bindProperty: 'value'
        formatResult: (item) ->
          item.text if item?
        formatSelection: (item) ->
          item.text if item?
        collection: []
      collection: []

    skillLevelOptions:
      uiSelect:
        searchEnabled: false
        bindProperty: 'value'
        formatResult: (item) ->
          item.text if item?
        formatSelection: (item) ->
          item.text if item?
        collection: []
      collection: []

    newsletterUpdateOptions:
      radios:
        collection: []
      collection: []

    referrerOptions:
      uiSelect:
        searchEnabled: false
        bindProperty: 'value'
        formatResult: (item) ->
          item.text if item?
        formatSelection: (item) ->
          item.text if item?
        collection: []
      collection: []

    termsOfEmploymentOptions:
      uiSelect:
        searchEnabled: false
        bindProperty: 'value'
        formatResult: (item) ->
          item.text if item?
        formatSelection: (item) ->
          item.text if item?
        collection: []
      collection: []

    educationOptions:
      uiSelect:
        searchEnabled: false
        bindProperty: 'value'
        formatResult: (item) ->
          item.text if item?
        formatSelection: (item) ->
          item.text if item?
        collection: []
      collection: []

    industryOptions:
      uiSelect:
        bindProperty: 'value'
        formatResult: (item) ->
          item.text if item?
        formatSelection: (item) ->
          item.text if item?
        collection: []
      collection: []

    documentOptions:
      uiSelect:
        bindProperty: 'value'
        searchEnabled: false
        formatResult: (item) ->
          item.text if item?
        formatSelection: (item) ->
          item.text if item?
        collection: []
      collection: []

    salaryOptions:
      uiSelect:
        bindProperty: 'value'
        searchEnabled: false
        formatResult: (item) ->
          item.text if item?
        formatSelection: (item) ->
          item.text if item?
        collection: []
      collection: []

    jobInterestOptions:
      uiSelect:
        bindProperty: 'value'
        searchEnabled: false
        formatResult: (item) ->
          item.text if item?
        formatSelection: (item) ->
          item.text if item?
        collection: []
      collection: []

    availableOptions:
      uiSelect:
        bindProperty: 'value'
        searchEnabled: false
        formatResult: (item) ->
          item.text if item?
        formatSelection: (item) ->
          item.text if item?
        collection: []
      collection: []

  service
