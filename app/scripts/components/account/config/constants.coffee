angular.module("veetaTalentApp.account")
.constant "ACCOUNT",
  VIEWS:
    DETAIL: "views/components/account/detail.html"

  PARTIALS:
    PROFILE: "views/components/account/partials/profile.html"
    AVAILABILITY: "views/components/account/partials/availability.html"
    SETTINGS: "views/components/account/partials/settings.html"
    DELETE: "views/components/account/partials/delete.html"
    TALENT_PROFILE: "views/components/account/partials/talent-profile.html"

    MODAL:
      DELETE_CONFIRMATION: "views/components/account/partials/modal/delete-confirmation.html"
      UPDATE_CONFIRMATION: "views/components/account/partials/modal/update-confirmation.html"