###*
  # @ngdoc overview
  # @name sharing
  #
###

angular.module 'veetaTalentApp.sharing', [
  'veetaTalentApp.sharing.controllers'
  'veetaTalentApp.sharing.directives'
  'veetaTalentApp.sharing.resources'
]

angular.module 'veetaTalentApp.sharing.controllers', []
angular.module 'veetaTalentApp.sharing.directives', []
angular.module 'veetaTalentApp.sharing.resources', ['rails']
