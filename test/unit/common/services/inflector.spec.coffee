describe "veetaTalentApp.common", ->
  describe "services", ->
    describe "Inflector", ->
      beforeEach module('veetaTalentApp.common.services')

      InflectorService = null

      beforeEach inject (_InflectorService_) ->
        InflectorService = _InflectorService_

      it 'camelizeString()', ->
        expect(InflectorService.camelizeString('abc_abc')).toBe('abcAbc')

      it 'camelizeObject()', ->
        srcObject =
          "first_name": "John"
          "last_name": "Doe"
          "address":
            "address_line": "Auhofstraße 8"
            "country_id": 1
            "country":
              "id": 1
              "name": "Austria"
              "iso_code": "AT"
          "work_experiences": [
            {
              "id": 2
              "position": "CEO"
              "resume_id": 1
              "country":
                "id": 1
                "iso_code": "AT"
            }
            {
              "id": 3
              "position": "CEO 2"
              "resume_id": 2
              "country":
                "id": 2
                "iso_code": "AT 2"
            }
          ]


        destObject =
          "firstName": "John"
          "lastName": "Doe"
          "address":
            "addressLine": "Auhofstraße 8"
            "countryId": 1
            "country":
              "id": 1
              "name": "Austria"
              "isoCode": "AT"
          "workExperiences": [
            {
              "id": 2
              "position": "CEO"
              "resumeId": 1
              "country":
                "id": 1
                "isoCode": "AT"
            }
            {
              "id": 3
              "position": "CEO 2"
              "resumeId": 2
              "country":
                "id": 2
                "isoCode": "AT 2"
            }
          ]

        expect(InflectorService.camelizeObject(srcObject)).toEqual(destObject)
