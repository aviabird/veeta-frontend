SharingOverviewPage = require './pages/sharing/overview.page'
#resumesFullListResults = require '../../api_mocks/resumes/list.json'

describe 'Sharing', ->
  sharingOverviewPage = undefined

  beforeEach ->
    sharingOverviewPage = new SharingOverviewPage()
    sharingOverviewPage.get()

  describe 'has a overview page', ->
    it 'should has a list of sharings', ->
      expect(sharingOverviewPage.sharingsList.count()).toEqual(3)

