describe "veetaTalentApp", ->
  describe 'account', ->
    describe 'directives', ->
      describe 'accountDelete', ->
        beforeEach module(
          'veetaTalentApp.account'
          'veetaTalentApp.common'
          "views/components/account/partials/delete.html"
        )

        $document = null
        $scope = null
        $modal = null
        elementScope = null
        $httpBackend = null
        TalentAuth = null

        beforeEach inject (_$rootScope_
                           _$document_
                           _$modal_
                           _$httpBackend_
                           _TalentAuth_) ->
          $scope = _$rootScope_.$new()
          $document = _$document_
          $modal = _$modal_
          $httpBackend = _$httpBackend_
          TalentAuth = _TalentAuth_

        beforeEach inject (_$compile_) ->
          element = angular.element '<div account-delete=""></div>'
          $httpBackend.whenGET(/(api\/locales)/).respond()
          _$compile_(element)($scope)
          $scope.$apply()
          elementScope = element.isolateScope()

        describe 'initialize', ->
          it 'should initialize a new Talent Auth Instant', ->
            talentAuth = new TalentAuth
              deleteReason: ''
              withOtherDeleteReason: false
              otherDeleteReason: ''
              deleteAgreement: false
            expect(elementScope.talentAuth).toEqual talentAuth

          it 'should set deleteAccountMode as false', ->
            expect(elementScope.deleteAccountMode).toBeFalsy()

          it 'should set delete button disabled', ->
            expect(elementScope.deleteButtonEnabled()).toBeFalsy()

        describe 'onDeleteAccountMode()', ->
          it 'should set deleteAccountMode to true', ->
            elementScope.onDeleteAccountMode()
            expect(elementScope.deleteAccountMode).toBeTruthy()

        describe 'onCancelDeleteAccountMode()', ->
          it 'should set deleteAccountMode to false', ->
            elementScope.onCancelDeleteAccountMode()
            expect(elementScope.deleteAccountMode).toBeFalsy()

        describe '$on($translateChangeSuccess)', ->

        describe '$watch(talentAuth.deleteReason)', ->
          it 'should set withOtherDeleteReason false if above reason be selected', ->
            expect(elementScope.talentAuth.withOtherDeleteReason).toBeFalsy()
            elementScope.talentAuth.withOtherDeleteReason = true
            expect(elementScope.talentAuth.withOtherDeleteReason).toBeTruthy()
            elementScope.talentAuth.deleteReason = 'some reason'
            $scope.$apply()
            expect(elementScope.talentAuth.withOtherDeleteReason).toBeFalsy()

        describe 'selectOtherDeleteReason()', ->
          it 'should clear deleteReason', ->
            elementScope.selectOtherDeleteReason()
            expect(elementScope.talentAuth.deleteReason).toBeNull

        describe 'openDeleteAccountConfirmationModal()', ->
          beforeEach ->
            fakeModalInstance =
              close: jasmine.createSpy("modalInstance.close")
              dismiss: jasmine.createSpy("modalInstance.dismiss")
              result:
                then: jasmine.createSpy("modalInstance.result.then")

            spyOn($modal, 'open').andReturn(fakeModalInstance);

          it 'should open the modal window', ->
            elementScope.openDeleteAccountConfirmationModal()
            expect($modal.open).toHaveBeenCalled

          it 'should not open the modal window if delete disabled', ->
            # todo check spyon function here
            elementScope.openDeleteAccountConfirmationModal()
            expect($modal.open).not.toHaveBeenCalled

        describe 'deleteButtonEnabled()', ->
          it 'should enable delete button if the conditions be matched', ->
            expect(elementScope.deleteButtonEnabled()).toBeFalsy()
            elementScope.talentAuth.deleteAgreement = true
            expect(elementScope.deleteButtonEnabled()).toBeFalsy()
            elementScope.talentAuth.deleteReason = 'some reason'
            expect(elementScope.deleteButtonEnabled()).toBeTruthy()
            elementScope.talentAuth.deleteReason = ''
            expect(elementScope.deleteButtonEnabled()).toBeFalsy()
            elementScope.talentAuth.withOtherDeleteReason = true
            elementScope.talentAuth.otherDeleteReason = ''
            expect(elementScope.deleteButtonEnabled()).toBeFalsy()
            elementScope.talentAuth.otherDeleteReason = 'some reason'
            expect(elementScope.deleteButtonEnabled()).toBeTruthy()

      describe "veetaTalentApp.account.directives:accountDeleteConfirmationModalCtrl", ->

        $scope = undefined
        $controller = undefined
        $httpBackend = undefined
        talentAuth = undefined

        beforeEach module(
          'veetaTalentApp.account'
          'veetaTalentApp.common'
          "views/components/account/partials/modal/delete-confirmation.html"
        )

        beforeEach inject (_$httpBackend_
                           _$rootScope_
                           _$controller_
                           _TalentAuth_) ->
          $httpBackend = _$httpBackend_
          $scope = _$rootScope_.$new()

          # mock a talentAuth
          talentAuth = new _TalentAuth_()

          # Create a mock modalInstance using spies
          modalInstance =
            close: jasmine.createSpy("modalInstance.close")
            dismiss: jasmine.createSpy("modalInstance.dismiss")
            result:
              then: jasmine.createSpy("modalInstance.result.then")

          # create controller object
          $controller = _$controller_ 'accountDeleteConfirmationModalCtrl',
            $scope: $scope
            $modalInstance: modalInstance
            talentAuth: talentAuth

        it 'can delete a account', ->
      #    expect($scope.talentAuth.$url()).toBe()
      #    $scope.talentAuth.$put("delete")
          $scope.delete()


