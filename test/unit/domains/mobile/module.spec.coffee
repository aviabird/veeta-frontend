describe "veetaTalentApp.mobile", ->
  xdescribe "module", ->
    module = undefined

    beforeEach ->
      module = angular.module('veetaTalentApp.mobile')

    it "should be registered", ->
      expect(module).toBeDefined()

    describe "Dependencies:", ->
      deps = undefined

      hasModule = (m)->
        deps.indexOf(m) >= 0

      beforeEach ->
        deps = module.value('appName').requires

      # components
  #    it "should have veetaTalentApp.resume as a dependency", ->
  #      expect(hasModule('veetaTalentApp.resume')).toBeTruthy()

      it "should have veetaTalentApp.jobapp as a dependency", ->
        expect(hasModule('veetaTalentApp.jobapp')).toBeTruthy()

  #    it "should have veetaTalentApp.account as a dependency", ->
  #      expect(hasModule('veetaTalentApp.account')).toBeTruthy()

      # modules
      it "should have veetaTalentApp.mobile.controllers as a dependency", ->
        expect(hasModule('veetaTalentApp.mobile.controllers')).toBeTruthy()

      it "should have veetaTalentApp.mobile.directives as a dependency", ->
        expect(hasModule('veetaTalentApp.mobile.directives')).toBeTruthy()
