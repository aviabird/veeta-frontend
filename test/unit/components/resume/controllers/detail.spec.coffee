describe "veetaTalentApp.resume", ->
  describe "controllers", ->
    describe 'ResumeDetailCtrl', ->
      beforeEach module(
        "veetaTalentApp.resume"
        'mocked/resumes/detail.json'
      )

      $rootScope = null
      $scope = undefined
      $controller = undefined
      $httpBackend = undefined
      resume = undefined

      beforeEach inject (_$httpBackend_, _$rootScope_, _$controller_, _$stateParams_, _FileUploader_, Resume, _mockedResumesDetail_, OptionsService) ->
        $httpBackend = _$httpBackend_
        $rootScope = _$rootScope_
        $scope = $rootScope.$new()

        # mock a resume
        resume = new Resume(_mockedResumesDetail_)


        $rootScope.talent =
          firstName: 'John'
          lastName: 'Smith'
          salutation: 'male'

        # create controller object
        $controller = _$controller_ 'ResumeDetailCtrl',
          $scope: $scope
          $rootScope: $rootScope
          $stateParams: _$stateParams_
          FileUploader: _FileUploader_
          OptionsService: OptionsService
          resume: resume

      describe 'initialize', ->
        it 'should get a resume from state router', ->
          expect($scope.resume.id).toBe(1)

      describe 'shareResume()', ->

      describe 'onTitleEdit()', ->
        it 'Title edit mode should be off by init', ->
          expect($scope.titleEditMode).toBeFalsy

        it 'Title change should be able to save', ->
          $scope.onTitleEdit
          expect($scope.titleEditMode).toBeTruthy

          $scope.resume.name = 'new Name'

          $httpBackend.expectPUT($scope.resume.$url()).respond(200)
          $scope.updateResumeName

          expect($scope.resume.name).toBe 'new Name'

      describe 'updateResumeName()', ->

      describe 'onSave()', ->