module.exports = (grunt, appConfig) ->
  tasks:
  # Automatically inject Bower components into the app
    wiredep:
      app:
        src: ["#{appConfig.paths.app}/index.jade", "#{appConfig.paths.app}/mobile.jade"]
        ignorePath: /\.\.\//

      sass:
        src: ["#{appConfig.paths.app}/styles/{,*/}*.{scss,sass}"]
        ignorePath: /(\.\.\/){1,2}bower_components\//


      karma:
        src: ["#{appConfig.paths.test}/karma-dev.conf.coffee"]
        ignorePath: /\.\.\//
        devDependencies: true
        fileTypes:
          coffee:
            block: /(([ \t]*)\#\s*bower:*(\S*))(\n|\r|.)*?(\#\s*endbower)/gi
            detect: {}
            replace:
              js: '\'{{filePath}}\''

  # Automatically inject scripts and styles into the app
    injector:
      common:
        options:
          starttag: "// injector:common:js"
          endtag: "// endinjector"
          ignorePath: "app/"
          addRootSlash: false
          transform: (filepath) ->
            e = require("path").extname(filepath).slice(1)
            if (e is 'coffee')
              filepath = filepath.replace /.coffee/, ".js"
            'script(src="' + filepath + '")'
        files:
          "<%= paths.app %>/index.jade": appConfig.paths.scripts.common
          "<%= paths.app %>/mobile.jade": appConfig.paths.scripts.common

      desktop:
        options:
          starttag: "// injector:desktop:js"
          endtag: "// endinjector"
          ignorePath: "app/"
          addRootSlash: false
          transform: (filepath) ->
            e = require("path").extname(filepath).slice(1)
            if (e is 'coffee')
              filepath = filepath.replace /.coffee/, ".js"
            'script(src="' + filepath + '")'
        files:
          "<%= paths.app %>/index.jade": appConfig.paths.scripts.desktop


      mobile:
        options:
          starttag: "// injector:mobile:js"
          endtag: "// endinjector"
          ignorePath: "app/"
          addRootSlash: false
          transform: (filepath) ->
            e = require("path").extname(filepath).slice(1)
            if (e is 'coffee')
              filepath = filepath.replace /.coffee/, ".js"
            'script(src="' + filepath + '")'
        files:
          "<%= paths.app %>/mobile.jade": appConfig.paths.scripts.mobile

      karma:
        options:
          starttag: "# injector"
          endtag: "# endinjector"
#        ignorePath: "ngapp/<%= paths.appName %>/"
          addRootSlash: false
          transform: (filepath) ->
            e = require("path").extname(filepath).slice(1)
            return "'" + filepath + "'"  if e is "js" or e is "coffee"
        files:
          "<%= paths.test %>/karma-dev.conf.coffee": [
            appConfig.paths.scripts.common
            appConfig.paths.scripts.desktop
            appConfig.paths.scripts.mobile
          ]