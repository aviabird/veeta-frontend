describe "veetaTalentApp.mobile", ->
  xdescribe "states", ->
    beforeEach module('veetaTalentApp.mobile')

    $rootScope = undefined
    $state = undefined


    beforeEach inject (_$rootScope_
                       _$state_) ->
      $rootScope = _$rootScope_
      $state = _$state_

    describe "app", ->
      state = "app"
      it "verify state configuration", ->
        config = $state.get(state)
        expect(config.abstract).toBeTruthy()
        expect(config.url).toBeUndefined()
