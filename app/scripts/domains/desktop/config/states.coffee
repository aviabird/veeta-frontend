"use strict"

angular.module("veetaTalentApp.desktop")
.config ($stateProvider
         $urlRouterProvider
         $locationProvider
         LAYOUTS
         RESUME
         SHARING
         ACCOUNT
         JOBAPP
         AUTH
         DESKTOP) ->
  $urlRouterProvider.otherwise "/"

  # enable html5Mode for pushstate ('#'-less URLs)
  #  $locationProvider.html5Mode(true)
  #  $locationProvider.hashPrefix('!')

  $stateProvider
  .state "app",
    abstract: true
    templateUrl: LAYOUTS.VIEWS.DESKTOP
    controller: "AppCtrl"
    resolve:
      bootstrap: ($timeout, TranslateService, OptionsService, $translate, $rootScope) ->
        TranslateService.recognizeDefaultLanguage()
        $translate.use($rootScope.currentTranslate.key).then ->
          OptionsService.load()
          OptionsService.translate()

  .state 'app.welcome',
    url: '/'
    templateUrl: DESKTOP.VIEWS.WELCOME
    controller: 'WelcomeCtrl'

  .state 'app.signIn',
    url: '/sign-in',
    templateUrl: AUTH.VIEWS.SIGN_IN
    controller: 'SignInCtrl'

  .state 'app.signUp',
    url: '/sign-up'
    templateUrl: AUTH.VIEWS.SIGN_UP
    controller: 'SignUpCtrl'

  .state 'app.requestPassword',
    url: '/request-password'
    templateUrl: AUTH.VIEWS.REQUEST_PASSWORD

  .state 'app.resetPassword',
    url: '/reset-password'
    templateUrl: AUTH.VIEWS.RESET_PASSWORD

  .state 'app.job',
    url: '/jobs/:jobToken'
    templateUrl: JOBAPP.VIEWS.JOB
    controller: 'JobCtrl'
    resolve:
      job: (Job, $stateParams) ->
        Job.get($stateParams.jobToken).then (response)->
          response

  .state "app.inside",
    abstract: true
    template: '<ui-view />'
    controller: 'InsideCtrl'
    resolve:
      auth: ($rootScope, $timeout, $q, $auth, Talent, TalentSetting)->

        # TODO: Use $q.all more at OptionsService.coffee
        #
        deferred = $q.defer()

        dTasksDone = 0

        auth = {}

        _doAuth = ->
          $auth.validateUser().then (response)->
            auth = response

            Talent.query().then (response) ->
              auth.talent = response
              dTasksDone = dTasksDone + 1
              deferred.resolve(auth) if dTasksDone is 2
            TalentSetting.query().then (response) ->
              auth.talentSetting = response
              dTasksDone = dTasksDone + 1
              deferred.resolve(auth) if dTasksDone is 2
          , (reject) ->
            deferred.reject(reject)

        $timeout _doAuth, 300

        deferred.promise


  .state "app.inside.resumes", # Resume States
    abstract: true
    url: "/resumes"
    template: "<ui-view/>"

  .state "app.inside.resumes.overview",
    url: "/overview"
    templateUrl: RESUME.VIEWS.OVERVIEW
    controller: "ResumeOverviewCtrl"

  .state "app.inside.resumes.new",
    url: "/new"
    templateUrl: RESUME.VIEWS.DETAIL
    controller: "ResumeDetailCtrl"
    resolve:
      resume: (Resume) ->
        Resume.get("new").then (data) ->
          data

  .state "app.inside.resumes.detail",
    url: "/:id"
    templateUrl: RESUME.VIEWS.DETAIL
    controller: "ResumeDetailCtrl"
    resolve:
      resume: (Resume, $stateParams) ->
        Resume.get($stateParams.id).then (data) ->
          data

  .state "app.inside.jobapps", # Jobapps States
    abstract: true
    url: "/jobapps"
    template: "<ui-view/>"


  .state "app.inside.jobapps.overview",
    url: "/overview"
    templateUrl: JOBAPP.VIEWS.OVERVIEW
    controller: "JobappOverviewCtrl"

  .state "app.inside.jobapps.new",
    url: "/new"
    templateUrl: JOBAPP.VIEWS.NEW.WRAPPER
    controller: "JobappNewCtrl"
    resolve:
      jobapp: (Jobapp) ->
        Jobapp.get("new").then (data) ->
          data

  .state "app.inside.jobapps.new.resume",
    url: "/resume"
    template: '<ui-view />'
    controller: 'JobappNewResumeCtrl'

  .state "app.inside.jobapps.new.resume.select",
    url: "/select"
    templateUrl: JOBAPP.VIEWS.NEW.RESUME_SELECT
    controller: "JobappNewResumeSelectCtrl"

  .state "app.inside.jobapps.new.resume.new",
    url: "/new"
    templateUrl: JOBAPP.VIEWS.NEW.RESUME_DETAIL
    controller: "JobappNewResumeDetailCtrl"
    resolve:
      resume: (Resume) ->
        Resume.get("new").then (response) ->
          response

  .state "app.inside.jobapps.new.resume.detail",
    url: "/:resumeId"
    templateUrl: JOBAPP.VIEWS.NEW.RESUME_DETAIL
    controller: "JobappNewResumeDetailCtrl"
    resolve:
      resume: (Resume, $stateParams) ->
        Resume.get($stateParams.resumeId).then (response) ->
          response

  .state "app.inside.jobapps.new.addCoverLetter",
    url: "/add_cover_letter"
    templateUrl: JOBAPP.VIEWS.NEW.ADD_COVER_LETTER
    controller: "JobappNewAddCoverLetterCtrl"

  .state "app.inside.jobapps.new.checkAndSend",
    url: "/check_and_send"
    templateUrl: JOBAPP.VIEWS.NEW.CHECK_AND_SEND
    controller: "JobappNewCheckAndSendCtrl"


  .state "app.inside.jobapps.detail",
    url: "/:id"
    templateUrl: JOBAPP.VIEWS.DETAIL
    controller: "JobappDetailCtrl"
    resolve:
      jobapp: (Jobapp, $stateParams) ->
        Jobapp.get($stateParams.id).then (data) ->
          data

  .state "app.inside.jobappAssistant", # JobappAssistant States
    url: '/jobapp-assistant/:jobId'
    templateUrl: JOBAPP.VIEWS.NEW.WRAPPER
    controller: "JobappNewCtrl"
    resolve:
      jobapp: ($q, $timeout, Jobapp, $rootScope, $stateParams, $state) ->
        deferred = $q.defer()
        Jobapp.getNew($stateParams.jobId).then (response) ->
          if response.job and response.job.id and response.jobId
            console.log response
            if response.job.available
#
              deferred.resolve(response)
            else
              $timeout $rootScope.$broadcast('$window.redirectTo', URL.BASE + '/jobs/' + response.job.id), 10000
          else
            $rootScope.$broadcast('$window.redirectTo', URL.BASE)
        , ->
          $rootScope.$broadcast('$window.redirectTo', URL.BASE)

        return deferred.promise

  .state "app.inside.jobappAssistant.resume",
    url: '/resume'
    template: '<ui-view />'
    controller: 'JobappNewResumeCtrl'

  .state "app.inside.jobappAssistant.resume.select",
    url: "/select"
    templateUrl: JOBAPP.VIEWS.NEW.RESUME_SELECT
    controller: "JobappNewResumeSelectCtrl"

  .state "app.inside.jobappAssistant.resume.new",
    url: "/new"
    templateUrl: JOBAPP.VIEWS.NEW.RESUME_DETAIL
    controller: "JobappNewResumeDetailCtrl"
    resolve:
      resume: (Resume) ->
        Resume.get("new").then (response) ->
          response

  .state "app.inside.jobappAssistant.resume.detail",
    url: "/:resumeId"
    templateUrl: JOBAPP.VIEWS.NEW.RESUME_DETAIL
    controller: "JobappNewResumeDetailCtrl"
    resolve:
      resume: (Resume, $stateParams) ->
        Resume.get($stateParams.resumeId).then (response) ->
          response

  .state "app.inside.jobappAssistant.addCoverLetter",
    url: "/add_cover_letter"
    templateUrl: JOBAPP.VIEWS.NEW.ADD_COVER_LETTER
    controller: "JobappNewAddCoverLetterCtrl"

  .state "app.inside.jobappAssistant.checkAndSend",
    url: "/check_and_send"
    templateUrl: JOBAPP.VIEWS.NEW.CHECK_AND_SEND
    controller: "JobappNewCheckAndSendCtrl"


  .state "app.inside.account", # Account States
    url: "/settings"
    templateUrl: ACCOUNT.VIEWS.DETAIL
    controller: "AccountDetailCtrl"
    resolve:
      talent: (Talent) ->
        Talent.query().then (data) ->
          data

      talentSetting: (TalentSetting) ->
        TalentSetting.query().then (data) ->
          data

  .state "app.inside.sharings", # Sharings States
    abstract: true
    url: "/sharings"
    template: "<ui-view/>"

  .state "app.inside.sharings.overview",
    url: "/overview"
    templateUrl: SHARING.VIEWS.OVERVIEW
    controller: "SharingOverviewCtrl"

# TODO: After handling 404 error
.run ($rootScope, $state, $timeout) ->
  $rootScope.$on "$stateChangeStart", (event, toState, toParams, fromState, fromParams) ->
    if toState.name is "app.resumes.new"
      unless fromState.name is "app.resumes.overview"
        event.preventDefault()
        $timeout (->
          event.currentScope.$apply ->
            $state.go "app.inside.resumes.overview"
        ), 300
