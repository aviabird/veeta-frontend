describe "veetaTalentApp.common", ->
  describe "module", ->
    module = undefined

    beforeEach ->
      module = angular.module('veetaTalentApp.common')

    it "should be registered", ->
      expect(module).toBeDefined()

    describe "Dependencies:", ->
      deps = undefined

      hasModule = (m)->
        deps.indexOf(m) >= 0

      beforeEach ->
        deps = module.value('appName').requires

      it "should have veetaTalentApp.common.controllers as a dependency", ->
        expect(hasModule('veetaTalentApp.common.controllers')).toBeTruthy()

      it "should have veetaTalentApp.common.directives as a dependency", ->
        expect(hasModule('veetaTalentApp.common.directives')).toBeTruthy()

      it "should have  veetaTalentApp.common.resources as a dependency", ->
        expect(hasModule('veetaTalentApp.common.resources')).toBeTruthy()

      it "should have  veetaTalentApp.common.services as a dependency", ->
        expect(hasModule('veetaTalentApp.common.services')).toBeTruthy()

      it "should have  veetaTalentApp.common.filters as a dependency", ->
        expect(hasModule('veetaTalentApp.common.filters')).toBeTruthy()