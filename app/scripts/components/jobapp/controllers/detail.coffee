"use strict"

###*
  # @ngdoc controller
  # @name jobapp.controllers:JobappDetailCtrl
  #
  # @requires $scope
  # @requires $modal
  # @requires jobapp
  # @requires JOBAPP
  #
  # @description
  # Controller for Jobapp Detail
###

angular.module 'veetaTalentApp.jobapp.controllers'
.controller 'JobappDetailCtrl', ($scope, $modal, jobapp, JOBAPP)->
  $scope.jobapp = jobapp

  $scope.isWithdrawn = ->
    $scope.jobapp.withdrawnAt != null

  $scope.isWithSharingCompanies = ->
    $scope.jobapp.resume && $scope.jobapp.resume.companies && $scope.jobapp.resume.companies.length > 0

  $scope.openWithdrawConfirmModal = ->
    modalInstance = $modal.open
      templateUrl: JOBAPP.PARTIALS.WITHDRAWN_CONFIRM
      controller: 'JobappWithdrawConfirmationCtrl'
      resolve:
        jobapp: ->
          jobapp

    modalInstance.result.then ->
      $scope.jobapp.get()