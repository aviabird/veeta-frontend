"use strict"

###*
  # @ngdoc directive
  # @name resume.directives:resume-language
  #
  # @requires $state
  # @requires RESUME
  # @requires OptionsService
  #
  # @description
  # change language of a resume
  # be used on resume detail page
###

angular.module 'veetaTalentApp.resume.directives'
.directive 'resumeLanguage', ($state, RESUME, OptionsService)->
  restrict: "AE"
  templateUrl: RESUME.PARTIALS.LANGUAGE
  replace: true
  scope: {}
  controller: ($scope) ->

    # ----------------------------------------
    # Initialize attributes
    # ----------------------------------------
    $scope.resume = angular.copy($scope.$parent.resume)
    $scope.languageOptions = OptionsService.languageOptions

    $scope.updateLanguage = ->
      $scope.resume.updateOnly('language').then (response)->
        $scope.$parent.resume.language = $scope.resume.language
