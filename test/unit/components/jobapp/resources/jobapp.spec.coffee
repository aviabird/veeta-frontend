describe "veetaTalentApp.jobapp", ->
  describe "resources", ->
    describe 'Jobapp', ->
      beforeEach module(
        "veetaTalentApp.jobapp.resources"
        'mocked/jobapps/detail.json'
        'mocked/jobapps/list.json'
      )

      Jobapp = undefined
      $httpBackend = undefined

      mockedJobappDetail = undefined
      mockedJobappList = undefined


      beforeEach inject (_$httpBackend_
                         _Jobapp_
                         _mockedJobappsDetail_
                         _mockedJobappsList_) ->
        $httpBackend = _$httpBackend_
        Jobapp = _Jobapp_

        mockedJobappDetail = _mockedJobappsDetail_
        mockedJobappList = _mockedJobappsList_


      describe 'class methods', ->



      describe 'instance methods', ->