JobappAssistantSelectResumePage = require('./select_resume.page')

class JobappAssistantSelectResumePage
  constructor: ->
    @url = '?job_id=1#/select_resume'
  get: ->
    browser.get(this.url)
    return @

module.exports = JobappAssistantSelectResumePage