"use strict"

###*
  # @ngdoc directive
  # @name resume.directives:resume-skills
  #
  # @requires ResumeLinks
  # @requires RESUME
  #
  # @description
  # languages block of resume
  # be used on resume detail page
###

angular.module("veetaTalentApp.resume.directives")
.directive "resumeSkills", (RESUME, ResumeSkill) ->
  restrict: "AE"
  templateUrl: RESUME.PARTIALS.SKILLS
  replace: true
  scope: {}
  controller: ($scope, OptionsService) ->

    # ----------------------------------------
    # Initialize attributes
    # ----------------------------------------

    # init a empty skills array
    $scope.skills = []

    $scope.isOnUpdate = false

    $scope.skillLevelOptions = OptionsService.skillLevelOptions


    # ----------------------------------------
    # Private methods
    # ----------------------------------------

    _loadSkills = ->
      $scope.$parent.resume.getSkills()
      .then (data) ->
        $scope.skills = data

    _initSkills = ->
      $scope.skills.push new ResumeSkill(skill) for skill in $scope.$parent.resume.skills


    _newSkill = ->
      $scope.$parent.resume.newSkill()



    # ----------------------------------------
    # Public methods
    # ----------------------------------------


    $scope.showControl = (skill) ->
      skill.isShowControl = true


    $scope.hideControl = (skill) ->
      skill.isShowControl = false

    $scope.add = ->
      $scope.$parent.resume.addSkill($scope.skillOnEdit)
      .then ->
        _loadSkills()
        $scope.skillOnEdit = _newSkill()

    $scope.remove = (skill) ->
      $scope.$parent.resume.removeSkill(skill)
      .then ->
        _loadSkills()

    $scope.update = () ->
      $scope.skillOnEdit.save().then ->
        _loadSkills()
        $scope.isOnUpdate = false
        $scope.skillOnEdit = _newSkill()

    $scope.select = (skill) ->
      $scope.skillOnEdit = angular.copy(skill)
      $scope.isOnUpdate = true

    $scope.cancel = ->
      $scope.skillOnEdit = _newSkill()
      $scope.isOnUpdate = false


    # ----------------------------------------
    # Events
    # ----------------------------------------

    # set a listener for handle the imported data
    $scope.$on 'resume:handle-imported-data', ->
      _loadSkills()


    # ----------------------------------------
    # Initialize methods
    # ----------------------------------------

    # init skills from resume
    _initSkills()

    $scope.skillOnEdit = _newSkill()
