module.exports = (grunt, appConfig) ->
  tasks:
    # Empties folders to start fresh
    clean:
      dist:
        files: [
          dot: true
          src: [
            "#{appConfig.paths.tmp}"
            "#{appConfig.paths.dist}/{,*/}*"
            "!#{appConfig.paths.dist}/.git*"
          ]
        ]

      server: ".tmp"

      docs:
        files: [
          dot: true
          src: [
            "#{appConfig.paths.tmp}"
            "#{appConfig.paths.docs}/{,*/}*"
          ]
        ]