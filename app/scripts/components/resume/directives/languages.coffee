"use strict"

###*
  # @ngdoc directive
  # @name resume.directives:resume-languages
  #
  # @requires $filter
  # @requires RESUME
  # @requires Language
  # @requires ResumeLanguage
  #
  # @description
  # languages block of resume
  # be used on resume detail page
###

angular.module 'veetaTalentApp.resume.directives'
.directive 'resumeLanguages', ($filter, RESUME, Language, ResumeLanguage)->
  templateUrl = (element, attrs)->
    if attrs.mobile == 'true' then RESUME.MOBILE.PARTIALS.LANGUAGES else RESUME.PARTIALS.LANGUAGES

  return(
    restrict: "AE"
    templateUrl: templateUrl
    replace: true
    scope: {}
    controller: ($scope, OptionsService) ->

      # ----------------------------------------
      # Initialize attributes
      # ----------------------------------------

      # init a empty languages array
      $scope.languages = []

      $scope.editMode = false
      $scope.isOnUpdate = false

      $scope.languageOptions = angular.copy(OptionsService.languageOptions)
      $scope.languageLevelOptions = angular.copy(OptionsService.languageLevelOptions)


      # ----------------------------------------
      # Private methods
      # ----------------------------------------

      _filterLanguageOptions = ->
        if $scope.languages? and $scope.languages.length > 0
          $scope.languageOptions = angular.copy(OptionsService.languageOptions)
          for language in $scope.languages
            if $scope.languageOptions.uiSelect.collection? and $scope.languageOptions.uiSelect.collection.length > 0
              for langItem in $scope.languageOptions.uiSelect.collection
                if langItem? and language.language.isoCode is langItem.isoCode
                  langItemIdx = $scope.languageOptions.uiSelect.collection.indexOf(langItem)
                  $scope.languageOptions.uiSelect.collection.splice(langItemIdx, 1)

      _loadLanguages = ->
        $scope.$parent.resume.getLanguages()
        .then (data) ->
          $scope.languages = data
          _filterLanguageOptions()

      _initLanguages = ->
        $scope.languages.push new ResumeLanguage(language: languageItem) for languageItem in $scope.$parent.resume.languages
        _filterLanguageOptions()


      _newLanguage = ->
        $scope.$parent.resume.newLanguage()


      # ----------------------------------------
      # Public methods
      # ----------------------------------------

      $scope.showControl = (language) ->
        language.isShowControl = true


      $scope.hideControl = (language) ->
        language.isShowControl = false

      $scope.add = ->
        $scope.$parent.resume.addLanguage($scope.languageOnEdit)
        .then ->
          _loadLanguages()
          $scope.languageOnEdit = _newLanguage()
          $scope.editMode = false

      $scope.update = () ->
        $scope.languageOnEdit.save()
        .then ->
          _loadLanguages()
          $scope.languageOnEdit = _newLanguage()
          $scope.isOnUpdate = false
          $scope.editMode = false

      $scope.remove = (language) ->
        $scope.$parent.resume.removeLanguage(language)
        .then ->
          _loadLanguages()
          $scope.editMode = false

      $scope.select = (language) ->
        $scope.languageOnEdit = angular.copy(language)
        $scope.isOnUpdate = true
        $scope.editMode = true

      $scope.cancel = ->
        $scope.languageOnEdit = _newLanguage()
        $scope.isOnUpdate = false
        $scope.editMode = false

      $scope.addNew = ->
        $scope.languageOnEdit = _newLanguage()
        $scope.editMode = true
        $scope.isOnUpdate = false

      $scope.isLevelPointActive = (level, index) ->
        level >= index


      # ----------------------------------------
      # Events
      # ----------------------------------------

      # set a listener for handle the imported data
      $scope.$on 'resume:handle-imported-data', ->
        _loadLanguages()

      $scope.$on 'languageOptions:loaded', ->
        $scope.languageOptions = angular.copy(OptionsService.languageOptions)
        $scope.languageLevelOptions = angular.copy(OptionsService.languageLevelOptions)
        _filterLanguageOptions()

      # ----------------------------------------
      # Initialize methods
      # ----------------------------------------

      # init languages from resume
      _initLanguages()

      $scope.languageOnEdit = _newLanguage()

  )