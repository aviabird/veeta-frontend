describe "veetaTalentApp.resume", ->
  describe "controllers", ->
    describe 'ResumeOverviewCtrl', ->
      beforeEach module(
        "veetaTalentApp.resume"
        "veetaTalentApp.account.resources"
        'mocked/resumes/list.json'
        'mocked/resumes/full-list.json'
        'mocked/resumes/empty-list.json'
        'mocked/talent/talent-setting.json'
      )

      $scope = undefined
      $rootScope = undefined
      $controller = undefined
      $httpBackend = undefined
      mockedResumesList = undefined
      mockedResumesEmptyList = undefined
      mockedResumesFullList = undefined
      Resume = undefined

      beforeEach inject (_$httpBackend_
                         _$rootScope_
                         _$controller_
                         _$modal_
                         _Resume_
                         _RESUME_
                         _TalentSetting_
                         _mockedResumesList_
                         _mockedResumesEmptyList_
                         _mockedResumesFullList_
                         _mockedTalentTalentSetting_) ->
        mockedResumesList = _mockedResumesList_
        mockedResumesEmptyList = _mockedResumesEmptyList_
        mockedResumesFullList = _mockedResumesFullList_

        Resume = _Resume_

        $httpBackend = _$httpBackend_

        $scope = _$rootScope_.$new()
        $rootScope = _$rootScope_.$new()

        # mock talent settings
        $rootScope.talentSetting = new _TalentSetting_(_mockedTalentTalentSetting_)

        $controller = _$controller_ 'ResumeOverviewCtrl',
          $scope: $scope
          $rootScope: $rootScope
          $modal: _$modal_
          Resume: Resume
          RESUME: _RESUME_


      describe 'initialize', ->
        it 'should get a list of resumes after into the controller', ->
          $httpBackend.whenGET(Resume.$url()).respond(mockedResumesList)
          $httpBackend.flush()
          expect($scope.resumes.length).toBeGreaterThan(1)

      describe 'canAddNewResume()', ->
        it 'should not be able to add a new resume with more than 3 resumes', ->
          $httpBackend.whenGET(Resume.$url()).respond(mockedResumesFullList)
          $httpBackend.flush()
          expect($scope.resumes.length).toBe(3)
          expect($scope.canAddNewResume()).toBeFalsy

        it 'should be able to add a new resume with less than 3 resumes', ->
          $httpBackend.whenGET(Resume.$url()).respond(mockedResumesEmptyList)
          $httpBackend.flush()
          expect($scope.resumes.length).toBe(0)
          expect($scope.canAddNewResume()).toBeTruthy

      describe 'isShowSharings(resume)', ->
        it 'should check if sharings be showed', ->
          resume =
            sharedWith: {}
          expect($scope.isShowSharings(resume)).toBeFalsy
          resume =
            sharedWith: [1, 2, 3]
          expect($scope.isShowSharings(resume)).toBeTruthy

      describe '$watch(resumeUpdates)', ->
        it 'should watch value change for resumeUpdates', ->



