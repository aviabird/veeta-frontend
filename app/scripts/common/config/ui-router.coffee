angular.module "veetaTalentApp.common"
.run ($rootScope, $log) ->
  $rootScope.$on "$stateChangeStart", (event, toState, toParams, fromState, fromParams) ->
    $log.debug '$stateChangeStart', event, toState, toParams, fromState, fromParams
#    $rootScope.flashMessage = {}

  $rootScope.$on "stateNotFound", (event, unfoundState, fromState, fromParams) ->
    $log.debug 'stateNotFound', event, unfoundState, fromState, fromParams

  $rootScope.$on "stateChangeSuccess", (event, toState, toParams, fromState, fromParams) ->
    $log.debug 'stateChangeSuccess', event, toState, toParams, fromState, fromParams

  $rootScope.$on "stateChangeError", (event, toState, toParams, fromState, fromParams, error) ->
    $log.debug 'stateChangeError', event, toState, toParams, fromState, fromParams, error
