describe "veetaTalentApp.jobapp", ->
  describe "controllers", ->
    describe 'JobappDetailCtrl', ->
      beforeEach module(
        "veetaTalentApp.jobapp"
        'mocked/jobapps/detail.json'
      )

      $scope = undefined
      $controller = undefined
      fakeModalInstance = undefined
      $httpBackend = undefined
      jobapp = undefined
      Jobapp = undefined

      beforeEach inject (_$httpBackend_
                         _$rootScope_
                         _$modal_
                         _$controller_
                         _Jobapp_
                         _JOBAPP_
                         _mockedJobappsDetail_) ->
        $httpBackend = _$httpBackend_
        $scope = _$rootScope_.$new()

        # mock a Jobapp
        Jobapp = _Jobapp_
        jobapp = new Jobapp(_mockedJobappsDetail_)

        # create controller object
        $controller = _$controller_ 'JobappDetailCtrl',
          $scope: $scope
          $modal: _$modal_
          jobapp: jobapp
          JOBAPP: _JOBAPP_

        # Create a mock modalInstance using spies
        fakeModalInstance =
          close: jasmine.createSpy("modalInstance.close")
          dismiss: jasmine.createSpy("modalInstance.dismiss")
          result:
            then: jasmine.createSpy("modalInstance.result.then")

        spyOn(_$modal_, 'open').andReturn(fakeModalInstance);

      # create controller object
      #        $withdrawConfirmationController = _$controller_ 'JobappWithdrawConfirmationCtrl',
      #          $scope: $scope
      #          $modalInstance: fakeModalInstance
      #          jobapp: jobapp

      describe 'initialize', ->
        it 'should get a jobapp from state router', ->
          expect($scope.jobapp.id).toBe(1)

      describe 'isWithdrawn()', ->
        it 'should check if jobapp has been withdrawn', ->
          $scope.jobapp.withdrawnAt = null
          expect($scope.isWithdrawn()).toBeFalsy
          $scope.jobapp.withdrawnAt = undefined
          expect($scope.isWithdrawn()).toBeFalsy
          $scope.jobapp.withdrawnAt = '2014-01-01'
          expect($scope.isWithdrawn()).toBeTruthy

      describe 'isWithSharingCompanies()', ->
        it 'should check if jobapp has sharing companies', ->
          $scope.jobapp.resume = undefined
          expect($scope.isWithSharingCompanies()).toBeFalsy
          $scope.jobapp.resume = {}
          expect($scope.isWithSharingCompanies()).toBeFalsy
          $scope.jobapp.resume.companies = []
          expect($scope.isWithSharingCompanies()).toBeFalsy
          $scope.jobapp.resume.companies.push 'company'
          expect($scope.isWithSharingCompanies()).toBeTruthy

      describe 'openWithdrawConfirmModal()', ->
        it 'should open a modal window to confirm withdraw', ->
          # @todo find way to testing open action of modal
#          $scope.openWithdrawConfirmModal()
#          expect(fakeModalInstance.command).toBe("show");


