"use strict"

###*
  # @ngdoc directive
  # @name common.directives:month-select
  #
  # @requires COMMON
  # @requires $compile
  #
  # @restrict AE
  #
  # @description
  # a extend input for year and month select
  # be used on resume edit page (work exp and education's form)
###

angular.module 'veetaTalentApp.common.directives'
.directive "monthSelect", ($compile, COMMON, $translate)->
  _checkDate = (date) ->
    return false  if not date.month or not date.year
    new Date(Date.UTC(date.year, date.month - 1, 1))

  return(
    restrict: "AE"
    scope:
      ngModel: '='
      ngDisabled: '='
      # Move from object (=) to string (@) 2-way binding to one-way (-> reduce watchers)
      allowNow: '='
    templateUrl: COMMON.PARTIALS.MONTH_SELECT
    controller: ($scope) ->
      maxYear = moment().year()
      minYear = 1940

      $scope.monthOptions =
        uiSelect:
          searchEnabled: false
          formatResult: (item) ->
            item.name if item?
          formatSelection: (item) ->
            item.name if item?
          bindProperty: 'value'
          collection: []

      $scope.yearOptions =
        uiSelect:
          bindProperty: 'value'
          formatResult: (item) ->
            item.name if item?
          formatSelection: (item) ->
            item.name if item?
          collection: []

      if $scope.allowNow is true
        $scope.monthOptions.uiSelect.collection.push {
          value: 0
          name: $translate.instant('talent.data.date.now')
        }

      $scope.monthOptions.uiSelect.collection.push {
        value: num
        name: moment().month(num - 1).format("MM")
      } for num in [1..12]

      $scope.yearOptions.uiSelect.collection.push {
        value: num
        name: moment().year(num).format("YYYY")
      } for num in [maxYear .. minYear]

      $scope.dateFields = {}
      $scope.dateFields.month = new Date($scope.ngModel).getUTCMonth() + 1 if $scope.ngModel
      $scope.dateFields.year = new Date($scope.ngModel).getUTCFullYear() if $scope.ngModel

      $scope.checkDate = ->
        if $scope.dateFields.month is 0
          $scope.yearDisabled = true
          $scope.dateFields.year = null
          $scope.ngModel = null
        else
          $scope.yearDisabled = false
          date = _checkDate($scope.dateFields)
          $scope.ngModel = date if date

      $scope.$watch 'dateFields', ->
        $scope.checkDate()
      , true
  )