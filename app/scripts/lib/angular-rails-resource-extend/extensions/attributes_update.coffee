angular.module("rails.extend")
.factory "RailsResourceAttributesMixin", ->
  RailsResourceAttributesMixin = ->
    return

  ###*
  update the attributes only with given keys
  ###
  updateOnly = ->
    return if arguments.length < 1
    @constructor.$put(@$url(), extractedValue(this, arguments))

  ###*
  extracted the values into a new hash
  ###
  extractedValue = (obj, argArr)->
    hash = {}
    hash[arg] = obj[arg] for arg in argArr
    hash

  RailsResourceAttributesMixin.extended = (Resource) ->
    Resource.include
      updateOnly: updateOnly
      extractedValue: extractedValue

  RailsResourceAttributesMixin
