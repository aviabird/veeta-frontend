describe "veetaTalentApp.jobapp", ->
  describe "controllers", ->
    describe 'new', ->
      describe 'JobappNewAddCoverLetterCtrl', ->
        beforeEach module(
          "veetaTalentApp.common"
          "veetaTalentApp.jobapp"
          'mocked/jobapps/detail.json'
        )

        $scope = undefined
        $controller = undefined
        $httpBackend = undefined
        jobapp = undefined
        Jobapp = undefined
        OptionsService = undefined

        beforeEach inject (_$httpBackend_
                           _$rootScope_
                           _$controller_
                           _Resume_
                           _$state_
                           _$anchorScroll_
                           _$location_
                           _OptionsService_
                           _$translate_
                           _Jobapp_
                           _JOBAPP_
                           _mockedJobappsDetail_) ->
          $httpBackend = _$httpBackend_
          $scope = _$rootScope_.$new()

          # mock a resume
          Jobapp = _Jobapp_
          jobapp = new Jobapp(_mockedJobappsDetail_)
          OptionsService = _OptionsService_

          # create controller object
          $controller = _$controller_ 'JobappNewAddCoverLetterCtrl',
            $scope: $scope
            $anchorScroll: _$anchorScroll_
            $location: _$location_
            OptionsService: _OptionsService_
            Resume: _Resume_
            $state: _$state_

        describe 'initialize', ->
          it 'should get a jobapp', ->

        describe 'goPreviousStep()', ->
          it 'should previous step', ->

        describe 'goNextStep()', ->
          it 'should go to next step', ->
