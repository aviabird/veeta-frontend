angular.module "veetaTalentApp.resume.resources"
.factory "ResumeLanguage", (railsResourceFactory) ->
  railsResourceFactory
    url: CONFIG.apiTalentUrl + '/resume/languages'
    name: 'language'
    serializer: 'ResumeLanguageSerializer'
    interceptors: ['belongsToResume']

.factory "ResumeLanguageSerializer", (railsSerializer) ->
  railsSerializer ->
    @exclude "resumeId", "language"
    @add "languageId", (language) ->
      language.language.id  if language.language

