"use strict"

###*
  # @ngdoc directive
  # @name jobapp.directives:jobapp-job-intro
  #
  # @requires JOBAPP
  #
  # @description
  # a job intro component, show information summary about a job
  # be used on jobapp assistant page
###

angular.module 'veetaTalentApp.jobapp.directives'
.directive "jobappJobIntro", (JOBAPP) ->
  restrict: "AE"
  templateUrl: JOBAPP.PARTIALS.JOB_INTRO
  replace: true
