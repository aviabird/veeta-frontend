describe "veetaTalentApp.jobapp", ->
  describe "controllers", ->
    describe 'JobappNewCtrl', ->
      beforeEach module(
        'veetaTalentApp.desktop'
        'mocked/jobapps/new-with-job.json'
        'mocked/jobapps/new-without-job.json'
      )

      $scope = null
      $rootScope = null
      $state = null
      $location = null
      $controller = null
      $httpBackend = null
      jobapp = null
      Jobapp = null
      job = null


      beforeEach inject (_$rootScope_
                         _$httpBackend_
                         _$state_
                         _$location_
                         _Jobapp_) ->
        $httpBackend = _$httpBackend_
        $scope = _$rootScope_.$new()
        $rootScope = _$rootScope_.$new()
        $state = _$state_
        $location = _$location_

        # mock a Jobapp
        Jobapp = _Jobapp_

        spyOn(_$state_, 'go')
        spyOn($rootScope, '$broadcast');


      describe 'EmailApplication', ->
        beforeEach inject (_$controller_
                           _mockedJobappsNewWithoutJob_) ->

          # mock a jobapp
          jobapp = new Jobapp(_mockedJobappsNewWithoutJob_)

          # create controller object
          $controller = _$controller_ 'JobappNewCtrl',
            $scope: $scope
            $rootScope: $rootScope
            $state: $state
            jobapp: jobapp

        describe 'initializeize', ->
          it 'should has a jobapp', ->
            expect($scope.jobapp).toEqual jobapp

          it 'should has a emtpy job', ->
            expect($scope.job).toBe null

          it 'should has a empty resume object', ->
            expect($scope.resume).toEqual {}

          it 'should has a empty resumes array', ->
            expect($scope.resumes).toEqual []

          it 'should be by step 0', ->
            expect($scope.step).toBe(0)

          it 'should be without job_id', ->
            expect($scope.jobapp.jobId).toBe(null)

          it 'should throw $window.redirectTo broadcast if job unavailable', inject (_$controller_) ->
#            $scope.job['available?'] = false
#            $controller = _$controller_ 'JobappNewCtrl',
#              $scope: $scope
#              $rootScope: $rootScope
#              $state: $state
#              jobapp: jobapp
#              job: job
#
#            expect($rootScope.$broadcast).toHaveBeenCalledWith('$window.redirectTo');

          it 'should not throw $window.redirectTo broadcast if job available', ->
#            $scope.job['available?'] = true
#            expect($rootScope.$broadcast).not.toHaveBeenCalledWith('$window.redirectTo');

          it 'should redirect to selectResume if user try to visit a invalid url', ->
#              expect($state.go).toHaveBeenCalledWith('asdfasdf', 'asdfasdfasd');

        describe 'jobType', ->
          it 'should be RefApplication ', ->
            expect($scope.jobapp.jobType).toBe 'EmailApplication'

        describe 'isShowNavBack()', ->
          it 'should be false', ->
            expect($scope.isShowNavBack()).toBe true

        xdescribe 'isShowJobInfoRef()', ->
          it 'should be false', ->
            expect($scope.isShowJobInfoRef()).toBe false

        xdescribe 'isShowJobInfoEmail()', ->
          it 'should be false by initialize', ->
            expect($scope.isShowJobInfoEmail()).toBe false


      describe 'RefApplication', ->
        beforeEach inject (_$controller_
                           _mockedJobappsNewWithJob_) ->
          # mock a jobapp
          jobapp = new Jobapp(_mockedJobappsNewWithJob_)

          # create controller object
          $controller = _$controller_ 'JobappNewCtrl',
            $scope: $scope
            $rootScope: $rootScope
            $state: $state
            jobapp: jobapp

        describe 'initialize', ->
          it 'should has a jobapp', ->
            expect($scope.jobapp).toEqual jobapp

          it 'should has a job', ->
            expect($scope.job).not.toBe null
            expect($scope.job).toEqual jobapp.job

          it 'should has a empty resume object', ->
            expect($scope.resume).toEqual {}

          it 'should be by step 0', ->
            expect($scope.step).toBe(0)

          it 'should be with job_id', ->
            expect($scope.jobapp.jobId).not.toBe(null)

        describe 'redirectTo', ->
          xit 'should can visit goal state with a valid url ', ->
            expect($state.href('app.jobappAssistant.resume.select',
              {jobId: 1})).toEqual('#/jobapp_assistant/1/resume/select')

          xit 'should redirect to first step with a invalid url', ->
            $location.url('jobapps/overview')
            expect($state.current).toBe ''
            expect($state.href('app.jobappAssistant.resume.wrong')).toEqual('')

        describe 'jobType', ->
          it 'should be RefApplication ', ->
            expect($scope.jobapp.jobType).toBe 'Job'

        describe 'isShowNavBack()', ->
          it 'should be false', ->
            expect($scope.isShowNavBack()).toBe false

        xdescribe 'isShowJobInfoEmail()', ->
          it 'should be true', ->
            expect($scope.isShowJobInfoRef()).toBe true

        xdescribe 'isShowJobInfoEmail()', ->
          it 'should be false', ->
            expect($scope.isShowJobInfoEmail()).toBe false




