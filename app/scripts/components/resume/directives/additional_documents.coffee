"use strict"

###*
  # @ngdoc directive
  # @name resume.directives:resume-additional-documents
  #
  # @requires $auth
  # @requires RESUME
  # @requires FileUploader
  # @requires OptionsService
  #
  # @description
  # add documentations into a resume,
  # be used on resume edit page
  # TODO: Validate uploaded documenty type before server validation
###

angular.module 'veetaTalentApp.resume.directives'
.directive 'resumeAdditionalDocuments', ($auth
                                         RESUME
                                         FileUploader
                                         OptionsService) ->
  restrict: "AE"
  templateUrl: RESUME.PARTIALS.ADDITIONAL_DOCUMENTS
  replace: true
  scope: {}
  controller: ($scope) ->

    # get resume from parent
    $scope.resume = $scope.$parent.resume
    $scope.documentTypeOptions = OptionsService.documentOptions
    $scope.newDocumentType = null

    loadDocuments = ->
      $scope.resume.getDocuments().then (data) ->
        $scope.documents = data

    # config avatar uploader
    documentUploader = $scope.documentUploader = new FileUploader
      url: CONFIG.apiTalentUrl + "/resume/documents"
      autoUpload: true
      alias: "document[file]"
      headers: $auth.retrieveData('auth_headers')
      formData: [
        {
          resume_id: $scope.resume.id
          "document['document_type']": $scope.newDocumentType
        }
      ]

    # Successful callback of avatar uploader
    documentUploader.onSuccessItem = ->
      $scope.newDocumentType = null
      loadDocuments()

    loadDocuments()