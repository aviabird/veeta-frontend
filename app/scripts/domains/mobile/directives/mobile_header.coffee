"use strict"

###*
  # @ngdoc directive
  # @name mobile.directives:mobile-header
  #
  # @requires MOBILE
  #
  # @description
  # application header
  # be used on mobile application layout
###

angular.module 'veetaTalentApp.mobile.directives'
.directive "mobileHeader", (MOBILE)->
  restrict: "AE",
  templateUrl: MOBILE.PARTIALS.HEADER,
  replace: true