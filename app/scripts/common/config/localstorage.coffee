"use strict"
angular.module("veetaTalentApp.common")
.config (localStorageServiceProvider) ->
  localStorageServiceProvider
  .setPrefix('veetaTalentApp')
  .setStorageType('localStorage')
  .setNotify(true, true)