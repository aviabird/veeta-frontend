describe "veetaTalentApp.common", ->
  describe "filter", ->
    beforeEach module('veetaTalentApp.common.filters')

    $filter = undefined

    beforeEach inject (_$filter_) ->
      $filter = _$filter_

    describe "utc", ->
      it 'has a utc filter', ->
        expect($filter('datetimeDefault')).not.toBeNull()

      it 'should convert a date to UTC date object', inject (_utcFilter_) ->
        # @todo find way to convert it with same utc timezone
#        dateString = '2014-08-05T12:29:28'
#        expect(_utcFilter_(dateString).toUTCString()).toEqual(new Date(dateString).toUTCString())


    describe "datetimeDefault", ->
      it 'has a datetimeDefault filter', ->
        expect($filter('datetimeDefault')).not.toBeNull()

      it 'should return a formatted datetime correctly', inject (_datetimeDefaultFilter_) ->
        expect(_datetimeDefaultFilter_('2014-08-05T12:29:28.109Z')).toBe('05.08.2014 12:29')
