describe "veetaTalentApp.jobapp", ->
  describe "controllers", ->
    describe 'new', ->
      describe 'JobappNewSelectResumeCtrl', ->
        beforeEach module(
          "veetaTalentApp.jobapp"
          'mocked/jobapps/new-with-job.json'
          'mocked/jobapps/new-without-job.json'
        )

        $scope = undefined
        $state = undefined
        $controller = undefined
        $httpBackend = undefined
        jobapp = undefined
        Jobapp = undefined
        Resume = undefined


        beforeEach inject (_$httpBackend_
                           _$rootScope_
                           _Resume_
                           _$state_
                           _Jobapp_
                           _JOBAPP_) ->
          $httpBackend = _$httpBackend_
          $scope = _$rootScope_.$new()
          $state = _$state_

          # mock Jobapp
          Jobapp = _Jobapp_

          # mock Resume
          Resume = _Resume_

        describe 'EmailApplication', ->
          beforeEach inject (_$controller_
                             _mockedJobappsNewWithoutJob_) ->
            # create controller object
            $scope.jobType = ->
              'EmailApplication'

            # mock a jobapp
            jobapp = new Jobapp(_mockedJobappsNewWithoutJob_)

            $controller = _$controller_ 'JobappNewResumeSelectCtrl',
              $scope: $scope
              Resume: Resume
              $state: $state

            $scope.jobapp = jobapp

          describe 'initialize', ->
            xit 'should by step 1', ->
              expect($scope.$parent.step).toBe(1)

            it 'should get a jobapp', ->
              #            expect($scope.jobapp.id).toBe(1)

            it 'should get a list of resumes', ->


          describe 'canAddNewResume()', ->
            it 'should check if user can add a new resume', ->
              $scope.resumes = undefined
              expect($scope.canAddNewResume()).toBeTruthy
              $scope.resumes = []
              expect($scope.canAddNewResume()).toBeTruthy
              $scope.resumes = [1, 2]
              expect($scope.canAddNewResume()).toBeTruthy
              $scope.resumes = [1, 2, 3]
              expect($scope.canAddNewResume()).toBeFalsy

          describe 'isShowJobDetailForm', ->
            it 'should be true', ->
              expect($scope.isShowJobDetailForm()).toBeTruthy()

          describe 'goNextStep()', ->
            it 'should go to next step', ->

        describe 'RefApplication', ->
          beforeEach inject (_$controller_
                             _mockedJobappsNewWithJob_) ->
            # create controller object
            $scope.jobType = ->
              'RefApplication'

            # mock a jobapp
            jobapp = new Jobapp(_mockedJobappsNewWithJob_)

            $scope.$parent.jobapp = jobapp

            $controller = _$controller_ 'JobappNewResumeSelectCtrl',
              $scope: $scope
              Resume: Resume
              $state: $state

          describe 'initialize', ->
            xit 'should by step 1', ->
              expect($scope.$parent.step).toBe(1)

            it 'should get a jobapp', ->
              #            expect($scope.jobapp.id).toBe(1)

            it 'should get a list of resumes', ->


          describe 'canAddNewResume()', ->
            it 'should check if user can add a new resume', ->
              $scope.resumes = undefined
              expect($scope.canAddNewResume()).toBeTruthy
              $scope.resumes = []
              expect($scope.canAddNewResume()).toBeTruthy
              $scope.resumes = [1, 2]
              expect($scope.canAddNewResume()).toBeTruthy
              $scope.resumes = [1, 2, 3]
              expect($scope.canAddNewResume()).toBeFalsy

          describe 'isShowJobDetailForm', ->
            it 'should be false', ->
              expect($scope.isShowJobDetailForm()).toBeFalsy()

          describe 'goNextStep()', ->
            it 'should go to next step', ->

          xdescribe 'canGoNextStep', ->
            it 'should be false by initialize', ->
              expect($scope.canGoNextStep()).toBeFalsy()

            it 'should be true after select a resume', ->
              $scope.resumes = [1, 2, 3]
              $scope.jobapp.resume = 1
              expect($scope.canGoNextStep()).toBeTruthy()

            it 'should be true after if select to create a new resume', ->
              $scope.jobapp.resume = $scope.newResume
              expect($scope.canGoNextStep()).toBeTruthy()