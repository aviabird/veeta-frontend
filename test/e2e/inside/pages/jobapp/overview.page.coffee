class JobappOverviewPage
  constructor: ->
    @jobappsList = element.all(By.repeater 'jobapp in jobapps')

  get: ->
    browser.get '#/jobapps/overview'
    browser.getCurrentUrl()

module.exports = JobappOverviewPage