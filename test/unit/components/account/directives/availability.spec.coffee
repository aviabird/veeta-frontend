describe "veetaTalentApp", ->
  describe 'account', ->
    describe 'directives', ->
      describe 'accountAvailability', ->
        beforeEach module(
          'veetaTalentApp.account'
          'veetaTalentApp.common'
          "views/components/account/partials/availability.html"
        )

        $document = null
        $scope = null
        $rootScope = null
        $modal = null
        elementScope = null
        $httpBackend = null
        TalentAuth = null
        TalentSetting = null

        beforeEach inject (_$rootScope_
                           _$document_
                           _$modal_
                           _$httpBackend_
                           _TalentAuth_
                           _TalentSetting_) ->
          $rootScope = _$rootScope_
          $scope = _$rootScope_.$new()
          $document = _$document_
          $modal = _$modal_
          $httpBackend = _$httpBackend_
          TalentAuth = _TalentAuth_
          TalentSetting = _TalentSetting_

          $httpBackend.whenGET(/(api\/locales)/).respond()
          $httpBackend.whenPUT(/(api\/talent\/talent_setting)/).respond()


        beforeEach inject (_$compile_) ->
          element = angular.element '<div account-availability=""></div>'
          $rootScope.talentSetting = new TalentSetting
            workingAreas: ['area1', 'area2']
            availableIn: 'three_months'
            interested: 'very'
            workingIndustries: ['Industry1', 'Industry2']
            salaryExpMin: "2000"
          _$compile_(element)($scope)
          $scope.$apply()
          elementScope = element.isolateScope()


        describe 'initialize', ->
          it 'should initialize a new Talent Setting', ->
            expect(elementScope.talentSetting).toEqual $rootScope.talentSetting

          it 'should initialize empty newWorkingArea & newWorkingIndustry model', ->
            expect(elementScope.newWorkingArea).toEqual ''
            expect(elementScope.newWorkingIndustry).toEqual ''

          it 'should initialize availableOptions', ->
            expect(elementScope.availableInOptions).not.toBe null

          it 'should initialize interestedOptions', ->
            expect(elementScope.interestedOptions).not.toBe null

          it 'should initialize priceRangeOptions', ->
            expect(elementScope.priceRangeOptions).not.toBe null

        describe 'save()', ->
          it 'should save setting to server', ->
            elementScope.save()
            $httpBackend.flush()

        describe 'reset()', ->
          it 'should save setting to server', ->
            elementScope.talentSetting.workingAreas = []
            elementScope.reset()
            expect(elementScope.talentSetting).toEqual $rootScope.talentSetting

        describe 'addWorkingArea(workingArea)', ->
          it 'should add a new WorkingArea', ->
            workingArea = 'Area3'
            elementScope.addWorkingArea(workingArea)
            expect(elementScope.talentSetting.workingAreas).toContain 'Area3'

        describe 'removeWorkingArea(workingArea)', ->
          it 'should remove a WorkingArea', ->
            elementScope.talentSetting.workingAreas = ['Area1', 'Area2', 'Area3']
            elementScope.removeWorkingArea('Area3')
            expect(elementScope.talentSetting.workingAreas).not.toContain 'Area3'

        describe 'addWorkingIndustry(workingIndustry)', ->
          it 'should add a new WorkingArea', ->
            workingIndustry = 'Industry3'
            elementScope.addWorkingIndustry(workingIndustry)
            expect(elementScope.talentSetting.workingIndustries).toContain 'Industry3'

        describe 'removeWorkingIndustry(workingIndustry)', ->
          it 'should remove a workingIndustry', ->
            elementScope.talentSetting.workingIndustries = ['Industry1', 'Industry2', 'Industry3']
            elementScope.removeWorkingIndustry('Industry3')
            expect(elementScope.talentSetting.workingIndustries).not.toContain 'Industry3'