module.exports = (grunt, appConfig) -> tasks:
#  watch:
#    compass:
#      files: ["#{appConfig.paths.app}/styles/**/*.{scss,sass}"]
#      tasks: [
#        "compass:server"
#        "autoprefixer"
#      ]
#      options:
#        spawn: false

  # Compiles Sass to CSS and generates necessary files if requested
  compass:
    options:
      sassDir: "#{appConfig.paths.app}/styles"
      cssDir: "#{appConfig.paths.tmp}/styles"
      generatedImagesDir: "#{appConfig.paths.tmp}/images/generated"
      imagesDir: "#{appConfig.paths.app}/images"
      javascriptsDir: "#{appConfig.paths.app}/scripts"
      fontsDir: "#{appConfig.paths.app}/fonts"
      importPath: "./bower_components"
      httpImagesPath: "/images"
      httpGeneratedImagesPath: "/images/generated"
      httpFontsPath: "/fonts"
      relativeAssets: false
      assetCacheBuster: false
      raw: "Sass::Script::Number.precision = 10\n"

    dist:
      options:
        httpFontsPath: "../fonts"
        generatedImagesDir: "#{appConfig.paths.dist}/images/generated"

    server:
      options:
        debugInfo: true

    dev:
      options:
        debugInfo: true
        watch: true
