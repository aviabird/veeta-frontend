"use strict"

###*
  # @ngdoc directive
  # @name resume.directives:resume-add-module
  #
  # @requires RESUME
  #
  # @description
  # add additional modules into a resume,
  # right now it is just a dummy block
  # be used on resume edit page
###

angular.module 'veetaTalentApp.resume.directives'
.directive 'resumeAddModule', (RESUME) ->
  restrict: "AE"
  templateUrl: RESUME.PARTIALS.ADD_MODULE
  replace: true
  scope: {}
  controller: ($scope) ->
    # get resume from parent
    $scope.resume = $scope.$parent.resume

    $scope.modules = ['Programing matrix'
                      'Career highlights'
                      'Mantra'
                      'Short description'
                      'Publications'
                      'Projects'
                      'international experience']
