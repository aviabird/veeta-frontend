###*
  # @ngdoc controller
  # @name jobapp.controllers:JobappWithdrawnConfirmCtrl
  #
  # @requires $scope
  # @requires $modalInstance
  # @requires jobapp
  #
  # @description
  # Controller for Jobapp Drawn Confirmation
###

angular.module 'veetaTalentApp.jobapp.controllers'
.controller 'JobappWithdrawConfirmationCtrl', ($scope
                                               $modalInstance
                                               jobapp) ->
  $scope.jobapp = jobapp

  $scope.withdrawn = ->
    $scope.jobapp.withdrawn().then ->
      $modalInstance.close()

  $scope.cancel = ->
    $modalInstance.dismiss('cancel')
