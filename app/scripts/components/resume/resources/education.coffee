angular.module "veetaTalentApp.resume.resources"
.factory "ResumeEducation", (railsResourceFactory) ->
  resource = railsResourceFactory
    url: CONFIG.apiTalentUrl + '/resume/educations'
    name: 'education'
    serializer: 'ResumeEducationSerializer'
    interceptors: ['belongsToResume']


  resource

.factory "ResumeEducationSerializer", (railsSerializer) ->
  railsSerializer ->
    @only(
      "typeOfEducation"
      "schoolName"
      "degree"
      "countryId"
      "subject"
      "from"
      "to"
      "description")
    @add "countryId", (education) ->
      education.country.id  if education.country