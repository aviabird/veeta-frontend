###*
  # @ngdoc controller
  # @name resume.controllers:ResumeOverviewCtrl
  #
  # @requires $scope
  # @requires $rootScope
  # @requires $modal
  # @requires Resume
  # @requires RESUME
  #
  # @description
  # Controller for the Resume Overview
###

angular.module 'veetaTalentApp.resume.controllers'
.controller 'ResumeOverviewCtrl', ($scope, $rootScope, $modal, Resume, RESUME)->

  $scope.resumeUpdates = $rootScope.talentSetting.resumeUpdates

  # Get a list of resumes from API
  Resume.query().then (data)->
    $scope.resumes = data

  # TODO: Check this out at some point
  $scope.sharedCompaniesLimit = (resume)->
    (resume.moreSharedCompanies) ? 99: 7

  ###*
    # @ngdoc function
    # @name veetaTalentApp.resume.controllers:ResumeOverviewCtrl:canAddNewResume
    #
    # @description
    # If the user could add a new resume
    # @return {boolean} true or false
  ###
  $scope.canAddNewResume = ->
    ($scope.resumes.length < 3) if $scope.resumes

  ###*
    # @ngdoc function
    # @name veetaTalentApp.resume.controllers:ResumeOverviewCtrl:isShowSharings
    #
    # @description
    # Show sharings only if a resume has more then one sharings
    #
    # @param {Object} resume a resume object
    # @return {boolean} true or false
  ###
  $scope.isShowSharings = (resume) ->
    (resume.sharedWith.length > 0) if resume.sharedWith
