###*
  # @ngdoc controller
  # @name jobapp.controllers:JobappNewCtrl
  #
  # @requires $scope
  # @requires $rootScope
  # @requires $state
  # @requires jobapp
  #
  # @description
  # Controller for add cover letter for jobapp creating
###

angular.module 'veetaTalentApp.jobapp.controllers'
.controller 'JobappNewCtrl', ($scope, $log, $rootScope, $state, jobapp, Jobapp)->
  $scope.jobapp = jobapp
  $scope.job = if jobapp.jobType is 'Job' then jobapp.job else null
  $scope.resume = {}
  $scope.resumes = []
  $scope.step = 0
  
  ###*
  # @ngdoc function
  # @name jobapp.controllers:JobappNewCtrl#isShowNavBack
  # @methodOf jobapp.controllers:JobappNewCtrl
  #
  # @description
  # check the jobapp's type, control if show back link
  #
  ###
  $scope.isShowNavBack = ->
    $scope.jobapp.jobType is 'EmailApplication'

  # TODO: Enhance page reload (zB: when user change the language, store values in db/localStorage)
  if $state.includes('**.jobappAssistant.*')
    $rootScope.hideUserNav = true

  unless jobapp.wizardStep?
    if $state.includes('**.resume.select')
    else if $state.includes('**.resume.detail') or $state.includes('**.resume.new')
      $state.go '^.select'
    else if $state.includes('**.resume') or $state.includes('**.addCoverLetter') or $state.includes('**.checkAndSend') or $state.includes('**.finished')
      $state.go '^.resume.select'
    else
      $state.go '.resume.select'