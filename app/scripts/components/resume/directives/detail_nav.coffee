"use strict"

###*
  # @ngdoc directive
  # @name resume.directives:resume-detail-nav
  #
  # @requires $rootScope
  # @requires $location
  # @requires $anchorScroll
  # @requires RESUME
  #
  # @description
  # a nav in resume detail page
  # be used on resume detail page (left)
###

angular.module("veetaTalentApp.resume.directives")
.directive "resumeDetailNav", ($rootScope
                               $location
                               $anchorScroll
                               RESUME) ->
  restrict: "AE"
  templateUrl: RESUME.PARTIALS.DETAIL_NAV
  replace: true
  scope: {}
  link: (scope, element) ->

    scope.modules = scope.$parent.modules

    element.find('ul#resume-nav').affix
      offset:
        top: 100
        bottom: ->
          @bottom = $(".footer").outerHeight(true)

    scope.scrollTo = (id) ->
      $location.hash(id)
      $anchorScroll()