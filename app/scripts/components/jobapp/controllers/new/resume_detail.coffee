###*
  # @ngdoc controller
  # @name jobapp.controllers:JobappNewResumeDetailCtrl
  #
  # @requires $scope
  # @requires $state
  #
  # @description
  # Controller for resume detail for jobapp creating
###

angular.module 'veetaTalentApp.jobapp.controllers'
.controller 'JobappNewResumeDetailCtrl', ($scope
                                          $rootScope
                                          $log
                                          $state
                                          resume) ->

  # ----------------------------------------
  # Initialize attributes
  # ----------------------------------------
  $rootScope.pageLoading = false
  $parentScope = $scope.$parent
  $parentScope.resume = resume
  $parentScope.jobapp.resumeId = resume.id
  $scope.resume = resume
  $scope.modules =
    language: true
    import: $state.includes('**.resume.new')
    profile: true
    generalInformation: not $rootScope.talent.salutation? # check if user fill out personal data once
    workExperience: true
    education: true
    languages: true
    skills: true
    links: true
    documents: true
    addModule: true
    password: $rootScope.talent.guest is true

  # ----------------------------------------
  # Public methods
  # ----------------------------------------

  $scope.isNewResume = ->
    $state.includes('**.resume.new')

  $scope.isShowSharedCompanies = ->
    $scope.resume.sharedWith && $scope.resume.sharedWith.length > 0

  $scope.isShowBackLink = ->
    $parentScope.resumes.length > 0

  $scope.goPreviousStep = ->
    $state.go('^.select')

  $scope.goNextStep = ->
    # todo check resume validation maybe in client?
    $scope.$broadcast 'resume:save'
    $state.go '^.^.addCoverLetter'
    # $scope.resume.talent = $rootScope.talent
    # $scope.resume.save().then ->
      # $state.go '^.^.addCoverLetter'
