angular.module "veetaTalentApp.resume.resources"
.factory "ResumeWorkExperience", (railsResourceFactory) ->
  resource = railsResourceFactory
    url: CONFIG.apiTalentUrl + '/resume/work_experiences'
    name: 'work_experience'
    serializer: 'ResumeWorkExperienceSerializer'
    interceptors: ['belongsToResume']

  resource

.factory "ResumeWorkExperienceSerializer", (railsSerializer) ->
  railsSerializer ->
    @only(
      'position'
      'company'
      'industry'
      'jobLevel'
      'from'
      'to'
      'current'
      'description'
      'countryId'
      'termsOfEmployment')
    @add "countryId", (workExperience) ->
      workExperience.country.id  if workExperience.country


