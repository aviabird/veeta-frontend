###*
  # @ngdoc controller
  # @name auth.controllers:SignInCtrl
  #
  # @requires $scope
  #
  # @description
  #
  # TODO: Create shared sign-in and signu-up controllers used directly in views
###

angular.module("veetaTalentApp.auth.controllers")
.controller "SignInCtrl", ($scope, $rootScope, $state, $auth, $http) ->

  $scope.talentAuth = {}

  $scope.signIn = ->
    $auth.submitLogin($scope.talentAuth)
    .then ->
      $state.go 'app.inside.jobapps.overview'
    .catch (rejection) ->
      if  rejection.errors? and rejection.errors.length > 0
        error = rejection.errors[0]
        $rootScope.$broadcast 'flash:warning', error
