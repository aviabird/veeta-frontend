angular.module("veetaTalentApp.sharing")
.constant "SHARING",
  VIEWS:
    OVERVIEW: "views/components/sharing/overview.html"

  PARTIALS:
    REVOKE_CONFIRM: "views/components/sharing/partials/revoke_confirm.html"
