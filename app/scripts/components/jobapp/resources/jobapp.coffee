###*
  # @ngdoc service
  # @name jobapp.resources:Jobapp
  #
  # @requires railsResourceFactory
  #
  # @description
###

angular.module 'veetaTalentApp.jobapp.resources'
.factory 'Jobapp', (railsResourceFactory) ->
  resource = railsResourceFactory
    url: CONFIG.apiTalentUrl + '/jobapps'
    name: 'jobapp'
    serializer: 'JobappSerializer'

  resource.getNew = (jobId)->
    resource.$get(resource.$url('new'), {job_id: jobId})

  resource::validate = ->
    if @isNew() then @$post @$url('validate') else @$put @$url('validate')

  resource::withdrawn = ->
    resource.$put(@$url('withdrawn'), {withdrawnReason: @withdrawnReason})

  resource
.factory 'JobappSerializer', (railsSerializer) ->
  return railsSerializer ->
    @only(
      'id'
      'jobId'
      'job'
      'jobType'
      'resumeId'
      'coverLetter'
      'administrationNote'
      'blocked'
    )
    @nestedAttribute('job')
    @serializeWith('job', 'JobSerializer')


