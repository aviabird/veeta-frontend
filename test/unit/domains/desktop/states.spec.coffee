describe "veetaTalentApp.desktop", ->
  describe "states", ->
    beforeEach module('veetaTalentApp.desktop')

    $rootScope = undefined
    $state = undefined
    LAYOUTS = undefined
    $injector = undefined


    beforeEach inject (_$rootScope_
                       _$state_
                       _$injector_
                       _LAYOUTS_) ->
      $rootScope = _$rootScope_
      $state = _$state_
      $injector = _$injector_
      LAYOUTS = _LAYOUTS_

    describe "app", ->
      state = "app"
      it "verify state configuration", ->
        config = $state.get(state)
        expect(config.abstract).toBeTruthy
        expect(config.templateUrl).toBe LAYOUTS.VIEWS.DESKTOP
        expect(config.controller).toBe 'AppCtrl'
        expect(config.url).toBeUndefined
#        expect($injector.invoke(config.resolve.talent)).toBe('settings-all');
#        expect($injector.invoke(config.resolve.talentSetting)).toBe('settings-all');


      describe 'resumes', ->
        describe 'overview', ->

        describe 'new', ->

        describe 'detail', ->

      describe 'jobapps', ->
        describe 'overview', ->

        describe 'new', ->

        describe 'detail', ->