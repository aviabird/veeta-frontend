"use strict"

###*
  # @ngdoc directive
  # @name resume.directives:resume-import
  #
  # @requires $rootScope
  # @requires $auth
  # @requires $translate
  # @requires $stateParams
  # @requires RESUME
  # @requires FileUploader
  # @requires InflectorService
  #
  # @description
  # import resume from third service, Pdf, xing, linkin, only shown on a new resume page
  # be used on resume detail page
  # TODO: PDF validation
###

angular.module("veetaTalentApp.resume.directives")
.directive "resumeImport", ($rootScope
                            $auth
                            $translate
                            $stateParams
                            RESUME
                            FileUploader
                            InflectorService) ->
  restrict: "AE"
  templateUrl: RESUME.PARTIALS.IMPORT
  replace: true
  scope: {}
  link: (scope, element) ->
    $translate ['talent.page.resumes.detail.import.pdf_title'
                'talent.page.resumes.detail.import.pdf_description']
    .then (translations)->
      element.find(':file').filestyle(
        input: false
        icon: false
        buttonName: 'btn-pdf'
        buttonText: "<h4>#{translations['talent.page.resumes.detail.import.pdf_title']}</h4><p>#{translations['talent.page.resumes.detail.import.pdf_description']}</p>"
      )

  controller: ($scope) ->

    # ----------------------------------------
    # Initialize attributes
    # ----------------------------------------

    # get resume from parent
    $scope.resume = $scope.$parent.resume


    # ----------------------------------------
    # Public methods
    # ----------------------------------------

    pdfParsingUploader = $scope.pdfParsingUploader = new FileUploader
      url: CONFIG.apiTalentUrl + '/resumes/' + $scope.resume.id + '/fillin'
      autoUpload: true
      method: 'PUT'
      headers: $auth.retrieveData('auth_headers')
      alias: 'resume_pdf[file]'

    # ----------------------------------------
    # Event
    # ----------------------------------------

    pdfParsingUploader.onSuccessItem = (item, response) ->
      return unless response.resume
      resume = InflectorService.camelizeObject(response.resume)
      $scope.$emit 'resume:import-successful', resume
