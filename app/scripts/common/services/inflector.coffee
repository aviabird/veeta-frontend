angular.module 'veetaTalentApp.common.services'
.factory 'InflectorService', ->
  camelizeString = (key) ->
    return key  unless angular.isString(key)

    # should this match more than word and digit characters?
    key.replace /_[\w\d]/g, (match, index, string) ->
      (if index is 0 then match else string.charAt(index + 1).toUpperCase())

  camelizeObject = (obj) ->
    newObj = {}

    for key, value of obj
      if angular.isArray value
        arr = []

        for valueItem in value
          arr.push camelizeObject(valueItem)

        value = arr
      else if angular.isObject value
        value = camelizeObject(value)

      newKey = camelizeString(key)
      newObj[newKey] = value

    newObj

  return {
  camelizeString: camelizeString
  camelizeObject: camelizeObject
  }
