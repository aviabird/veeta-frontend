class ResumeOverviewPage
  constructor: ->
    @resumesList = element.all(By.repeater 'resume in resumes')

  get: ->
    browser.get '#/resumes/overview'
    browser.getCurrentUrl()

module.exports = ResumeOverviewPage