angular.module("rails.extend").factory "RailsResourceFlashMixin", (RailsResourceInjector, $q, growl, $rootScope) ->
  render_messages_array = (messages) ->
    str = ""
    str += "<ul>"
    for key of messages
      str += "<li>" + messages[key] + "</li>"
    str += "</ul>"
    str

  RailsResourceFlashMixin = ->
    return

  RailsResourceFlashMixin.extended = (Resource) ->
    Resource.intercept "response", (result) ->
      if result.originalData and result.originalData.meta
        growl_str = ""
        if result.originalData.meta.message is "warning"
          growl_str = result.originalData.meta.warning
          $rootScope.$broadcast "flash:warning", growl_str
        if result.originalData.meta.message is "info"
          growl_str = result.originalData.meta.info
          $rootScope.$broadcast "flash:info", growl_str
        if result.originalData.meta.message is "notice"
          growl_str = result.originalData.meta.notice
          $rootScope.$broadcast "flash:notice", growl_str
      result

    Resource.intercept "responseError", (rejection) ->
      if rejection.data and rejection.data.meta
        growl_str = ""
        if rejection.data.meta.error and rejection.data.meta.error.messages
          growl_str += render_messages_array(rejection.data.meta.error.messages)  if rejection.data.meta.error.messages
          $rootScope.$broadcast "flash:error", growl_str
      $q.reject rejection

    return

  RailsResourceFlashMixin
