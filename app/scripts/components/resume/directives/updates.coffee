"use strict"

###*
  # @ngdoc directive
  # @name resume.directives:resume-updates
  #
  # @requires ResumeLinks
  # @requires RESUME
  #
  # @description
  # switch resume update
  # be used on resume overview page
###

angular.module 'veetaTalentApp.resume.directives'
.directive 'resumeUpdates', ($rootScope, $modal, RESUME) ->
  restrict: "AE"
  templateUrl: RESUME.PARTIALS.UPDATES
  replace: true
  scope: {}
  controller: ($scope) ->

    # declare private variables
    resumeUpdatesResetting = false

    # declare private methods
    openResumeUpdatesModal = undefined
    resetResumeUpdates = undefined

    # bind resumeUpdates attributes to scope
    $scope.resumeUpdates = $rootScope.talentSetting.resumeUpdates

    # if resumeUpdates be changed, update talent settings to server
    $scope.$watch 'resumeUpdates', (newVal, oldVal)->
      unless newVal == oldVal
        if resumeUpdatesResetting == true
          resumeUpdatesResetting = false
        else
          if newVal == false
            openResumeUpdatesModal()
          else
            $rootScope.talentSetting.resumeUpdates = true
            $rootScope.talentSetting.updateResumeUpdates()

    # open a modal for confirm with deactivate resume updates
    openResumeUpdatesModal = ->
      modalInstance = $modal.open(
        templateUrl: RESUME.PARTIALS.MODAL.RESUME_UPDATES_DEACTIVATE
        controller: 'ResumeUpdatesDeactivateModalCtrl'
      )
      modalInstance.result.then ((result) ->
        if result == true
          $rootScope.talentSetting.resumeUpdates = false
          $rootScope.talentSetting.updateResumeUpdates()
        else
          resetResumeUpdates()
      ), ->
        resetResumeUpdates()

    # reset resume updates
    resetResumeUpdates = ->
      resumeUpdatesResetting = true
      $scope.resumeUpdates = true
