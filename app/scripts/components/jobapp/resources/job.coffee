###*
  # @ngdoc service
  # @name jobapp.resources:Job
  #
  # @requires railsResourceFactory
  #
  # @description
###

angular.module 'veetaTalentApp.jobapp.resources'
.factory 'Job', (railsResourceFactory) ->
  resource = railsResourceFactory
    url: CONFIG.apiTalentUrl + '/jobs'
    name: 'job'

.factory 'JobSerializer', (railsSerializer) ->
  return railsSerializer ->
    @only(
      'companyName'
      'jobname'
      'source'
      'recruiterEmail'
    )
