"use strict"

###*
  # @ngdoc directive
  # @name resume.directives:resume-header-extend
  #
  # @requires ResumeSharing
  # @requires RESUME
  #
  # @description
  # a extend header of a resume
  # be used on resume detail page
###

angular.module 'veetaTalentApp.resume.directives'
.directive 'resumeHeaderExtend', (RESUME, ResumeSharing) ->
  restrict: "AE"
  templateUrl: RESUME.PARTIALS.HEADER_EXTEND
  replace: true
  scope: {}
  controller: ($scope, $state) ->
    # get resume from parent
    $scope.resume = $scope.$parent.resume
    $scope.sharingMode = false

    $scope.isNew = ->
      $state.includes('**.resumes.new')

    $scope.updateName = (name) ->
      $scope.resume.name = name
      $scope.resume.updateName().then (response) ->
        $scope.$parent.resume.name = response.name
        true

    $scope.isShowSharedCompanies = ->
      $scope.resume.companies and $scope.resume.companies.length > 0

    $scope.onSharing = ->
      $scope.sharingMode = true
      $scope.resumeSharing = new ResumeSharing()

    $scope.exitSharing = ->
      $scope.sharingMode = false

    $scope.share = ->
      $scope.resumeSharing.resumeId = $scope.resume.id
      $scope.resumeSharing.share().then ->
        $scope.exitSharing()
