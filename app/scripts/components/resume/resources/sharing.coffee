'use strict';

angular.module 'veetaTalentApp.resume.resources'
.factory 'ResumeSharing', (railsResourceFactory) ->
  resource = railsResourceFactory
    url: CONFIG.apiTalentUrl + '/resumes'
    name: 'resume_sharing'
    interceptors: ['belongsToResume']

  resource::share = ->
    @$put(resource.$url(@resumeId + '/share'))

  resource
