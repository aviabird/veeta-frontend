"use strict"


###*
  # @ngdoc controller
  # @name sharing.controllers:SharingOverviewCtrl
  #
  # @requires $scope
  # @requires $modal
  # @requires SHARING
  # @requires Sharing
  #
  # @description
  # Controller for sharing overview page
###
angular.module("veetaTalentApp.sharing.controllers")
.controller "SharingOverviewCtrl", ($scope, $modal, SHARING, Sharing) ->

  # Get a list of resumes from API
  Sharing.query().then (data) ->
    $scope.sharings = data

  $scope.isCollapsed = true

  ###*
  # @ngdoc function
  # @name sharing.controllers:SharingOverviewCtrl#onRevoke
  # @methodOf sharing.controllers:SharingOverviewCtrl
  #
  # @description
  # revoke selected sharing
  #
  ###
  $scope.onRevoke = (sharing) ->
    modalInstance = $modal.open
      templateUrl: SHARING.PARTIALS.REVOKE_CONFIRM
      controller: "SharingRevokeModalCtrl"
      resolve:
        sharing: ->
          sharing
    modalInstance.result.then (revokedSharing)->
      $scope.sharings.splice($scope.sharings.indexOf(revokedSharing), 1);

.controller "SharingRevokeConfirmCtrl", ($scope, $modalInstance, Sharing, sharing) ->
  $scope.revoke = ->
    Sharing.$revoke(sharing.company.id).then ->
      $modalInstance.close(sharing)

  $scope.cancel = ->
    $modalInstance.dismiss "cancel"

