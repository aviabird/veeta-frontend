"use strict"

###*
  # @ngdoc overview
  # @name resume
  #
###

angular.module 'veetaTalentApp.resume', [
  'veetaTalentApp.resume.controllers'
  'veetaTalentApp.resume.directives'
  'veetaTalentApp.resume.resources'
]

angular.module 'veetaTalentApp.resume.controllers', [
  'ui.router'
  'ui.bootstrap'
  'angularFileUpload'
  'veetaTalentApp.common.services'
]

angular.module 'veetaTalentApp.resume.directives', []
angular.module 'veetaTalentApp.resume.resources', [
  'rails'
  'veetaTalentApp.account.resources'
  'veetaTalentApp.common.resources'
]
