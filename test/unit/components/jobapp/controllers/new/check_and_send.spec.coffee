describe "veetaTalentApp.jobapp", ->
  describe "controllers", ->
    describe 'new', ->
      describe 'JobappNewAddCoverLetterCtrl', ->
        beforeEach module(
          "veetaTalentApp.jobapp"
          'mocked/jobapps/new-with-job.json'
          'mocked/jobapps/new-without-job.json'
        )

        $scope = undefined
        $controller = undefined
        $httpBackend = undefined
        jobapp = undefined
        Jobapp = undefined
        Resume = undefined

        beforeEach inject (_$httpBackend_
                           _$rootScope_
                           _$controller_
                           _Resume_
                           _$state_
                           _Jobapp_
                           _JOBAPP_
                           _mockedJobappsNewWithJob_) ->
          $httpBackend = _$httpBackend_
          $scope = _$rootScope_.$new()

          # mock a jobapp with resume
          Jobapp = _Jobapp_
          Resume = _Resume_
          jobapp = new Jobapp(_mockedJobappsNewWithJob_)
          jobapp.resume = new Resume()

          $scope.$parent.jobapp = jobapp

          spyOn(_$state_, 'go')

          # create controller object
          $controller = _$controller_ 'JobappNewAddCoverLetterCtrl',
            $scope: $scope
            $state: _$state_

        describe 'EmailApplication', ->
          describe 'initialize', ->
            it 'should get a jobapp', ->

        describe 'RefApplication', ->
          describe 'initialize', ->
            it 'should get a jobapp', ->