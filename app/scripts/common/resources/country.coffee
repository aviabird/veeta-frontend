'use strict';

# TODO: Move to namespaced API
# Read-only example
angular.module 'veetaTalentApp.common.resources'
.factory 'Country', (railsResourceFactory) ->
  resource = railsResourceFactory
    url: CONFIG.apiUrl + '/countries'
    name: 'country'
    pluralName: 'countries'
    serializer: 'CountrySerializer'
  resource

.factory 'CountrySerializer', (railsSerializer) ->
  return railsSerializer ->
    @only()