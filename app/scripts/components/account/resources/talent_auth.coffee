angular.module "veetaTalentApp.account.resources"
.factory "TalentAuth", (railsResourceFactory) ->
  resource = railsResourceFactory
    url: CONFIG.apiTalentUrl + "/talent_auth"
    name: "talent_auth"
    serializer: "TalentAuthSerializer"

  resource::deleteAccount = ->
    @$put(@$url('delete'))

  resource

.factory "TalentAuthSerializer", (railsSerializer) ->
  railsSerializer ->
    @only "email", 'password', 'deleteReason'
    @add 'numChapters', (talentAuth) ->
      if talentAuth.withOtherDeleteReason then talentAuth.otherDeleteReason else talentAuth.deleteReason

