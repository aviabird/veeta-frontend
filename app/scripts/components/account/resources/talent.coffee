angular.module "veetaTalentApp.account.resources"
.factory "Talent", (railsResourceFactory) ->
  resource = railsResourceFactory
    url: CONFIG.apiTalentUrl + "/talent"
    name: "talent"
    idAttribute: ''
    serializer: "TalentSerializer"

  resource::removeAvatar = ->
    resource.$delete(resource.$url('avatar_remove'))

  resource

.factory "TalentSerializer", (railsSerializer) ->
  railsSerializer ->
    @exclude "nationalityCountry"
    @add "nationalityCountryId", (talent) ->
      talent.nationalityCountry.id if talent.nationalityCountry
    @nestedAttribute "address"
    @serializeWith "address", "AddressSerializer"
