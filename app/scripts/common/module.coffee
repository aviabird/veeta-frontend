###*
  # @ngdoc overview
  # @name common
  #
###


angular.module 'veetaTalentApp.common', [
  # System Core
  'ngCookies'
  'ngSanitize'
  'ngLocale'
  'LocalStorageModule'

  # System Extend
  'ui.router'
  'ui.utils'
  'rails'

  # authencation
  'ng-token-auth'

  # UI Bootstrap
  'ui.bootstrap'

  # UI Extend
  'angular-growl'
  'angularSpinner'

  # Input Component Extend
  'xeditable'
  'angularFileUpload'
  'frapontillo.bootstrap-switch'
  'vr.directives.slider'

  # Loading
  'angular-growl'
  'angularSpinner'

  # I18n
  'pascalprecht.translate'

  # Libraries
  'rails.extend'
  'easy.form'

  # Modules
  'veetaTalentApp.templates'
  'veetaTalentApp.common.controllers'
  'veetaTalentApp.common.directives'
  'veetaTalentApp.common.services'
  'veetaTalentApp.common.resources',
  'veetaTalentApp.common.filters'
]

angular.module 'veetaTalentApp.common.controllers', []
angular.module 'veetaTalentApp.common.directives', []
angular.module 'veetaTalentApp.common.services', ['pascalprecht.translate']
angular.module 'veetaTalentApp.common.resources', []
angular.module 'veetaTalentApp.common.filters', []

