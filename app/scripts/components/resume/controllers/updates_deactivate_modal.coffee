###*
  # @ngdoc controller
  # @name resume.controllers:ResumeUpdatesDeactivateModalCtrl
  #
  # @requires $scope
  # @requires $stateParams
  # @requires FileUploader
  # @requires OptionsService
  # @requires resume
  #
  # @description
  # Controller for the Resume Detail
###


angular.module 'veetaTalentApp.resume.controllers'
.controller 'ResumeUpdatesDeactivateModalCtrl', ($scope, $modalInstance) ->
  ###*
  # @ngdoc function
  # @name veetaTalentApp.resume.controllers:ResumeUpdatesDeactivateModalCtrl:confirm
  #
  # @description
  # send confirm information back to modalInstance
  #
  ###
  $scope.confirm = ->
    $modalInstance.close true

  ###*
  # @ngdoc function
  # @name veetaTalentApp.resume.controllers:ResumeUpdatesDeactivateModalCtrl:cancel
  #
  # @description
  # close the modal window
  #
  ###
  $scope.cancel = ->
    $modalInstance.dismiss "cancel"
