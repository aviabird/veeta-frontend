# TODO: Add option to show description text in modal window
angular.module("veetaTalentApp.common.controllers")
.controller 'ItemRemoveModalCtrl', ($scope, $modalInstance, item) ->

  $scope.remove = ->
    if item.isNew()
      $modalInstance.close(item)
    else
      item.remove().then ->
        $modalInstance.close(item)

  $scope.cancel = ->
    $modalInstance.dismiss 'cancel'
